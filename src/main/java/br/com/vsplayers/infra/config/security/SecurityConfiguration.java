package br.com.vsplayers.infra.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.social.security.SpringSocialConfigurer;

import br.com.vsplayers.service.UsuarioService;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = { "br.com.vsplayers" })
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired(required = true)
	private UsuarioService usuarioService;

	@Autowired
	@Qualifier("AccessDeniedExceptionHandler")
	AccessDeniedExceptionHandler accessDeniedExceptionHandler;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth)
			throws Exception {

		DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
		daoAuthenticationProvider.setUserDetailsService(usuarioService);
		daoAuthenticationProvider.setPasswordEncoder(new Md5PasswordEncoder());
		auth.authenticationProvider(daoAuthenticationProvider);
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/js/**", "/css/**", "/img/**",
				"/webjars/**", "/pages/**", "/includes/**", "/resources/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/admin/**")
				.hasAuthority("ADMINISTRADOR")
				.antMatchers("/restricted/**")
				.hasAnyAuthority("USUARIO", "ADMINISTRADOR")
				.antMatchers("/resources/**", "/includes/**", "/cadastrar",
						"/logout", "/login", "/", "/home").permitAll().and()
				.apply(new SpringSocialConfigurer()).and().formLogin()
				.loginPage("/login")
				.defaultSuccessUrl("/restricted/welcome", true)
				.loginProcessingUrl("/login").failureUrl("/login?error=true")
				.permitAll().and().logout().logoutUrl("/logout")
				.logoutSuccessUrl("/").and().exceptionHandling()
				.accessDeniedHandler(accessDeniedExceptionHandler);
		;
	}
}
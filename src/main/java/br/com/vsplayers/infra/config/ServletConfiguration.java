package br.com.vsplayers.infra.config;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import br.com.vsplayers.infra.config.security.SecurityConfiguration;
import br.com.vsplayers.infra.config.security.social.SocialConfig;

@Order(2)
@Configuration
public class ServletConfiguration extends
		AbstractAnnotationConfigDispatcherServletInitializer {
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class<?>[] { SecurityConfiguration.class, SocialConfig.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[] { AppConfiguration.class };
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

	/**
	 * @Override protected Filter[] getServletFilters() {
	 * 
	 *           CharacterEncodingFilter characterEncodingFilter = new
	 *           CharacterEncodingFilter();
	 *           characterEncodingFilter.setEncoding("UTF-8");
	 *           characterEncodingFilter.setForceEncoding(true);
	 * 
	 *           MultipartFilter mpf = new MultipartFilter(); return new
	 *           Filter[] { characterEncodingFilter, mpf }; }
	 **/

	@Bean
	public MultipartResolver multipartResolver() {
		return new StandardServletMultipartResolver();
	}

	@Override
	protected void customizeRegistration(Dynamic registration) {
		String path = "/tmp";
		MultipartConfigElement multipartConfigElement = new MultipartConfigElement(
				path, 487572000, 487572000, 487572000);
		registration.setMultipartConfig(multipartConfigElement);
	}

}

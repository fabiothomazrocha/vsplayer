package br.com.vsplayers.infra.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import br.com.vsplayers.dto.OrderDTO;

import com.paypal.api.payments.Amount;
import com.paypal.api.payments.Details;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.RedirectUrls;
import com.paypal.api.payments.Transaction;
import com.paypal.core.rest.APIContext;
import com.paypal.core.rest.PayPalRESTException;

public class PayPalUtils {
	
	public static Payment createPayment(OrderDTO order) throws PayPalRESTException {

		Payment payment = new Payment();

		Details amountDetails = new Details();
		amountDetails.setShipping(order.getShipping());
		amountDetails.setSubtotal(order.getOrderAmount());
		amountDetails.setTax(order.getTax());

		Amount amount = new Amount();
		amount.setCurrency(order.getCurrency());
		Double total = Double.parseDouble(order.getTax())
				+ Double.parseDouble(order.getShipping())
				+ Double.parseDouble(order.getOrderAmount());

		DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols(
				Locale.US);
		decimalFormatSymbols.setDecimalSeparator('.');
		decimalFormatSymbols.setGroupingSeparator(',');
		DecimalFormat decimalFormat = new DecimalFormat("#,##0.00",
				decimalFormatSymbols);

		amount.setTotal(decimalFormat.format(total));
		amount.setDetails(amountDetails);

		RedirectUrls redirectUrls = new RedirectUrls();
		redirectUrls.setCancelUrl(order.getCancelUrl());
		redirectUrls.setReturnUrl(order.getReturnUrl());

		Transaction transaction = new Transaction();
		transaction.setAmount(amount);
		transaction.setDescription(order.getOrderDesc());
		List<Transaction> transactions = new ArrayList<Transaction>();
		transactions.add(transaction);

		Payer payer = new Payer();
		payer.setPaymentMethod("paypal");

		payment.setIntent(order.getPaymentIntent());
		payment.setPayer(payer);
		payment.setRedirectUrls(redirectUrls);
		payment.setTransactions(transactions);

		String accessToken = PaypalAccessTokenGenerator.getAccessToken();
		String requestId = UUID.randomUUID().toString();
		APIContext apiContext = new APIContext(accessToken, requestId);
		return payment.create(apiContext);
	}
}

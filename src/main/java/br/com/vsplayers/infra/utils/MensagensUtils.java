package br.com.vsplayers.infra.utils;


/**
 * @author Rodolfo Martins - furstmartins
 */
public class MensagensUtils {
	public static final String FORM = "form";
	public static final String INSERT_OK = "Registro gravado com sucesso!";
	public static final String UPDATE_OK = "Atualizado com sucesso!";
	public static final String UPDATE_DELETADO = "Não foi possivel alterar esse registro provavelmente o registro foi excluido!";
	public static final String DELETE_OK = "Deletado com Sucesso!";
	public static final String DELETE_ERRO = "Não possivel excluir esse registro, ele é referenciado por outros registros!";
	public static final String DELETE_DELETADO = "Esse registro já foi excluido!";
}

package br.com.vsplayers.infra.utils;

import com.paypal.core.ConfigManager;
import com.paypal.core.rest.OAuthTokenCredential;
import com.paypal.core.rest.PayPalRESTException;

public class PaypalAccessTokenGenerator {
	
	private static String accessToken;

	public static String getAccessToken() throws PayPalRESTException {

		if (accessToken == null) {

			String clientID = ConfigManager.getInstance().getValue("clientID");
			String clientSecret = ConfigManager.getInstance().getValue(
					"clientSecret");
			
			accessToken = new OAuthTokenCredential(clientID, clientSecret)
					.getAccessToken();
		}

		return accessToken;
	}
}

package br.com.vsplayers.infra.dao;

import java.util.List;

import br.com.vsplayers.domain.Usuario;

/**
 * @author Rodolfo Martins - furstmartins
 */
public interface UsuarioDAO {

	public void addUsuario(Usuario usuario);
    public List<Usuario> listUsuario();
    public void removeUsuario(Long id);
    public void updateUsuario(Usuario usuario);
    public Usuario getUsuarioPorEmail(String email);
	public Boolean verificaUsuarioExistePorEmail(String email);
	public Usuario findUsuarioByCodigoAtivacao(String cod);
	public Usuario recuperaUsuarioPorCodigoAlteracaoSenha(
			String codigoAlterarSenha);
	public Usuario getUsuarioPorEmailAtivado(String email);
	public Usuario findById(Long idUsuario);
}

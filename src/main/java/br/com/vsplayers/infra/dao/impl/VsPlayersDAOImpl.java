package br.com.vsplayers.infra.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.domain.VsPlayers;
import br.com.vsplayers.infra.dao.VsPlayersDAO;

@Repository
@Transactional
@Qualifier("VsPlayersDAOImpl")
public class VsPlayersDAOImpl extends BaseDAO<VsPlayers> implements
		VsPlayersDAO {

	@Override
	public VsPlayers getFirstResult() {
		Session s = super.openSession();
		Query query = s.createQuery("select obj from VsPlayers obj");
		List<VsPlayers> result = query.list();
		
		if(!result.isEmpty()) {
			return result.get(0);
		}
		
		return null;
	}

	@Override
	public void inserirNovaInstancia(VsPlayers vsInstance) {
		Session s = super.openSession();
		s.persist(vsInstance);
	}

}

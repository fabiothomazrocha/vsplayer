package br.com.vsplayers.infra.dao;

import br.com.vsplayers.domain.Imagem;

public interface ImagemDAO {
	
	void inserirImagem(Imagem img);

	Imagem loadImagemByIdPerfil(Long id);

	Boolean removeImagem(Long id);

}

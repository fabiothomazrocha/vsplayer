package br.com.vsplayers.infra.dao;

import java.util.List;

import br.com.vsplayers.domain.ItemFaq;

public interface ItemFaqDAO {
	public List<ItemFaq> listaTodosItemFaqs();

	ItemFaq atualizarFaq(ItemFaq itemFaq);

	Boolean deleteItemFaq(Long idItemFaqSelecionado);

	ItemFaq getById(Long id);

	public void inserirItemFaq(ItemFaq novoItemFaq);
}

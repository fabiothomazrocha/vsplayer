package br.com.vsplayers.infra.dao;

import java.util.List;

import br.com.vsplayers.domain.Console;
import br.com.vsplayers.domain.enums.TipoAcessoRedeJogo;
import br.com.vsplayers.domain.enums.TipoConsole;

public interface ConsoleDAO {
	
	public List<Console> listarConsolesPorTipoRede(TipoAcessoRedeJogo tipoAcesso);

	public List<Console> getConsolesByConsoleNames(
			List<TipoConsole> consolesSelecionados);

	public List<Console> listaTodosConsoles();

	Console atualizarConsole(Console console);

	Boolean deleteConsole(Long idConsoleSelecionado);

	Console getById(Long id);

	public void inserirConsole(Console novoConsole);

}

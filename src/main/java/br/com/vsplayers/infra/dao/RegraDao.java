package br.com.vsplayers.infra.dao;

import java.util.List;

import br.com.vsplayers.domain.PerfilUsuario;
import br.com.vsplayers.domain.Regra;

public interface RegraDao {
	
	
	List<Regra> listarRegra(Long idUsuario);
	void inserirRegra(Regra novaRegra);
	Regra getById(Long id);
	Boolean deleteRegra(Long idRegraSelecionada);
	Regra atualizarRegra(Regra regra);
	Integer countRegras(Long idUsuario);
	

}

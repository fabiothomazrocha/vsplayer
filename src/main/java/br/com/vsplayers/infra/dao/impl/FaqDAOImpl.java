package br.com.vsplayers.infra.dao.impl;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.domain.Faq;
import br.com.vsplayers.infra.dao.FaqDAO;

@Transactional
@Repository
@Qualifier("FaqDAOImpl")
public class FaqDAOImpl extends BaseDAO<Faq> implements FaqDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Faq> listaTodosFaqs() {

		Session s = openSession();

		Query q = s
				.createQuery("select obj from Faq obj inner join fetch obj.vsPlayers order by obj.id");

		return q.list();
	}

	@Override
	public Faq atualizarFaq(Faq faq) {
		return (Faq) openSession().merge(faq);
	}

	@Override
	public Boolean deleteFaq(Long idFaqSelecionado) {
		Query query = openSession().createQuery(
				"delete Faq faq where faq.id = :idFaqSelecionado");
		query.setParameter("idFaqSelecionado", idFaqSelecionado);
		int result = query.executeUpdate();

		return result == 0;
	}

	@Override
	public Faq getById(Long id) {
		Query query = openSession().createQuery(
				"select obj from Faq obj where obj.id = :id");

		query.setParameter("id", id);

		Faq returnObject = (Faq) query.uniqueResult();
		if (returnObject.getVsPlayers() != null) {
			Hibernate.initialize(returnObject.getVsPlayers());
		}
		if (returnObject.getListaItemFAQ() != null) {
			Hibernate.initialize(returnObject.getListaItemFAQ());
		}
		return returnObject;
	}

	@Override
	public void inserirFaq(Faq novoFaq) {
		Session s = openSession();
		s.persist(novoFaq);
		s.flush();

	}

	@Override
	public Faq getByName(String nome) {
		Query query = openSession().createQuery(
				"select obj from Faq obj where obj.nmFaq = :nome");
		query.setParameter("nome", nome);

		Faq returnObject = (Faq) query.uniqueResult();
		if (returnObject.getVsPlayers() != null) {
			Hibernate.initialize(returnObject.getVsPlayers());
		}
		if (returnObject.getListaItemFAQ() != null) {
			Hibernate.initialize(returnObject.getListaItemFAQ());
		}
		return returnObject;
	}

}

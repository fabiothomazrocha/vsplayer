package br.com.vsplayers.infra.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.domain.Usuario;
import br.com.vsplayers.domain.enums.Flag;
import br.com.vsplayers.infra.dao.UsuarioDAO;

/**
 * @author Rodolfo Martins - furstmartins
 */
@Repository
@Transactional
@Qualifier("UsuarioDAOImpl")
public class UsuarioDAOImpl extends BaseDAO<Usuario> implements UsuarioDAO {

	@Override
	public void addUsuario(Usuario usuario) {
		Session s = openSession();
		s.save(usuario);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Usuario> listUsuario() {
		Session s = openSession();
		return s.createQuery("from Usuario").list();
	}

	@Override
	public void removeUsuario(Long id) {

		Session s = openSession();
		Usuario contact = (Usuario) s.load(Usuario.class, id);

		if (null != contact) {
			s.delete(contact);
		}
	}

	@Override
	public void updateUsuario(Usuario usuario) {
		Session s = openSession();
		s.update(usuario);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Usuario getUsuarioPorEmail(String email) {
		List<Usuario> userList = new ArrayList<>();
		Query query = openSession().createQuery(
				"from Usuario u where u.dsEmail = :email");
		query.setParameter("email", email);

		userList = query.list();
		if (userList != null && !userList.isEmpty()) {
			return userList.get(0);
		}

		return null;

	}

	@Override
	public Boolean verificaUsuarioExistePorEmail(String email) {

		List<Usuario> userList = new ArrayList<>();
		Query query = openSession().createQuery(
				"select count(u.id) from Usuario u where u.dsEmail = :email");
		query.setParameter("email", email);

		Long result = (Long) query.uniqueResult();
		return result > 0;
	}

	@Override
	public Usuario findUsuarioByCodigoAtivacao(String cod) {

		List<Usuario> userList = new ArrayList<>();

		Query query = openSession().createQuery(
				"from Usuario u where u.codigoAtivacao = :codAtivacao");

		query.setParameter("codAtivacao", cod);

		userList = query.list();

		if (userList != null && !userList.isEmpty()) {
			return userList.get(0);
		}

		return null;
	}

	@Override
	public Usuario recuperaUsuarioPorCodigoAlteracaoSenha(
			String codigoAlterarSenha) {

		List<Usuario> userList = new ArrayList<>();

		Query query = openSession()
				.createQuery(
						"from Usuario u where u.codigoAlteracaoSenha = :codigoAlteracaoSenha");

		query.setParameter("codigoAlteracaoSenha", codigoAlterarSenha);

		userList = query.list();

		if (userList != null && !userList.isEmpty()) {
			return userList.get(0);
		}

		return null;
	}

	@Override
	public Usuario getUsuarioPorEmailAtivado(String email) {

		List<Usuario> userList = new ArrayList<>();

		Query query = openSession()
				.createQuery(
						"from Usuario u where u.dsEmail = :email and u.ativado = :ativado");

		query.setParameter("email", email);
		query.setParameter("ativado", Flag.S);

		userList = query.list();

		if (userList != null && !userList.isEmpty()) {
			Usuario returnUser = userList.get(0);
			Hibernate.initialize(returnUser.getPapeis());
			return returnUser;
		}

		return null;
	}

	@Override
	public Usuario findById(Long idUsuario) {

		Query query = openSession().createQuery(
				"select u from Usuario u where u.id = :idUsuario");

		query.setParameter("idUsuario", idUsuario);

		Usuario usuario = (Usuario) query.uniqueResult();

		if (usuario != null) {
			
			if (usuario.getPapeis() != null) {
				Hibernate.initialize(usuario.getPapeis());
			}

			if (usuario.getListaAcessoRedeJogo() != null) {
				Hibernate.initialize(usuario.getListaAcessoRedeJogo());
			}
			
			return usuario;
		}

		return null;
	}

}
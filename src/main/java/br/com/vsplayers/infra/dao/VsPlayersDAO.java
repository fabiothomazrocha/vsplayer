package br.com.vsplayers.infra.dao;

import br.com.vsplayers.domain.VsPlayers;

public interface VsPlayersDAO {

	VsPlayers getFirstResult();

	void inserirNovaInstancia(VsPlayers vsInstance);

}
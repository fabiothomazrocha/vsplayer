package br.com.vsplayers.infra.dao;

import java.util.List;

import br.com.vsplayers.domain.Jogo;
import br.com.vsplayers.domain.Regra;

public interface JogoDAO {
	
	List<Jogo> listaTodosJogos();
	void inserirJogo(Jogo novoJogo);
	Boolean deleteJogo(Long idJogoSelecionado);
	Jogo atualizarJogo(Jogo jogo);
	Jogo getById(Long id);
}

package br.com.vsplayers.infra.dao.impl;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.domain.ItemFaq;
import br.com.vsplayers.infra.dao.ItemFaqDAO;

@Transactional
@Repository
@Qualifier("ItemFaqDAOImpl")
public class ItemFaqDAOImpl extends BaseDAO<ItemFaq> implements ItemFaqDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<ItemFaq> listaTodosItemFaqs() {

		Session s = openSession();

		Query q = s
				.createQuery("select obj from ItemFaq obj order by obj.id");

		return q.list();
	}

	@Override
	public ItemFaq atualizarFaq(ItemFaq itemFaq) {
		return (ItemFaq) openSession().merge(itemFaq);
	}

	@Override
	public Boolean deleteItemFaq(Long idItemFaqSelecionado) {
		Query query = openSession()
				.createQuery(
						"delete ItemFaq itemFaq where itemFaq.id = :idItemFaqSelecionado");
		query.setParameter("idItemFaqSelecionado", idItemFaqSelecionado);
		int result = query.executeUpdate();

		return result == 0;
	}

	@Override
	public ItemFaq getById(Long id) {
		Query query = openSession().createQuery(
				"select obj from ItemFaq obj where obj.id = :id");

		query.setParameter("id", id);

		ItemFaq returnObject = (ItemFaq) query.uniqueResult();
		if (returnObject.getFaq() != null) {
			Hibernate.initialize(returnObject.getFaq());
		}

		return returnObject;
	}

	@Override
	public void inserirItemFaq(ItemFaq novoItemFaq) {
		Session s = openSession();
		s.persist(novoItemFaq);
		s.flush();
	}

}

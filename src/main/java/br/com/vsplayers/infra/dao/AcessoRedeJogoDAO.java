package br.com.vsplayers.infra.dao;

import java.util.List;

import br.com.vsplayers.domain.AcessoRedeJogo;

public interface AcessoRedeJogoDAO {

	List<AcessoRedeJogo> listAllByUser(Long userId);

	void desativar(Long idAcessoRedeJogo);

	void persistNewEntity(AcessoRedeJogo acs);

	Integer countAcessoRedeAtivoUsuario(Long idUsuario);

}

package br.com.vsplayers.infra.dao.impl;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.domain.SalaJogos;
import br.com.vsplayers.infra.dao.SalaJogosDAO;

@Repository
@Transactional
@Qualifier("SalaJogosDAOImpl")
public class SalaJogosDAOImpl extends BaseDAO<SalaJogos> implements
		SalaJogosDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<SalaJogos> listaTodosSalaJogos() {
		Session s = openSession();

		Query q = s
				.createQuery("select obj from SalaJogos obj order by obj.id");

		return q.list();

	}

	@Override
	public void inserirSalaJogos(SalaJogos novoSalaJogos) {
		Session s = openSession();
		s.persist(novoSalaJogos);
		s.flush();

	}

	@Override
	public Boolean deleteSalaJogos(Long idSalaJogosSelecionado) {
		Query query = openSession().createQuery(
				"delete SalaJogos obj where obj.id = :idSalaJogosSelecionado");
		query.setParameter("idSalaJogosSelecionado", idSalaJogosSelecionado);
		int result = query.executeUpdate();

		return result == 0;
	}

	@Override
	public SalaJogos atualizarSalaJogos(SalaJogos salaJogos) {
		return (SalaJogos) openSession().merge(salaJogos);

	}

	@Override
	public SalaJogos getById(Long id) {
		Query query = openSession().createQuery(
				"select obj from SalaJogos obj where obj.id = :id");

		query.setParameter("id", id);

		SalaJogos returnObject = (SalaJogos) query.uniqueResult();
		if (returnObject.getListaDesafios() != null) {
			Hibernate.initialize(returnObject.getListaDesafios());
		}
		if (returnObject.getJogo() != null) {
			Hibernate.initialize(returnObject.getJogo());
		}

		return returnObject;
	}

}

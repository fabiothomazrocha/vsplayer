package br.com.vsplayers.infra.dao.impl;

import java.util.EnumSet;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.domain.Console;
import br.com.vsplayers.domain.Jogo;
import br.com.vsplayers.domain.enums.Flag;
import br.com.vsplayers.domain.enums.TipoAcessoRedeJogo;
import br.com.vsplayers.domain.enums.TipoConsole;
import br.com.vsplayers.infra.dao.ConsoleDAO;

@Repository
@Transactional
@Qualifier("ConsoleDAOImpl")
public class ConsoleDAOImpl extends BaseDAO<Console> implements ConsoleDAO {
	public List<Console> listarConsolesPorTipoRede(TipoAcessoRedeJogo tipoAcesso) {
		Session s = openSession();
		Query q = s
				.createQuery("select obj from Console obj where obj.tipoRede = (:tipoRede)and obj.ativo = :ativo");
		q.setParameter("tipoRede", tipoAcesso);
		q.setParameter("ativo", Flag.S);
		return q.list();
	}

	@Override
	public List<Console> getConsolesByConsoleNames(
			List<TipoConsole> consolesSelecionados) {
		Session s = openSession();
		Query q = s
				.createQuery("select obj from Console obj where obj.tipoConsole in :tipoConsole  and obj.ativo = :ativo");
		q.setParameterList("tipoConsole", EnumSet.copyOf(consolesSelecionados));
		q.setParameter("ativo", Flag.S);
		return q.list();
	}

	@Override
	public List<Console> listaTodosConsoles() {
		Session s = openSession();
		Query q = s.createQuery("select obj from Console obj where obj.ativo = :ativo");
		q.setParameter("ativo", Flag.S);
		return q.list();
	}

	@Override
	public Console atualizarConsole(Console console) {
		return (Console) openSession().merge(console);
	}

	@Override
	public Boolean deleteConsole(Long idConsoleSelecionado) {
		Query query = openSession()
				.createQuery(
						"update Console console set console.ativo = :ativo where console.id = :idConsoleSelecionado");
		query.setParameter("idConsoleSelecionado", idConsoleSelecionado);
		query.setParameter("ativo", Flag.N);
		int result = query.executeUpdate();

		return result == 0;
	}

	@Override
	public Console getById(Long id) {
		Query query = openSession().createQuery(
				"select obj from Console obj where obj.id = :id and obj.ativo = :ativo");
		query.setParameter("id", id);
		query.setParameter("ativo", Flag.S);
		Console returnObject = (Console) query.uniqueResult();
		if (returnObject.getListaJogos()!= null) {
			Hibernate.initialize(returnObject.getListaJogos());
		}
		return returnObject;
	}

	@Override
	public void inserirConsole(Console novoConsole) {
		Session s = openSession();
		s.persist(novoConsole);
		s.flush();
	}

}
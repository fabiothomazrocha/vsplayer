package br.com.vsplayers.infra.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.domain.AcessoRedeJogo;
import br.com.vsplayers.domain.enums.Flag;
import br.com.vsplayers.infra.dao.AcessoRedeJogoDAO;

@Repository
@Transactional
@Qualifier("AcessoRedeJogoDAOImpl")
public class AcessoRedeJogoDAOImpl extends BaseDAO<AcessoRedeJogo> implements
		AcessoRedeJogoDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<AcessoRedeJogo> listAllByUser(Long userId) {
		Session s = openSession();
		Query q = s
				.createQuery("select obj from AcessoRedeJogo obj where obj.usuario.id = :idUsuario and obj.ativo = :ativo");
		q.setParameter("idUsuario", userId);
		q.setParameter("ativo", Flag.S);
		return q.list();
	}

	@Override
	public void desativar(Long idAcessoRedeJogo) {

		Session s = openSession();

		Query q = s
				.createQuery("update AcessoRedeJogo obj set ativo = :ativo where obj.id = :idAcessoRedeJogo");
		q.setParameter("idAcessoRedeJogo", idAcessoRedeJogo);
		q.setParameter("ativo", Flag.N);

		q.executeUpdate();
	}

	@Override
	public void persistNewEntity(AcessoRedeJogo acs) {
		super.persistEntity(acs);
	}

	@Override
	public Integer countAcessoRedeAtivoUsuario(Long idUsuario) {
		Session s = openSession();
		Query q = s
				.createQuery("select count(obj.id) from AcessoRedeJogo obj where obj.usuario.id = :idUsuario and obj.ativo = :ativo");

		q.setParameter("idUsuario", idUsuario);
		q.setParameter("ativo", Flag.S);
		return ((Long) q.uniqueResult()).intValue();
	}

}

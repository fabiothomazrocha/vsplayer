package br.com.vsplayers.infra.dao.impl;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.domain.Imagem;
import br.com.vsplayers.infra.dao.ImagemDAO;

@Repository
@Transactional
@Qualifier("ImagemDAOImpl")
public class ImagemDAOImpl extends BaseDAO<Imagem> implements ImagemDAO {

	@Override
	public void inserirImagem(Imagem img) {
		openSession().persist(img);
	}

	@Override
	public Imagem loadImagemByIdPerfil(Long id) {
		Session s = openSession();
		Query q = s
				.createQuery("select obj from Imagem obj where obj.perfilUsuario.id = :idPerfil");
		return (Imagem) q.uniqueResult();
	}

	@Override
	public Boolean removeImagem(Long id) {

		Session s = openSession();
		Query q = s.createQuery("delete Imagem obj where obj.id = :idImagem");
		q.setParameter("idImagem", id);

		return q.executeUpdate() == 0;
	}

}

package br.com.vsplayers.infra.dao;

import br.com.vsplayers.domain.Papel;

public interface PapelDAO {
	
	public Papel getPapelByName(String name);
	public void inserirPapel(Papel obj);
	
}
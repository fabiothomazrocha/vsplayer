package br.com.vsplayers.infra.dao;

import java.util.List;

import br.com.vsplayers.domain.SalaJogos;

public interface SalaJogosDAO {

	List<SalaJogos> listaTodosSalaJogos();

	void inserirSalaJogos(SalaJogos novoSalaJogos);

	Boolean deleteSalaJogos(Long idSalaJogosSelecionado);

	SalaJogos atualizarSalaJogos(SalaJogos salaJogos);

	SalaJogos getById(Long id);
}

package br.com.vsplayers.infra.dao;

import java.util.List;

import br.com.vsplayers.domain.PerfilUsuario;



public interface PerfilDAO {

	List<PerfilUsuario> listarPerfisUsuario(Long idUsuario);
	void inserirPerfil(PerfilUsuario novoPerfil);
	Boolean verificaApelidoJaExiste(String apelido);
	PerfilUsuario getByIdInitImagemPerfil(Long id);
	Boolean deletePerfil(Long idPerfilSelecionado);
	PerfilUsuario atualizarPerfil(PerfilUsuario perfil);
	Integer countPerfisAtivosUsuario(Long idUsuario);

}

package br.com.vsplayers.infra.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.domain.Papel;
import br.com.vsplayers.infra.dao.PapelDAO;

@Repository
@Transactional
@Qualifier("PapelDAOImpl")
public class PapelDAOImpl extends BaseDAO<Papel> implements PapelDAO {

	@Override
	public Papel getPapelByName(String nome) {

		List<Papel> papelList = new ArrayList<Papel>();

		Query query = openSession().createQuery(
				"from Papel p where p.nmNome = :nome");
		query.setParameter("nome", nome);

		papelList = query.list();

		if (papelList != null && !papelList.isEmpty()) {
			return papelList.get(0);
		}

		return null;

	}

	@Override
	public void inserirPapel(Papel obj) {
		super.persistEntity(obj);
	}

}

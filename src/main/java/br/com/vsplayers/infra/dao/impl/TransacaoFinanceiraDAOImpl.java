package br.com.vsplayers.infra.dao.impl;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.domain.TransacaoFinanceira;
import br.com.vsplayers.domain.enums.Flag;
import br.com.vsplayers.domain.enums.TipoTransacao;
import br.com.vsplayers.infra.dao.TransacaoFinanceiraDAO;

@Transactional
@Qualifier("TransacaoFinanceiraDAOImpl")
@Repository
public class TransacaoFinanceiraDAOImpl extends BaseDAO<TransacaoFinanceira>
		implements TransacaoFinanceiraDAO {

	@Override
	public List<TransacaoFinanceira> listaTransacaoUsuario(Long id) {

		Session s = super.openSession();
		Query query = s
				.createQuery("select obj from TransacaoFinanceira obj where obj.usuario.id = :idUsuario ");
		query.setParameter("idUsuario", id);

		return query.list();
	}

	@Override
	public void gravarNovaTransacao(TransacaoFinanceira trxSolic) {
		super.persistEntity(trxSolic);
	}

	@Override
	public List<TransacaoFinanceira> listaTransacaoTodosUsuarios() {

		Session s = super.openSession();

		Query query = s
				.createQuery("select obj from TransacaoFinanceira obj inner join fetch obj.usuario where obj.aprovada = :aprovada and obj.tipoTransacao = :tipoTransacao");
		
		query.setParameter("aprovada", Flag.N);
		query.setParameter("tipoTransacao", TipoTransacao.SLSA);

		return query.list();
	}

	@Override
	public BigDecimal getSaldoPositivoUsuario(Long idUsuario) {

		Session s = super.openSession();

		Query query = s
				.createQuery("select sum(obj.valorTransacao) from TransacaoFinanceira obj "
						+ "where obj.usuario.id = :idUsuario and obj.aprovada = :aprovada and obj.tipoTransacao = :tipoTransacao ");

		query.setParameter("idUsuario", idUsuario);
		query.setParameter("aprovada", Flag.S);
		query.setParameter("tipoTransacao", TipoTransacao.DEPOS);

		Object result = query.uniqueResult();

		if (result != null) {
			return (BigDecimal) result;
		} else {
			return BigDecimal.ZERO;
		}
	}

	@Override
	public BigDecimal getSaldoNegativoUsuario(Long idUsuario) {

		Session s = super.openSession();

		Query query = s
				.createQuery("select sum(obj.valorTransacao) from TransacaoFinanceira obj "
						+ "where obj.usuario.id = :idUsuario and obj.aprovada = :aprovada and obj.tipoTransacao = :tipoTransacao ");

		query.setParameter("idUsuario", idUsuario);
		query.setParameter("aprovada", Flag.S);
		query.setParameter("tipoTransacao", TipoTransacao.SAQUE);

		Object result = query.uniqueResult();

		if (result != null) {
			return (BigDecimal) result;
		} else {
			return BigDecimal.ZERO;
		}
	}

	@Override
	public TransacaoFinanceira getByIdInitDetalheSolic(Long idSolicitacao) {

		TransacaoFinanceira obj = super.findById(idSolicitacao);

		Hibernate.initialize(obj.getDetalheSolic());

		return obj;

	}

	@Override
	public List<TransacaoFinanceira> listaTransacaoTodosUsuariosAprovadas() {
		Session s = super.openSession();

		Query query = s
				.createQuery("select obj from TransacaoFinanceira obj where obj.aprovada = :aprovada ");

		query.setParameter("aprovada", Flag.S);

		return query.list();
	}
	
	@Override
	public void save(TransacaoFinanceira trx) {
		super.persistEntity(trx);
	}
	
}
package br.com.vsplayers.infra.dao;

import java.math.BigDecimal;
import java.util.List;

import br.com.vsplayers.domain.TransacaoFinanceira;

public interface TransacaoFinanceiraDAO {

	public List<TransacaoFinanceira> listaTransacaoUsuario(Long id);

	public void gravarNovaTransacao(TransacaoFinanceira trxSolic);

	public List<TransacaoFinanceira> listaTransacaoTodosUsuarios();

	public BigDecimal getSaldoPositivoUsuario(Long idUsuario);
	
	public BigDecimal getSaldoNegativoUsuario(Long idUsuario);

	public TransacaoFinanceira getByIdInitDetalheSolic(Long idSolicitacao);

	public List<TransacaoFinanceira> listaTransacaoTodosUsuariosAprovadas();

	public void save(TransacaoFinanceira trx);

}

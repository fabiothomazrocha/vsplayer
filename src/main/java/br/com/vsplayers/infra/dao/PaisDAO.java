package br.com.vsplayers.infra.dao;

import java.util.List;

import br.com.vsplayers.domain.Pais;
import br.com.vsplayers.dto.pais.PaisDTO;
import br.com.vsplayers.exception.VsPlayersException;

public interface PaisDAO {
	public List<Pais> listarTodosPaises();

	public void inserirPais(Pais novoPais) throws VsPlayersException;

	Boolean deletePais(Long idPaisSelecionado);

	Pais atualizarPais(Pais pais) throws VsPlayersException;

	Pais getById(Long idPaisSelecionado);

	public Pais getByName(String nome);
}

package br.com.vsplayers.infra.dao.impl;

import java.lang.reflect.ParameterizedType;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseDAO<T> {

	private Logger logger = LoggerFactory.getLogger(BaseDAO.class);

	private Class persistentClass;

	@SuppressWarnings("unchecked")
	public BaseDAO() {
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];
	}

	@Autowired
	private SessionFactory sessionFactory;

	public Session openSession() {
		return sessionFactory.getCurrentSession();
	}

	public T findById(Long id) {
		T entity = null;
		try {

			entity = (T) openSession().load(this.persistentClass, id);

		} catch (HibernateException e) {
			logger.error(e.getMessage());
			throw e;
		}
		return entity;
	}

	protected void persistEntity(T object) {
		openSession().persist(object);
	}

}

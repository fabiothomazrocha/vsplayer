package br.com.vsplayers.infra.dao.impl;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.domain.Jogo;
import br.com.vsplayers.infra.dao.JogoDAO;

@Transactional
@Repository
@Qualifier("JogoDAOImpl")
public class JogoDAOImpl extends BaseDAO<Jogo> implements JogoDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Jogo> listaTodosJogos() {
		
		Session s = openSession();
		
		Query q = s
				.createQuery("select obj from Jogo obj inner join fetch obj.console order by obj.console");
		

		return q.list();
	}

	@Override
	public void inserirJogo(Jogo novoJogo) {
		Session s = openSession();
		s.persist(novoJogo);
		s.flush();
		
	}
	@Override
	public Jogo atualizarJogo(Jogo jogo) {
	
		 return (Jogo)openSession().merge(jogo);
	}
	@Override
	public Boolean deleteJogo(Long idJogoSelecionado) {
		Query query = openSession()
				.createQuery(
						"delete Jogo jogo where jogo.id = :idJogoSelecionado");
		query.setParameter("idJogoSelecionado", idJogoSelecionado);
		int result = query.executeUpdate();

		return result == 0;
	}

	@Override
	public Jogo getById(Long id) {
		Query query = openSession().createQuery(
				"select obj from Jogo obj where obj.id = :id");

		query.setParameter("id", id);

		Jogo returnObject = (Jogo) query.uniqueResult();
		if (returnObject.getConsole()!= null) {
			Hibernate.initialize(returnObject.getConsole());
		}

		return returnObject;
	}
}

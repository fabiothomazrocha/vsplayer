package br.com.vsplayers.infra.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.domain.Pais;
import br.com.vsplayers.exception.VsPlayersException;
import br.com.vsplayers.infra.dao.PaisDAO;

@Transactional
@Repository
@Qualifier("PaisDAOImpl")
public class PaisDAOImpl extends BaseDAO<Pais> implements PaisDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Pais> listarTodosPaises() {

		Session s = openSession();

		Query q = s.createQuery("select obj from Pais obj order by obj.id");

		return q.list();
	}

	@Override
	public void inserirPais(Pais novoPais) throws VsPlayersException {
		Session s = openSession();
		s.persist(novoPais);
		s.flush();

	}

	@Override
	public Boolean deletePais(Long idPaisSelecionado) {
		Query query = openSession().createQuery(
				"delete Pais pais where pais.id = :idPaisSelecionado");
		query.setParameter("idPaisSelecionado", idPaisSelecionado);
		int result = query.executeUpdate();

		return result == 0;
	}

	@Override
	public Pais atualizarPais(Pais pais) throws VsPlayersException {
		return (Pais) openSession().merge(pais);

	}

	@Override
	public Pais getById(Long idPaisSelecionado) {
		Query query = openSession().createQuery(
				"select obj from Pais obj where obj.id = :id");

		query.setParameter("id", idPaisSelecionado);

		Pais returnObject = (Pais) query.uniqueResult();
	
		return returnObject;
	}

	@Override
	public Pais getByName(String nome) {
		Query query = openSession().createQuery(
				"select obj from Pais obj where obj.nmPais = :nome");
		query.setParameter("nome", nome);

		Pais returnObject = (Pais) query.uniqueResult();
	
		return returnObject;
	}
	}



package br.com.vsplayers.infra.dao;

public interface GenericDAOMethods<T> {

	public T findById(Long id);

}

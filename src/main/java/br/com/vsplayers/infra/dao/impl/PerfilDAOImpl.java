package br.com.vsplayers.infra.dao.impl;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.domain.PerfilUsuario;
import br.com.vsplayers.domain.enums.Flag;
import br.com.vsplayers.infra.dao.PerfilDAO;

@Repository
@Transactional
@Qualifier("PerfilDAOImpl")
public class PerfilDAOImpl extends BaseDAO<PerfilUsuario> implements PerfilDAO {

	@Override
	public List<PerfilUsuario> listarPerfisUsuario(Long idUsuario) {

		Query query = openSession()
				.createQuery(
						"select obj from PerfilUsuario obj where obj.usuario.id = :idUsuario and obj.ativo = :ativo");

		query.setParameter("idUsuario", idUsuario);
		query.setParameter("ativo", Flag.S);

		@SuppressWarnings("unchecked")
		List<PerfilUsuario> resultList = (List<PerfilUsuario>) query.list();

		return resultList;
	}

	@Override
	public void inserirPerfil(PerfilUsuario novoPerfil) {
		Session s = openSession();
		s.persist(novoPerfil);
		s.flush();
	}

	@Override
	public Boolean verificaApelidoJaExiste(String apelido) {
		Query query = openSession()
				.createQuery(
						"select count(obj.id) from PerfilUsuario obj where obj.apelido = :apelido");

		query.setParameter("apelido", apelido);

		return (((Long) query.uniqueResult()) > 0);
	}

	public PerfilUsuario getByIdInitImagemPerfil(Long id) {

		Query query = openSession().createQuery(
				"select obj from PerfilUsuario obj where obj.id = :id");

		query.setParameter("id", id);

		PerfilUsuario returnObject = (PerfilUsuario) query.uniqueResult();

		if (returnObject.getImagemPerfil() != null) {
			Hibernate.initialize(returnObject.getImagemPerfil());
		}

		return returnObject;
	}

	@Override
	public Boolean deletePerfil(Long idPerfilSelecionado) {
		Query query = openSession()
				.createQuery(
						"update PerfilUsuario perfil set perfil.ativo = :ativo where perfil.id = :idPerfilSelecionado");
		query.setParameter("idPerfilSelecionado", idPerfilSelecionado);
		query.setParameter("ativo", Flag.N);
		int result = query.executeUpdate();

		return result == 0;
	}

	@Override
	public PerfilUsuario atualizarPerfil(PerfilUsuario perfil) {
		return (PerfilUsuario) openSession().merge(perfil);
	}

	@Override
	public Integer countPerfisAtivosUsuario(Long idUsuario) {
		Session s = openSession();
		Query q = s
				.createQuery("select count(obj.id) from PerfilUsuario obj where obj.usuario.id = :idUsuario and obj.ativo = :ativo");
		
		q.setParameter("idUsuario", idUsuario);
		q.setParameter("ativo", Flag.S);
		return ((Long) q.uniqueResult()).intValue();
	}

}
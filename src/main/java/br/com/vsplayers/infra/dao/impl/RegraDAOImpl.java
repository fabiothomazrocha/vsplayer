package br.com.vsplayers.infra.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.domain.Regra;
import br.com.vsplayers.infra.dao.RegraDao;
@Repository
@Transactional
@Qualifier("RegraDAOImpl")
public class RegraDAOImpl extends BaseDAO<Regra> implements RegraDao {


	public List<Regra> listarRegra(Long idUsuario) {

		Query query = openSession()
				.createQuery(
						"select obj from Regra obj where obj.vsPlayers.id = :idUsuario");

		query.setParameter("idUsuario", idUsuario);

		@SuppressWarnings("unchecked")
		List<Regra> resultList = (List<Regra>) query.list();

		return resultList;
	}

	
	public void inserirRegra(Regra novaRegra) {
		Session s = openSession();
		s.persist(novaRegra);
		s.flush();

	}

	@Override
	public Boolean deleteRegra(Long idRegraSelecionada) {
		Query query = openSession()
				.createQuery(
						"delete Regra regra where regra.id = :idRegraSelecionada");
		query.setParameter("idRegraSelecionada", idRegraSelecionada);
		int result = query.executeUpdate();

		return result == 0;
	}

	@Override
	public Regra atualizarRegra(Regra regra) {
	
		 return (Regra)openSession().merge(regra);
	}

	@Override
	public Integer countRegras(Long idUsuario) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Regra getById(Long id) {
		Query query = openSession().createQuery(
				"select obj from Regra obj where obj.id = :id");

		query.setParameter("id", id);

		Regra returnObject = (Regra) query.uniqueResult();

		return returnObject;
	}

}

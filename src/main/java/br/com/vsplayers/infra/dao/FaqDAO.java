package br.com.vsplayers.infra.dao;

import java.util.List;

import br.com.vsplayers.domain.Faq;

public interface FaqDAO {

	public List<Faq> listaTodosFaqs();

	Faq atualizarFaq(Faq faq);

	Boolean deleteFaq(Long idFaqSelecionado);

	Faq getById(Long id);

	public void inserirFaq(Faq novoFaq);

	public Faq getByName(String nome);
}

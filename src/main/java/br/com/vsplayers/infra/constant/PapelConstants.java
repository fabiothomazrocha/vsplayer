package br.com.vsplayers.infra.constant;

public interface PapelConstants {

	public static final String ROLE_USER = "USUARIO";
	public static final String ROLE_ADMIN = "ADMINISTRADOR";

}

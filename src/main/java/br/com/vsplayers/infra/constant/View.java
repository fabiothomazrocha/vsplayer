package br.com.vsplayers.infra.constant;

/**
 * @author Rodolfo Martins - furstmartins
 */
public final class View {

	/**
	 * Home
	 */
	public static final String HOME = "home";
	public static final String LOGIN = "login";
	public static final String LIST_CONTENT = "list-content";
	/**
	 * Login
	 */
	public static final String ADMIN = "admin";
}

package br.com.vsplayers.infra.constant;

public interface ConfigConstants {
	
	public final String URL_SERVICO = "http://localhost:8080/vsplayers";
	public final String URL_SERVICO_ATIVACAO = URL_SERVICO + "/ativaUsuario";
	public final String URL_SERVICO_ALT_SENHA = URL_SERVICO + "/alteraSenha";
	
	public final String EMAIL_VS_PLAYERS = "vsplayers@fabio.com.br";
	

}

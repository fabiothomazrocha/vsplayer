package br.com.vsplayers.infra.constant;

/**
 * @author Rodolfo Martins - furstmartins
 */
public final class URL {

	/**
	 * Home
	 */
	public static final String HOME = "/";
	
	public static final String LOGIN = "/login";
	public static final String SEARCH = "s";
	public static final String FILTRO = "filtro";
	/**
	 * Admin
	 */
	public static final String ADMIN = "/admin";
	
}

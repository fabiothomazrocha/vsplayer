package br.com.vsplayers.service;

import br.com.vsplayers.domain.Usuario;
import br.com.vsplayers.integration.beans.VsPlayersUserDetail;

public interface UsuarioAssembler {
	
	public VsPlayersUserDetail buildUserFromUserEntity(Usuario usuario);

}

package br.com.vsplayers.service;

import java.util.List;

import br.com.vsplayers.domain.ItemFaq;
import br.com.vsplayers.dto.itemfaq.ItemFaqDTO;
import br.com.vsplayers.exception.VsPlayersException;

public interface ItemFaqService {

	public List<ItemFaqDTO> listarTodosItemFaqs();

	public void inserirItemFaq(ItemFaq novoItemFaq) throws VsPlayersException;

	Boolean deleteItemFaq(Long idItemFaqSelecionado);

	void atualizarItemFaq(ItemFaq itemFaq) throws VsPlayersException;

	ItemFaq getById(Long idItemFaqSelecionado);

}

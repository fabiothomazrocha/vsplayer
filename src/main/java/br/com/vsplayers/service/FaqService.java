package br.com.vsplayers.service;

import java.util.List;

import br.com.vsplayers.domain.Faq;
import br.com.vsplayers.dto.faq.FaqDTO;
import br.com.vsplayers.exception.VsPlayersException;

public interface FaqService {
	public List<FaqDTO> listarTodosFaqs();

	public void inserirFaq(Faq novoFaq) throws VsPlayersException;

	Boolean deleteFaq(Long idFaqSelecionado);

	void atualizarFaq(Faq faq) throws VsPlayersException;

	Faq getById(Long idFaqSelecionado);

	public Faq getByName(String nome);
}

package br.com.vsplayers.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import br.com.vsplayers.domain.Usuario;

/**
 * @author Marcus Soliva - viniciussoliva
 * @author Matheus Cardoso - mtzimba
 * @author Rodolfo Martins - furstmartins
 */
public interface UsuarioService extends UserDetailsService {

	public void addUsuario(Usuario usuario);

	public List<Usuario> listUsuario();

	public void removeUsuario(Long id);

	public void updateUsuario(Usuario usuario);

	public Boolean usuarioLoginValido(Usuario usuario);

	public Usuario inserirUsuarioFacebook(String nomeUsuario, String emailLogin, String cpf);

	public Usuario inserirUsuarioVsPlayers(String nomeUsuario,
			String emailLogin, String senhaDescrip, String cpf);

	Boolean verificaUsuarioExistePorEmail(String email);

	public Usuario ativaUsuario(String cod);

	public Boolean reenviaCodigoAtivacao(String email);

	public void solicitarAlteracaoSenha(String email);

	public Boolean alterarSenha(String codigoAlterarSenha, String novaSenha);
	
	public Boolean alterarSenha(Long idUsuario, String novaSenha);

	public Usuario loginREST(String usuario, String senha);

	public Usuario getUsuarioPorEmailAtivado(String trim);

	public Usuario getById(Long idUsuario);

	public Boolean alterarEmail(Long id, String novoEmail);
	
}

package br.com.vsplayers.service.ioutils;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.vsplayers.exception.VsPlayersException;

public class VsPlayersIOUtils {

	private static final Logger logger = LoggerFactory
			.getLogger(VsPlayersIOUtils.class);

	public static final String DEFAULT_IMG_FORMAT = "png";

	public static void writeImg2Disk(Image image, String fileName)
			throws VsPlayersException {
		try {

			File novoArquivo = new File(fileName);

			if (!novoArquivo.exists()) {
				novoArquivo.createNewFile();
			}

			boolean b = ImageIO.write((BufferedImage) image,
					VsPlayersIOUtils.DEFAULT_IMG_FORMAT, novoArquivo);
			if (!b) {
				throw new VsPlayersException(null);
			}

		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}

	public static void removeImgFromDisk(String file) {
		File novoArquivo = new File(file);
		logger.info("TENTANDO REMOVER: " + file);
		if (novoArquivo.exists()) {
			novoArquivo.delete();
			logger.info("REMOVEu: " + file);
		} else {
			logger.info("arquivo nao existe: " + file);
		}
	}

}

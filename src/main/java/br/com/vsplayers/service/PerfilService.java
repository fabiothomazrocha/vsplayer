package br.com.vsplayers.service;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.List;

import br.com.vsplayers.domain.PerfilUsuario;
import br.com.vsplayers.dto.perfil.PerfilUsuarioDTO;
import br.com.vsplayers.exception.VsPlayersException;

public interface PerfilService {

	public List<PerfilUsuarioDTO> listarPerfisUsuario(Long idUsuario);

	public void inserirPerfil(String apelido, String sobre, Long idUsuario,
			boolean temImagem, String path, BufferedImage image)
			throws VsPlayersException;

	public Boolean verificaApelidoJaExiste(String apelido);

	public void inserirFotoPerfil(String apelido, BufferedImage imgPerfil,
			String path) throws VsPlayersException;

	public PerfilUsuario getById(Long idPerfilSelecionado);
	public PerfilUsuarioDTO getDTOById(Long idPerfilSelecionado);

	public Boolean deletePerfil(Long idPerfilSelecionado);

	public void atualizarPerfil(PerfilUsuario perfil,
			Boolean temImagemCadastrada, Boolean removerImagem,
			Boolean alterarImagem, String realPath, Image image)
			throws VsPlayersException;

	public Integer countPerfisAtivosUsuario(Long id);

}

package br.com.vsplayers.service;

import java.math.BigDecimal;
import java.util.List;

import br.com.vsplayers.domain.TransacaoFinanceira;
import br.com.vsplayers.domain.Usuario;
import br.com.vsplayers.dto.TransacaoFinanceiraDTO;
import br.com.vsplayers.exception.VsPlayersException;

public interface TransacaoFinanceiraService {

	public List<TransacaoFinanceiraDTO> listarTransacoesUsuario(Usuario usuario);

	public void inserirNovaSolicitacaoSaque(Long id, String agencia,
			String nomeBanco, String nomeTitular, String numeroConta,
			String cpf, String string) throws VsPlayersException;

	public List<TransacaoFinanceiraDTO> listarTransacoesTodosUsuarios();
	
	public List<TransacaoFinanceiraDTO> listarTransacoesTodosUsuariosAprovadas();

	public BigDecimal verificaSaldoUsuario(Long id);

	public TransacaoFinanceiraDTO getDadosDeposito(Long idSolicitacao);

	public void confirmDeposito(Long idSolicitacao);

	public void inserirNovoDeposito(TransacaoFinanceira trx);

	public void confirmaTransacao(Long valueOf, String token);

	public TransacaoFinanceira getById(Long id);

}
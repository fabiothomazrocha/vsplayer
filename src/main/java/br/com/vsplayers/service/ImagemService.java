package br.com.vsplayers.service;

import br.com.vsplayers.domain.Imagem;

public interface ImagemService {

	void inserirImagem(Imagem img);

	Imagem loadImagemByIdPerfil(Long id);

	Boolean removerImagem(Long id);

}

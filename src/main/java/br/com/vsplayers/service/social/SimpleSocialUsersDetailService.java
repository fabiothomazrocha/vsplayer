package br.com.vsplayers.service.social;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.social.security.SocialUserDetailsService;
import org.springframework.stereotype.Service;

import br.com.vsplayers.domain.Usuario;
import br.com.vsplayers.service.UsuarioAssembler;
import br.com.vsplayers.service.UsuarioService;

@Service
@Qualifier("SimpleSocialUsersDetailService")
public class SimpleSocialUsersDetailService implements SocialUserDetailsService {

	@Autowired
	@Qualifier("UsuarioServiceImpl")
	private UsuarioService usuarioService;
	
	@Autowired
	@Qualifier("UsuarioAssemblerImpl")
	private UsuarioAssembler usuarioAssembler;

	@Override
	public SocialUserDetails loadUserByUserId(String userId)
			throws UsernameNotFoundException, DataAccessException {
		
		Usuario userEntity = usuarioService.getUsuarioPorEmailAtivado(userId
				.trim());
		
		if (userEntity == null)
			throw new UsernameNotFoundException(
					"Usuario nao encontrado ou não está ativado");

		return usuarioAssembler.buildUserFromUserEntity(userEntity);
	}

}

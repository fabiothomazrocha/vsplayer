package br.com.vsplayers.service;

import java.util.List;

import br.com.vsplayers.domain.Jogo;
import br.com.vsplayers.dto.jogo.JogoDTO;
import br.com.vsplayers.exception.VsPlayersException;

public interface JogoService {

	public List<JogoDTO> listarTodosJogos();

	public void inserirJogo(Jogo novoJogo) throws VsPlayersException;

	Boolean deleteJogo(Long idJogoSelecionado);

	void atualizarJogo(Jogo jogo) throws VsPlayersException;

	Jogo getById(Long idJogoSelecionado);
}

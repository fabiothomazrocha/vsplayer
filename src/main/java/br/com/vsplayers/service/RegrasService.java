package br.com.vsplayers.service;

import java.util.List;

import br.com.vsplayers.domain.Regra;
import br.com.vsplayers.exception.VsPlayersException;

public interface RegrasService {

	public List<Regra> listarRegras(Long idUsuario);

	public void inserirRegra(Regra regra) throws VsPlayersException;
	public void atualizarRegra(Regra regra) throws VsPlayersException;
	public Regra getById(Long idRegraSelecionado);

	public Boolean deleteRegra(Long idRegraSelecionado);
	
	public Integer countRegras(Long id);

}

package br.com.vsplayers.service;

public interface RestUsersManagerService {
	
	public Boolean isUserAuhorized(String token);
	public Boolean insertUserAuth(String token);

}

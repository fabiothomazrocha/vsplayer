package br.com.vsplayers.service;

import java.util.List;

import br.com.vsplayers.domain.AcessoRedeJogo;
import br.com.vsplayers.dto.acessoredejogo.AcessoRedeJogoDTO;

public interface AcessoRedeJogoService {
	
	List<AcessoRedeJogoDTO> listAllByUser(Long id);
	
	void deletePerfil(Long idAcessoRedeJogo);
	
	void inserirNovoAcessoRede(AcessoRedeJogo acs);

	Integer countAcessoRedeAtivoUsuario(Long id);
	
}

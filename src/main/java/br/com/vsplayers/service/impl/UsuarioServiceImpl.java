package br.com.vsplayers.service.impl;

import static br.com.vsplayers.infra.constant.Message.Logger.FINAL_METODO;
import static br.com.vsplayers.infra.constant.Message.Logger.INICIO_METODO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.domain.Papel;
import br.com.vsplayers.domain.Usuario;
import br.com.vsplayers.domain.enums.Flag;
import br.com.vsplayers.domain.enums.TipoUsuario;
import br.com.vsplayers.infra.constant.ConfigConstants;
import br.com.vsplayers.infra.constant.PapelConstants;
import br.com.vsplayers.infra.dao.PapelDAO;
import br.com.vsplayers.infra.dao.UsuarioDAO;
import br.com.vsplayers.infra.utils.CriptografiaUtils;
import br.com.vsplayers.infra.utils.StringUtils;
import br.com.vsplayers.service.UsuarioAssembler;
import br.com.vsplayers.service.UsuarioService;
import br.com.vsplayers.service.mail.MailService;

/**
 * @author Marcus Soliva - viniciussoliva
 * @author Matheus Cardoso - mtzimba
 * @author Rodolfo Martins - furstmartins
 */
@Service
@Transactional
@Qualifier("UsuarioServiceImpl")
public class UsuarioServiceImpl implements UsuarioService {

	private static final Logger logger = LoggerFactory
			.getLogger(UsuarioServiceImpl.class);

	@Autowired
	@Qualifier("UsuarioDAOImpl")
	private UsuarioDAO usuarioDAO;

	@Autowired
	@Qualifier("PapelDAOImpl")
	private PapelDAO papelDAO;

	@Autowired
	@Qualifier("UsuarioAssemblerImpl")
	private UsuarioAssembler usuarioAssembler;

	@Autowired
	@Qualifier("MailServiceImpl")
	private MailService mailService;

	@Override
	public void addUsuario(Usuario usuario) {
		logger.info(INICIO_METODO + usuario);

		usuarioDAO.addUsuario(usuario);

		logger.info(FINAL_METODO + usuario);
	}

	@Override
	public List<Usuario> listUsuario() {
		logger.info(INICIO_METODO);

		final List<Usuario> listUsuario = usuarioDAO.listUsuario();

		logger.info(FINAL_METODO + listUsuario);
		return listUsuario;
	}

	@Override
	public void removeUsuario(Long id) {
		logger.info(INICIO_METODO + id);

		usuarioDAO.removeUsuario(id);

		logger.info(FINAL_METODO + id);
	}

	@Override
	public void updateUsuario(Usuario usuario) {
		logger.info(INICIO_METODO + usuario);

		usuarioDAO.updateUsuario(usuario);

		logger.info(FINAL_METODO + usuario);
	}

	@Override
	public Boolean usuarioLoginValido(Usuario usuario) {
		logger.info(INICIO_METODO + usuario);

		if (usuarioNuloOuVazio(usuario)) {
			return false;
		}

		Usuario usuarioSalvo = usuarioDAO.getUsuarioPorEmail(usuario
				.getDsEmail().trim());

		if (usuarioNuloOuVazio(usuarioSalvo)) {
			return false;
		}

		if (!usuario.getDsEmail().equalsIgnoreCase(usuarioSalvo.getDsEmail())) {
			return false;
		} else {
			if (usuario.getDsSenha().equals(usuarioSalvo.getDsSenha())) {
				return true;
			}
		}
		logger.info(FINAL_METODO + usuario);

		return false;
	}

	/**
	 * Verifica se o usuário é nulo ou se as variaveis email e senha são nulas
	 * ou vazias.
	 * 
	 * @param usuario
	 * @return <code>true</code> se usuário <code>null</code> ou vazio.
	 *         <code>false</code> caso contrário.
	 */
	private Boolean usuarioNuloOuVazio(Usuario usuario) {

		logger.info(INICIO_METODO + usuario);
		if (usuario == null) {
			return true;
		}

		if (StringUtils.isBlank(usuario.getDsEmail())
				|| StringUtils.isBlank(usuario.getDsSenha())) {
			return true;
		}
		logger.info(FINAL_METODO + usuario);

		return false;
	}

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {

		Usuario userEntity = usuarioDAO.getUsuarioPorEmailAtivado(username
				.trim());
		if (userEntity == null)
			throw new UsernameNotFoundException(
					"Usuario nao encontrado ou não está ativado");

		return usuarioAssembler.buildUserFromUserEntity(userEntity);
	}

	@Transactional
	@Override
	public Usuario inserirUsuarioVsPlayers(String nomeUsuario,
			String emailLogin, String senhaDescrip, String cpf) {

		Usuario usuarioVs = new Usuario();

		usuarioVs.setNmNome(nomeUsuario);
		usuarioVs.setDsEmail(emailLogin);
		usuarioVs.setDsCpf(cpf);

		Papel papelUsuario = papelDAO.getPapelByName(PapelConstants.ROLE_USER);
		usuarioVs.setPapeis(new ArrayList<Papel>());
		usuarioVs.setAtivado(Flag.N);
		usuarioVs.getPapeis().add(papelUsuario);
		usuarioVs.setDataCadastro(new Date());
		usuarioVs.setDataUltimaAtualizacao(new Date());
		usuarioVs.setTipoUsuario(TipoUsuario.VSPLA);
		usuarioVs.setDsSenha(CriptografiaUtils.toMD5(senhaDescrip));

		String codigoPaginaAtivacao = CriptografiaUtils.toMD5(emailLogin
				+ "COD_ATIV_VSPLAYERS" + String.valueOf(new Date().getTime()));

		usuarioVs.setCodigoAtivacao(codigoPaginaAtivacao);

		usuarioDAO.addUsuario(usuarioVs);

		String emailAtivacao = gerarCorpoEmail(codigoPaginaAtivacao);

		try {

			mailService.sendMail(ConfigConstants.EMAIL_VS_PLAYERS,
					new String[] { emailLogin },
					"VsPlayers - Ativação de Sua Conta", emailAtivacao);

		} catch (Exception ex) {
			return null;
		}

		return usuarioVs;
	}

	@Transactional
	@Override
	public Usuario inserirUsuarioFacebook(String nomeUsuario,
			String emailLogin, String cpf) {

		Usuario usuarioFace = new Usuario();

		usuarioFace.setNmNome(nomeUsuario);
		usuarioFace.setDsEmail(emailLogin);
		usuarioFace.setDsCpf(cpf);

		Papel papelUsuario = papelDAO.getPapelByName(PapelConstants.ROLE_USER);
		usuarioFace.setPapeis(new ArrayList<Papel>());
		usuarioFace.getPapeis().add(papelUsuario);
		usuarioFace.setAtivado(Flag.N);
		usuarioFace.setDataUltimaAtualizacao(new Date());
		usuarioFace.setTipoUsuario(TipoUsuario.FACEB);
		usuarioFace.setDataCadastro(new Date());
		usuarioFace.setDsSenha(CriptografiaUtils.toMD5("VsPlayersSocialUser"
				+ Math.random() * 0.45));

		String codigoPaginaAtivacao = CriptografiaUtils.toMD5(emailLogin
				+ "COD_ATIV_VSPLAYERS" + String.valueOf(new Date().getTime()));

		usuarioFace.setCodigoAtivacao(codigoPaginaAtivacao);

		usuarioDAO.addUsuario(usuarioFace);

		String emailAtivacao = gerarCorpoEmail(codigoPaginaAtivacao);

		try {

			mailService.sendMail(ConfigConstants.EMAIL_VS_PLAYERS,
					new String[] { emailLogin },
					"VsPlayers - Ativação de Sua Conta", emailAtivacao);

		} catch (Exception ex) {
			logger.error(ex.getMessage());
			return null;
		}

		return usuarioFace;
	}

	private String gerarCorpoEmail(String codigoPaginaAtivacao) {
		// parte ativacao
		String url = ConfigConstants.URL_SERVICO_ATIVACAO + "/"
				+ codigoPaginaAtivacao;

		String emailAtivacao = "<html><body>Olá, para ativar seu perfil acesse: <a href='"
				+ url + "'>VsPlayers Pagina de Ativacao</a></body></html>";
		return emailAtivacao;
	}

	@Override
	@Transactional(readOnly = true)
	public Boolean verificaUsuarioExistePorEmail(String email) {
		Boolean existe = usuarioDAO.verificaUsuarioExistePorEmail(email);
		return existe;
	}

	@Transactional
	@Override
	public Usuario ativaUsuario(String cod) {
		Usuario usuario = usuarioDAO.findUsuarioByCodigoAtivacao(cod);
		if (usuario != null) {

			usuario.setAtivado(Flag.S);
			usuario.setCodigoAtivacao("");
			usuario.setDataAtivacao(new Date());
			usuario.setDataUltimaAtualizacao(new Date());
			usuarioDAO.updateUsuario(usuario);

			if (usuario.getPapeis() != null) {
				Hibernate.initialize(usuario.getPapeis());
			}

			return usuario;
		}
		return null;
	}

	@Override
	@Transactional
	public Boolean reenviaCodigoAtivacao(String email) {

		Usuario usuario = usuarioDAO.getUsuarioPorEmail(email);

		if (usuario != null) {
			if (Flag.N.equals(usuario.getAtivado())) {
				// parte ativacao
				String emailLogin = usuario.getDsEmail();
				String codigoPaginaAtivacao = CriptografiaUtils
						.toMD5(emailLogin + "COD_ATIV_VSPLAYERS"
								+ String.valueOf(new Date().getTime()));

				usuario.setCodigoAtivacao(codigoPaginaAtivacao);
				usuarioDAO.updateUsuario(usuario);
				String emailAtivacao = gerarCorpoEmail(codigoPaginaAtivacao);

				try {

					mailService.sendMail(ConfigConstants.EMAIL_VS_PLAYERS,
							new String[] { emailLogin },
							"VsPlayers - Ativação de Sua Conta", emailAtivacao);

				} catch (Exception ex) {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}

	@Override
	@Transactional
	public void solicitarAlteracaoSenha(String email) {

		Usuario usuario = usuarioDAO.getUsuarioPorEmail(email);

		if (usuario != null) {

			String emailLogin = usuario.getDsEmail();
			String codigoAlteracaoSenha = CriptografiaUtils.toMD5(emailLogin
					+ "ALT_SENHA_VSPLAYERS"
					+ String.valueOf(new Date().getTime()));

			usuario.setSolicitaAltSenha(Flag.S);
			usuario.setCodigoAlteracaoSenha(codigoAlteracaoSenha);
			usuarioDAO.updateUsuario(usuario);

			String url = ConfigConstants.URL_SERVICO_ALT_SENHA + "/"
					+ codigoAlteracaoSenha;

			String emailAtivacao = "<html><body>Olá, para alterar sua senha acesse: <a href='"
					+ url + "'>VsPlayers Pagina de Ativacao</a></body></html>";

			try {
				mailService.sendMail(ConfigConstants.EMAIL_VS_PLAYERS,
						new String[] { emailLogin },
						"VsPlayers - Alteração de Senha", emailAtivacao);

			} catch (Exception ex) {
				logger.error(ex.getMessage());
			}
		}

	}

	@Override
	@Transactional
	public Boolean alterarSenha(String codigoAlterarSenha, String novaSenha) {

		String senhaMD5 = CriptografiaUtils.toMD5(novaSenha);
		Usuario usuario = usuarioDAO
				.recuperaUsuarioPorCodigoAlteracaoSenha(codigoAlterarSenha);

		if (usuario != null) {

			usuario.setDsSenha(senhaMD5);
			usuario.setCodigoAlteracaoSenha("");
			usuario.setSolicitaAltSenha(Flag.N);
			usuario.setDataUltimaAtualizacao(new Date());
			usuarioDAO.updateUsuario(usuario);

			return Boolean.TRUE;
		} else {

			return Boolean.FALSE;
		}
	}

	@Override
	public Usuario loginREST(String usuario, String senha) {

		Usuario usuarioLogin = usuarioDAO.getUsuarioPorEmailAtivado(usuario);

		if (usuarioLogin != null) {

			if (senha.equals(usuarioLogin.getDsSenha())) {
				return usuarioLogin;
			}
		}

		return null;
	}

	@Override
	public Usuario getUsuarioPorEmailAtivado(String trim) {
		return usuarioDAO.getUsuarioPorEmailAtivado(trim);
	}

	@Override
	public Usuario getById(Long idUsuario) {
		return usuarioDAO.findById(idUsuario);
	}

	@Override
	public Boolean alterarSenha(Long idUsuario, String novaSenha) {

		String senhaMD5 = CriptografiaUtils.toMD5(novaSenha);

		Usuario usuario = usuarioDAO.findById(idUsuario);

		if (usuario != null) {

			usuario.setDsSenha(senhaMD5);
			usuario.setCodigoAlteracaoSenha("");
			usuario.setSolicitaAltSenha(Flag.N);
			usuario.setDataUltimaAtualizacao(new Date());
			usuarioDAO.updateUsuario(usuario);

			return Boolean.TRUE;
		} else {

			return Boolean.FALSE;
		}
	}

	@Override
	public Boolean alterarEmail(Long idUsuario, String novoEmail) {

		Usuario usuario = usuarioDAO.findById(idUsuario);
		
		return alterarEmailEnviaCodigoAtivacao(usuario, novoEmail);
	}

	public Boolean alterarEmailEnviaCodigoAtivacao(Usuario usuario, String novoEmail) {

		if (usuario != null) {
						
			String codigoPaginaAtivacao = CriptografiaUtils.toMD5(novoEmail
					+ "COD_ATIV_VSPLAYERS"
					+ String.valueOf(new Date().getTime()));

			usuario.setCodigoAtivacao(codigoPaginaAtivacao);
			usuario.setDsEmail(novoEmail);
			usuarioDAO.updateUsuario(usuario);
			
			String emailAtivacao = gerarCorpoEmail(codigoPaginaAtivacao);

			try {
				
				mailService.sendMail(ConfigConstants.EMAIL_VS_PLAYERS,
						new String[] { novoEmail },
					"VsPlayers - Ativação de Sua Conta - Alteraçao de E-mail", emailAtivacao);

			} catch (Exception ex) {
				return false;
			}
		} else {
			return false;
		}
		return true;
	}
}

package br.com.vsplayers.service.impl;

import java.util.Date;

import javax.inject.Singleton;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import br.com.vsplayers.infra.utils.CriptografiaUtils;
import br.com.vsplayers.service.RestTokenService;

@Singleton
@Service
@Qualifier("RestTokenServiceImpl")
public class RestTokenServiceImpl implements RestTokenService {

	@Override
	public String getTokenForUser(String user) {
		
		String tk = "VSPlayerS_REST_TOKEN" + user
				+ String.valueOf(new Date().getTime());
		
		return CriptografiaUtils.toMD5(tk).toUpperCase();
	}

}

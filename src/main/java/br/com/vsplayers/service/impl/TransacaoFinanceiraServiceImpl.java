package br.com.vsplayers.service.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.converter.Converter;
import br.com.vsplayers.converter.impl.TransacaoFinanceiraComDetalheConverter;
import br.com.vsplayers.converter.impl.TransacaoFinanceiraConverter;
import br.com.vsplayers.domain.DetalheSolicitacaoSaque;
import br.com.vsplayers.domain.TransacaoFinanceira;
import br.com.vsplayers.domain.Usuario;
import br.com.vsplayers.domain.enums.Flag;
import br.com.vsplayers.domain.enums.TipoBanco;
import br.com.vsplayers.domain.enums.TipoTransacao;
import br.com.vsplayers.dto.TransacaoFinanceiraDTO;
import br.com.vsplayers.exception.VsPlayersException;
import br.com.vsplayers.infra.dao.TransacaoFinanceiraDAO;
import br.com.vsplayers.infra.dao.UsuarioDAO;
import br.com.vsplayers.infra.utils.StringUtils;
import br.com.vsplayers.service.TransacaoFinanceiraService;

@Transactional
@Service
@Qualifier("TransacaoFinanceiraServiceImpl")
public class TransacaoFinanceiraServiceImpl implements
		TransacaoFinanceiraService {

	@Autowired
	@Qualifier("TransacaoFinanceiraDAOImpl")
	private TransacaoFinanceiraDAO transacaoFinanceiraDAO;

	@Autowired
	@Qualifier("UsuarioDAOImpl")
	private UsuarioDAO usuarioDAO;

	public TransacaoFinanceiraDAO getTransacaoFinanceiraDAO() {
		return transacaoFinanceiraDAO;
	}

	public void setTransacaoFinanceiraDAO(
			TransacaoFinanceiraDAO transacaoFinanceiraDAO) {
		this.transacaoFinanceiraDAO = transacaoFinanceiraDAO;
	}

	@Override
	public List<TransacaoFinanceiraDTO> listarTransacoesUsuario(Usuario usuario) {

		List<TransacaoFinanceiraDTO> resultList = new ArrayList<TransacaoFinanceiraDTO>();

		List<TransacaoFinanceira> transacoesEntity = transacaoFinanceiraDAO
				.listaTransacaoUsuario(usuario.getId());

		Converter<TransacaoFinanceira, TransacaoFinanceiraDTO> converter = new TransacaoFinanceiraConverter();

		for (TransacaoFinanceira trx : transacoesEntity) {
			resultList.add(converter.getDTOFromEntity(trx));
		}

		return resultList;
	}

	@Override
	public void inserirNovaSolicitacaoSaque(Long idUsuario, String valorTrx,
			String agencia, String nomeBanco, String nomeTitular,
			String numeroConta, String cpf) throws VsPlayersException {

		Usuario user = usuarioDAO.findById(idUsuario);

		TransacaoFinanceira trxSolic = new TransacaoFinanceira();
		trxSolic.setAprovada(Flag.N);
		trxSolic.setUsuario(user);
		trxSolic.setDtTransacao(new Date());
		trxSolic.setNmTransacao("Solicitação de Saque");
		try {
			trxSolic.setValorTransacao(new BigDecimal(StringUtils
					.stringMonetarioToDouble(valorTrx)));
		} catch (ParseException e1) {
			throw new VsPlayersException(e1);
		}
		trxSolic.setTipoTransacao(TipoTransacao.SLSA);

		DetalheSolicitacaoSaque detalhe = new DetalheSolicitacaoSaque();

		try {
			detalhe.setTipoBanco(TipoBanco.getByString(nomeBanco));
		} catch (VsPlayersException e) {
			throw e;
		}

		detalhe.setAgencia(agencia);
		detalhe.setConta(numeroConta);
		detalhe.setCpf(cpf);

		trxSolic.setDetalheSolic(new ArrayList<DetalheSolicitacaoSaque>());
		trxSolic.getDetalheSolic().add(detalhe);

		BigDecimal saldoAtual = this.verificaSaldoUsuario(user.getId());

		if (saldoAtual.doubleValue() >= trxSolic.getValorTransacao()
				.doubleValue()) {
			transacaoFinanceiraDAO.gravarNovaTransacao(trxSolic);
		} else {
			throw new VsPlayersException(null);
		}
	}

	@Override
	public List<TransacaoFinanceiraDTO> listarTransacoesTodosUsuarios() {

		List<TransacaoFinanceiraDTO> resultList = new ArrayList<TransacaoFinanceiraDTO>();

		List<TransacaoFinanceira> transacoesEntity = transacaoFinanceiraDAO
				.listaTransacaoTodosUsuarios();

		Converter<TransacaoFinanceira, TransacaoFinanceiraDTO> converter = new TransacaoFinanceiraConverter();

		for (TransacaoFinanceira trx : transacoesEntity) {
			resultList.add(converter.getDTOFromEntity(trx));
		}

		return resultList;
	}

	@Override
	public BigDecimal verificaSaldoUsuario(Long idUsuario) {

		BigDecimal saldoPositivo = transacaoFinanceiraDAO
				.getSaldoPositivoUsuario(idUsuario);
		BigDecimal saldoNegativo = transacaoFinanceiraDAO
				.getSaldoNegativoUsuario(idUsuario);

		return new BigDecimal(saldoPositivo.doubleValue()
				- saldoNegativo.doubleValue());
	}

	@Override
	public TransacaoFinanceiraDTO getDadosDeposito(Long idSolicitacao) {

		Converter<TransacaoFinanceira, TransacaoFinanceiraDTO> converter = new TransacaoFinanceiraComDetalheConverter();
		TransacaoFinanceira trx = transacaoFinanceiraDAO
				.getByIdInitDetalheSolic(idSolicitacao);

		return converter.getDTOFromEntity(trx);
	}

	@Override
	public void confirmDeposito(Long idSolicitacao) {

		TransacaoFinanceira trx = transacaoFinanceiraDAO
				.getByIdInitDetalheSolic(idSolicitacao);

		trx.setTipoTransacao(TipoTransacao.SAQUE);
		trx.setDtAprovacao(new Date());
		trx.setAprovada(Flag.S);

		transacaoFinanceiraDAO.save(trx);
	}

	@Override
	public List<TransacaoFinanceiraDTO> listarTransacoesTodosUsuariosAprovadas() {

		List<TransacaoFinanceiraDTO> resultList = new ArrayList<TransacaoFinanceiraDTO>();

		List<TransacaoFinanceira> transacoesEntity = transacaoFinanceiraDAO
				.listaTransacaoTodosUsuariosAprovadas();

		Converter<TransacaoFinanceira, TransacaoFinanceiraDTO> converter = new TransacaoFinanceiraConverter();

		for (TransacaoFinanceira trx : transacoesEntity) {
			resultList.add(converter.getDTOFromEntity(trx));
		}

		return resultList;
	}

	@Override
	public void inserirNovoDeposito(TransacaoFinanceira trx) {
		transacaoFinanceiraDAO.save(trx);
	}

	@Override
	public void confirmaTransacao(Long idTransacao, String token) {

		TransacaoFinanceira trx = transacaoFinanceiraDAO
				.getByIdInitDetalheSolic(idTransacao);

		trx.setTokenAprovacao(token);
		trx.setTipoTransacao(TipoTransacao.DEPOS);
		trx.setDtAprovacao(new Date());
		trx.setAprovada(Flag.S);

		transacaoFinanceiraDAO.save(trx);
	}

	@Override
	public TransacaoFinanceira getById(Long id) {
		return transacaoFinanceiraDAO.getByIdInitDetalheSolic(id);
	}

}
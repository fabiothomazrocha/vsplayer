package br.com.vsplayers.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.converter.Converter;
import br.com.vsplayers.domain.Faq;
import br.com.vsplayers.dto.faq.FaqDTO;
import br.com.vsplayers.exception.VsPlayersException;
import br.com.vsplayers.infra.dao.FaqDAO;
import br.com.vsplayers.service.FaqService;

@Transactional
@Service
@Qualifier("FaqServiceImpl")
public class FaqServiceImpl implements FaqService {

	@Autowired
	@Qualifier("FaqDAOImpl")
	private FaqDAO faqDAO;

	@Autowired
	@Qualifier("FaqConverter")
	private Converter<Faq, FaqDTO> converter;

	@Override
	public List<FaqDTO> listarTodosFaqs() {
		List<FaqDTO> returnList = new ArrayList<FaqDTO>();
		List<Faq> listaFaqs = faqDAO.listaTodosFaqs();
		for (Faq faq : listaFaqs) {
			returnList.add(converter.getDTOFromEntity(faq));
		}
		return returnList;
	}
	@Override
	public void inserirFaq(Faq novoFaq) throws VsPlayersException {
		faqDAO.inserirFaq(novoFaq);
	}

	@Override
	public Boolean deleteFaq(Long idFaqSelecionado) {
		return faqDAO.deleteFaq(idFaqSelecionado);
	}
	@Override
	public void atualizarFaq(Faq faq) throws VsPlayersException {
		faqDAO.atualizarFaq(faq);
	}
	@Override
	public Faq getById(Long idFaqSelecionado) {
		return faqDAO.getById(idFaqSelecionado);
	}
	@Override
	public Faq getByName(String nome) {
		return faqDAO.getByName(nome);
	}

}

package br.com.vsplayers.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.converter.Converter;
import br.com.vsplayers.domain.SalaJogos;
import br.com.vsplayers.dto.salajogos.SalaJogosDTO;
import br.com.vsplayers.exception.VsPlayersException;
import br.com.vsplayers.infra.dao.SalaJogosDAO;
import br.com.vsplayers.service.SalaJogosService;

@Service
@Transactional
@Qualifier("SalaJogosServiceImpl")
public class SalaJogosServiceImpl implements SalaJogosService {
	@Autowired
	@Qualifier("SalaJogosDAOImpl")
	private SalaJogosDAO salaJogosDAO;

	@Autowired
	@Qualifier("SalaJogosConverter")
	private Converter<SalaJogos, SalaJogosDTO> converter;

	@Override
	public List<SalaJogosDTO> listarTodosSalaJogos() {
		List<SalaJogosDTO> returnList = new ArrayList<SalaJogosDTO>();

		for (SalaJogos salaJogos : salaJogosDAO.listaTodosSalaJogos()) {
			returnList.add(converter.getDTOFromEntity(salaJogos));
		}

		return returnList;
	}

	@Override
	public List<SalaJogos> getAllSalaJogos() {

		return salaJogosDAO.listaTodosSalaJogos();
	}

	@Override
	public void inserirSalaJogos(SalaJogos novoSalaJogos)
			throws VsPlayersException {
		salaJogosDAO.inserirSalaJogos(novoSalaJogos);

	}

	@Override
	public Boolean deleteSalaJogos(Long idSalaJogosSelecionado) {

		return salaJogosDAO.deleteSalaJogos(idSalaJogosSelecionado);
	}

	@Override
	public void atualizarSalaJogos(SalaJogos salaJogos)
			throws VsPlayersException {
		salaJogosDAO.atualizarSalaJogos(salaJogos);

	}

	@Override
	public SalaJogos getById(Long idSalaJogosSelecionado) {

		return salaJogosDAO.getById(idSalaJogosSelecionado);
	}

}

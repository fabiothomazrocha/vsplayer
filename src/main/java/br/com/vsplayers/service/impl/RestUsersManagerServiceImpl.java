package br.com.vsplayers.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Singleton;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import br.com.vsplayers.service.RestUsersManagerService;

@Singleton
@Service
@Qualifier("RestUsersManagerServiceImpl")
public class RestUsersManagerServiceImpl implements RestUsersManagerService {
	
	private Map<String, Object> authUsers = new HashMap<>();
	
	@Override
	public Boolean isUserAuhorized(String token) {
		return (authUsers.get(token) != null);
	}

	@Override
	public Boolean insertUserAuth(String token) {
		authUsers.put(token, new Object());
		return true;
	}
	
}
package br.com.vsplayers.service.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.domain.Papel;
import br.com.vsplayers.domain.Usuario;
import br.com.vsplayers.integration.beans.VsPlayersUserDetail;
import br.com.vsplayers.service.UsuarioAssembler;

@Service("UsuarioAssemblerImpl")
@Qualifier("UsuarioAssemblerImpl")
public class UsuarioAssemblerImpl implements UsuarioAssembler {

	@Override
	@Transactional(readOnly = true)
	public VsPlayersUserDetail buildUserFromUserEntity(Usuario usuario) {

		String username = usuario.getDsEmail();
		String password = usuario.getDsSenha();
		boolean enabled = true;
		boolean accountNonExpired = true;
		boolean credentialsNonExpired = true;
		boolean accountNonLocked = true;

		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		for (Papel papel : usuario.getPapeis()) {
			authorities.add(new SimpleGrantedAuthority(papel.getNmNome()));
		}

		VsPlayersUserDetail user = new VsPlayersUserDetail(usuario, username, password,
				enabled, accountNonExpired, credentialsNonExpired,
				accountNonLocked, authorities);

		return user;
	}
}
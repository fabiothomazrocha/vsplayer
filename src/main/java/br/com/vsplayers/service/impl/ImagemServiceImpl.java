package br.com.vsplayers.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.domain.Imagem;
import br.com.vsplayers.infra.dao.ImagemDAO;
import br.com.vsplayers.service.ImagemService;

@Transactional
@Service
@Qualifier("ImagemServiceImpl")
public class ImagemServiceImpl implements ImagemService {
	
	@Autowired
	@Qualifier("ImagemDAOImpl")
	private ImagemDAO imagemDAO;

	@Transactional
	@Override
	public void inserirImagem(Imagem img) {
		imagemDAO.inserirImagem(img);
	}

	@Override
	public Imagem loadImagemByIdPerfil(Long id) {
		return imagemDAO.loadImagemByIdPerfil(id);
	}

	@Override
	public Boolean removerImagem(Long id) {
		return imagemDAO.removeImagem(id);
	}

}

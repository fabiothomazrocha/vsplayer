package br.com.vsplayers.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.converter.Converter;
import br.com.vsplayers.domain.Jogo;
import br.com.vsplayers.dto.jogo.JogoDTO;
import br.com.vsplayers.exception.VsPlayersException;
import br.com.vsplayers.infra.dao.JogoDAO;
import br.com.vsplayers.service.JogoService;

@Service
@Transactional
@Qualifier("JogoServiceImpl")
public class JogoServiceImpl implements JogoService {

	@Autowired
	@Qualifier("JogoDAOImpl")
	private JogoDAO jogoDAO;

	@Autowired
	@Qualifier("JogoConverter")
	private Converter<Jogo, JogoDTO> converter;

	@Override
	public List<JogoDTO> listarTodosJogos() {

		List<JogoDTO> returnList = new ArrayList<JogoDTO>();

		List<Jogo> listaJogos = jogoDAO.listaTodosJogos();

		for (Jogo jogo : listaJogos) {
			returnList.add(converter.getDTOFromEntity(jogo));
		}

		return returnList;
	}

	@Override
	public void atualizarJogo(Jogo jogo) throws VsPlayersException {
		jogoDAO.atualizarJogo(jogo);

	}

	@Override
	public void inserirJogo(Jogo novoJogo) {
		jogoDAO.inserirJogo(novoJogo);

	}

	@Override
	public Jogo getById(Long idJogoSelecionado) {

		return jogoDAO.getById(idJogoSelecionado);
	}
	@Override
	public Boolean deleteJogo(Long idJogoSelecionado) {
		// TODO Auto-generated method stub
		return jogoDAO.deleteJogo(idJogoSelecionado);
	}
}
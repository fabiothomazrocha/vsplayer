package br.com.vsplayers.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.domain.Regra;
import br.com.vsplayers.exception.VsPlayersException;
import br.com.vsplayers.infra.dao.RegraDao;
import br.com.vsplayers.service.RegrasService;

@Service
@Transactional
@Qualifier("RegrasServiceImpl")

public class RegrasServiceImpl implements RegrasService {
	@Autowired
	@Qualifier("RegraDAOImpl")
	private RegraDao regraDAO;

	@Override
	public List<Regra> listarRegras(Long idUsuario) {
		List<Regra> returnList = new ArrayList<Regra>();

		List<Regra> result = regraDAO.listarRegra(idUsuario);

		for (Regra regra : result) {
			returnList.add(regra);
		}

		return returnList;
	}

	@Override
	public void inserirRegra(Regra regra) throws VsPlayersException {

		regraDAO.inserirRegra(regra);

	}

	@Override
	public Integer countRegras(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void atualizarRegra(Regra regra) throws VsPlayersException {

		regraDAO.atualizarRegra(regra);

	}

	@Override
	public Boolean deleteRegra(Long idRegraSelecionado) {
		// TODO Auto-generated method stub
		return regraDAO.deleteRegra(idRegraSelecionado);
	}

	@Override
	public Regra getById(Long idRegraSelecionado) {

		return regraDAO.getById(idRegraSelecionado);
	}

}

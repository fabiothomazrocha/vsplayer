package br.com.vsplayers.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.converter.Converter;
import br.com.vsplayers.domain.AcessoRedeJogo;
import br.com.vsplayers.dto.acessoredejogo.AcessoRedeJogoDTO;
import br.com.vsplayers.infra.dao.AcessoRedeJogoDAO;
import br.com.vsplayers.service.AcessoRedeJogoService;

@Transactional
@Service
@Qualifier("AcessoRedeJogoServiceImpl")
public class AcessoRedeJogoServiceImpl implements AcessoRedeJogoService {

	@Autowired
	@Qualifier("AcessoRedeJogoDAOImpl")
	private AcessoRedeJogoDAO acessoRedeJogoDAO;

	@Autowired
	@Qualifier("AcessoRedeJogoConverter")
	Converter<AcessoRedeJogo, AcessoRedeJogoDTO> converter;

	@Override
	public List<AcessoRedeJogoDTO> listAllByUser(Long userId) {

		List<AcessoRedeJogoDTO> returnList = new ArrayList<AcessoRedeJogoDTO>();

		List<AcessoRedeJogo> queryResult = acessoRedeJogoDAO
				.listAllByUser(userId);

		for (AcessoRedeJogo acs : queryResult) {
			returnList.add(converter.getDTOFromEntity(acs));
		}

		return returnList;
	}

	@Override
	public void deletePerfil(Long idAcessoRedeJogo) {
		acessoRedeJogoDAO.desativar(idAcessoRedeJogo);
	}

	@Override
	public void inserirNovoAcessoRede(AcessoRedeJogo acs) {
		acessoRedeJogoDAO.persistNewEntity(acs);
	}

	@Override
	public Integer countAcessoRedeAtivoUsuario(Long idUsuario) {
		return acessoRedeJogoDAO.countAcessoRedeAtivoUsuario(idUsuario);
	}

}

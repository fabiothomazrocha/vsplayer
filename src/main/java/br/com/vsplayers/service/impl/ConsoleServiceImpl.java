package br.com.vsplayers.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.converter.Converter;
import br.com.vsplayers.domain.Console;
import br.com.vsplayers.domain.Jogo;
import br.com.vsplayers.domain.enums.TipoAcessoRedeJogo;
import br.com.vsplayers.domain.enums.TipoConsole;
import br.com.vsplayers.dto.console.ConsoleDTO;
import br.com.vsplayers.exception.VsPlayersException;
import br.com.vsplayers.infra.dao.ConsoleDAO;
import br.com.vsplayers.service.ConsoleService;

@Transactional
@Service
@Qualifier("ConsoleServiceImpl")
public class ConsoleServiceImpl implements ConsoleService {

	@Autowired
	@Qualifier("ConsoleDAOImpl")
	private ConsoleDAO consoleDAO;

	@Autowired
	@Qualifier("ConsoleConverter")
	private Converter<Console, ConsoleDTO> converter;

	@Override
	public List<ConsoleDTO> listarConsolePorTipoAcesso(
			TipoAcessoRedeJogo tipoAcesso) {
		List<ConsoleDTO> returnList = new ArrayList<ConsoleDTO>();
		List<Console> listaConsoleRede = consoleDAO
				.listarConsolesPorTipoRede(tipoAcesso);
		for (Console c : listaConsoleRede) {
			returnList.add(converter.getDTOFromEntity(c));
		}
		return returnList;
	}

	@Override
	public List<Console> getConsolesByConsoleNames(
			List<String> consolesSelecionados) {
		List<TipoConsole> listaTipoConsole = new ArrayList<TipoConsole>();
		for(String s: consolesSelecionados) {
			for(TipoConsole tc: TipoConsole.values()) {
				if(tc.name().equals(s)) {
					listaTipoConsole.add(tc);
					break;
				}
			}
		}
		
		return consoleDAO.getConsolesByConsoleNames(listaTipoConsole);
	}

	@Override
	public List<ConsoleDTO> listaTodosConsoles() {
		List<ConsoleDTO> returnList = new ArrayList<ConsoleDTO>();
		List<Console> listaConsoles = consoleDAO.listaTodosConsoles();
		for(Console console: listaConsoles) {
			returnList.add(converter.getDTOFromEntity(console));
		}
		return returnList;
	}
	
	@Override
	public void atualizarConsole(Console console) throws VsPlayersException {
		consoleDAO.atualizarConsole(console);

	}

	@Override
	public void inserirConsole(Console novoConsole) {
		consoleDAO.inserirConsole(novoConsole);

	}

	@Override
	public Console ConsolegetById(Long idConsoleSelecionado) {
		return consoleDAO.getById(idConsoleSelecionado);
	}
	@Override
	public Boolean deleteConsole(Long idConsoleSelecionado) {
		return consoleDAO.deleteConsole(idConsoleSelecionado);
	}
		
	
}
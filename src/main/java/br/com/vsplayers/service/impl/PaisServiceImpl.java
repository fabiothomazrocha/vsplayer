package br.com.vsplayers.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.converter.Converter;
import br.com.vsplayers.domain.Pais;
import br.com.vsplayers.dto.pais.PaisDTO;
import br.com.vsplayers.exception.VsPlayersException;
import br.com.vsplayers.infra.dao.PaisDAO;
import br.com.vsplayers.service.PaisService;

@Service
@Transactional
@Qualifier("PaisServiceImpl")
public class PaisServiceImpl implements PaisService {
	@Autowired
	@Qualifier("PaisDAOImpl")
	private PaisDAO paisDAO;

	@Autowired
	@Qualifier("PaisConverter")
	private Converter<Pais, PaisDTO> converter;

	@Override
	public List<PaisDTO> listarTodosPaises() {
		List<PaisDTO> returnList = new ArrayList<PaisDTO>();
		List<Pais> listapaises = paisDAO.listarTodosPaises();
		for (Pais pais : listapaises) {
			returnList.add(converter.getDTOFromEntity(pais));
		}
		return returnList;
	}

	@Override
	public void inserirPais(Pais novoPais) throws VsPlayersException {
		paisDAO.inserirPais(novoPais);
	}

	@Override
	public Boolean deletePais(Long idPaisSelecionado) {

		return paisDAO.deletePais(idPaisSelecionado);
	}

	@Override
	public void atualizarPais(Pais pais) throws VsPlayersException {
		paisDAO.atualizarPais(pais);

	}

	@Override
	public Pais getById(Long idPaisSelecionado) {

		return paisDAO.getById(idPaisSelecionado);
	}

	@Override
	public Pais getByName(String nome) {

		return paisDAO.getByName(nome);
	}

}

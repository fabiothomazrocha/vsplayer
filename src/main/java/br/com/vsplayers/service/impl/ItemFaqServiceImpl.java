package br.com.vsplayers.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.converter.Converter;
import br.com.vsplayers.domain.ItemFaq;
import br.com.vsplayers.dto.itemfaq.ItemFaqDTO;
import br.com.vsplayers.exception.VsPlayersException;
import br.com.vsplayers.infra.dao.ItemFaqDAO;
import br.com.vsplayers.service.ItemFaqService;

@Service
@Transactional
@Qualifier("ItemFaqServiceImpl")
public class ItemFaqServiceImpl implements ItemFaqService {

	@Autowired
	@Qualifier("ItemFaqDAOImpl")
	private ItemFaqDAO itemFaqDAO;

	@Autowired
	@Qualifier("ItemFaqConverter")
	private Converter<ItemFaq, ItemFaqDTO> converter;

	@Override
	public List<ItemFaqDTO> listarTodosItemFaqs() {
		List<ItemFaqDTO> returnList = new ArrayList<ItemFaqDTO>();
		List<ItemFaq> listaItemFaqs = itemFaqDAO.listaTodosItemFaqs();
		
		for (ItemFaq itemFaq : listaItemFaqs) {
			returnList.add(converter.getDTOFromEntity(itemFaq));
		}
		
		return returnList;
	}

	@Override
	public void inserirItemFaq(ItemFaq novoItemFaq) throws VsPlayersException {
		itemFaqDAO.inserirItemFaq(novoItemFaq);
	}

	@Override
	public Boolean deleteItemFaq(Long idItemFaqSelecionado) {
		return itemFaqDAO.deleteItemFaq(idItemFaqSelecionado);
	}

	@Override
	public void atualizarItemFaq(ItemFaq itemFaq) throws VsPlayersException {
		itemFaqDAO.atualizarFaq(itemFaq);
	}

	@Override
	public ItemFaq getById(Long idItemFaqSelecionado) {
		return itemFaqDAO.getById(idItemFaqSelecionado);
	}

}

package br.com.vsplayers.service.impl;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.vsplayers.converter.Converter;
import br.com.vsplayers.domain.Imagem;
import br.com.vsplayers.domain.PerfilUsuario;
import br.com.vsplayers.domain.Usuario;
import br.com.vsplayers.domain.enums.Flag;
import br.com.vsplayers.domain.enums.TipoImagem;
import br.com.vsplayers.dto.perfil.PerfilUsuarioDTO;
import br.com.vsplayers.exception.VsPlayersException;
import br.com.vsplayers.infra.dao.PerfilDAO;
import br.com.vsplayers.service.ImagemService;
import br.com.vsplayers.service.PerfilService;
import br.com.vsplayers.service.UsuarioService;
import br.com.vsplayers.service.ioutils.VsPlayersIOUtils;

@Service
@Transactional
@Qualifier("PerfilServiceImpl")
public class PerfilServiceImpl implements PerfilService {

	@Autowired
	@Qualifier("PerfilDAOImpl")
	private PerfilDAO perfilDAO;

	@Autowired
	@Qualifier("UsuarioServiceImpl")
	private UsuarioService usuarioService;

	@Autowired
	@Qualifier("ImagemServiceImpl")
	private ImagemService imagemService;

	@Autowired
	@Qualifier("PerfilUsuarioConverter")
	private Converter<PerfilUsuario, PerfilUsuarioDTO> perfilUsuarioConverter;

	@Transactional(readOnly = true)
	@Override
	public List<PerfilUsuarioDTO> listarPerfisUsuario(Long idUsuario) {

		List<PerfilUsuarioDTO> returnList = new ArrayList<PerfilUsuarioDTO>();

		List<PerfilUsuario> result = perfilDAO.listarPerfisUsuario(idUsuario);

		for (PerfilUsuario perfil : result) {
			returnList.add(perfilUsuarioConverter.getDTOFromEntity(perfil));
		}

		return returnList;
	}

	@Transactional
	@Override
	public void inserirPerfil(String apelido, String sobre, Long idUsuario,
			boolean temImagem, String path, BufferedImage image)
			throws VsPlayersException {

		PerfilUsuario novoPerfil = new PerfilUsuario();
		novoPerfil.setApelido(apelido);
		novoPerfil.setDsSobre(sobre);
		novoPerfil.setDtAtualizacao(new Date());
		novoPerfil.setDtCriacao(new Date());
		novoPerfil.setAtivo(Flag.S);

		Usuario usuario = usuarioService.getById(idUsuario);
		novoPerfil.setUsuario(usuario);

		Imagem img = null;

		if (temImagem) {
			img = new Imagem();
			img.setNmArquivo(apelido + "_profile.png");
			img.setDtAtualizacao(new Date());
			img.setDtCriacao(new Date());
			img.setTipoImagem(TipoImagem.PER);
			img.setPerfilUsuario(novoPerfil);
			novoPerfil.setImagemPerfil(img);

			this.inserirFotoPerfil(apelido, image, path);
		}

		perfilDAO.inserirPerfil(novoPerfil);

	}

	public void inserirFotoPerfil(String apelido, BufferedImage imgPerfil,
			String path) throws VsPlayersException {
		VsPlayersIOUtils.writeImg2Disk(imgPerfil, path + File.separator
				+ apelido + "_profile.png");
	}

	@Override
	public Boolean verificaApelidoJaExiste(String apelido) {

		return perfilDAO.verificaApelidoJaExiste(apelido);
	}

	@Override
	public PerfilUsuario getById(Long idPerfilSelecionado) {
		return perfilDAO.getByIdInitImagemPerfil(idPerfilSelecionado);
	}

	@Override
	public Boolean deletePerfil(Long idPerfilSelecionado) {
		return perfilDAO.deletePerfil(idPerfilSelecionado);
	}

	@Override
	public void atualizarPerfil(PerfilUsuario perfil,
			Boolean temImagemCadastrada, Boolean removerImagem,
			Boolean alterarImagem, String realPath, Image image)
			throws VsPlayersException {

		if (temImagemCadastrada && !removerImagem && alterarImagem) {

			Imagem img = perfil.getImagemPerfil();
			img.setNmArquivo(perfil.getApelido() + "_profile.png");
			img.setDtAtualizacao(new Date());

			this.inserirFotoPerfil(perfil.getApelido(), (BufferedImage) image,
					realPath);
		} else {

			if (!temImagemCadastrada && alterarImagem) {
				if (image != null) {

					Imagem img = new Imagem();
					img.setNmArquivo(perfil.getApelido() + "_profile.png");
					img.setDtAtualizacao(new Date());
					img.setDtCriacao(new Date());
					img.setTipoImagem(TipoImagem.PER);
					img.setPerfilUsuario(perfil);
					perfil.setImagemPerfil(img);

					this.inserirFotoPerfil(perfil.getApelido(),
							(BufferedImage) image, realPath);
				}
			}
		}

		if (removerImagem) {

			Imagem img = perfil.getImagemPerfil();
			String nmArquivo = img.getNmArquivo();
			perfil.setImagemPerfil(null);

			VsPlayersIOUtils.removeImgFromDisk(realPath + File.separator
					+ nmArquivo);
		}

		perfilDAO.atualizarPerfil(perfil);

	}

	@Override
	public PerfilUsuarioDTO getDTOById(Long idPerfilSelecionado) {
		
		PerfilUsuario perfil = perfilDAO
				.getByIdInitImagemPerfil(idPerfilSelecionado);
		
		if (perfil != null) {
			return perfilUsuarioConverter.getDTOFromEntity(perfil);
		}
		
		return null;
	}

	@Override
	public Integer countPerfisAtivosUsuario(Long idUsuario) {
		return perfilDAO.countPerfisAtivosUsuario(idUsuario);
	}

}
package br.com.vsplayers.service;

import java.util.List;

import br.com.vsplayers.domain.Pais;
import br.com.vsplayers.dto.pais.PaisDTO;
import br.com.vsplayers.exception.VsPlayersException;

public interface PaisService {
	public List<PaisDTO> listarTodosPaises();

	public void inserirPais(Pais novoPais) throws VsPlayersException;

	Boolean deletePais(Long idPaisSelecionado);

	void atualizarPais(Pais pais) throws VsPlayersException;

	Pais getById(Long idPaisSelecionado);

	public Pais getByName(String nome);
}

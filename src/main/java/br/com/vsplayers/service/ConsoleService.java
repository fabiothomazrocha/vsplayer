package br.com.vsplayers.service;

import java.util.List;

import br.com.vsplayers.domain.Console;
import br.com.vsplayers.domain.enums.TipoAcessoRedeJogo;
import br.com.vsplayers.dto.console.ConsoleDTO;
import br.com.vsplayers.exception.VsPlayersException;

public interface ConsoleService {

	public List<ConsoleDTO> listarConsolePorTipoAcesso(
			TipoAcessoRedeJogo tipoAcesso);

	public List<Console> getConsolesByConsoleNames(
			List<String> consolesSelecionados);

	public List<ConsoleDTO> listaTodosConsoles();

	void atualizarConsole(Console console) throws VsPlayersException;

	void inserirConsole(Console novoConsole)throws VsPlayersException;;

	Console ConsolegetById(Long idConsoleSelecionado);

	Boolean deleteConsole(Long idConsoleSelecionado);

}
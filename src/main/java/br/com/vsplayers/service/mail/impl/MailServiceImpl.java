package br.com.vsplayers.service.mail.impl;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import br.com.vsplayers.service.mail.MailService;

@Service("MailServiceImpl")
public class MailServiceImpl implements MailService {

	private static final Logger logger = LoggerFactory
			.getLogger(MailServiceImpl.class);

	@Autowired
	@Qualifier("VSPlayersMailSenderImpl")
	private JavaMailSenderImpl mailSender;

	@Override
	public void sendMail(String from, String[] to, String subject,
			String body) throws Exception {

		InternetAddress toAddrs[] = new InternetAddress[to.length];

		for (int i = 0; i < to.length; i++) {
			if (!to[i].equals("")) {
				toAddrs[i] = new InternetAddress(to[i]);
				System.out.println(toAddrs[i]);
			}
		}

		InternetAddress fromAddr = new InternetAddress(from);

		MimeMessage message = mailSender.createMimeMessage();
		message.setHeader("Content-Transfer-Encoding", "ISO-8859-1");
		message.setFrom(fromAddr);
		message.setRecipients(javax.mail.Message.RecipientType.TO, toAddrs);
		message.setSubject(subject);
		message.setContent(body, "text/html; charset=\"ISO-8859-1\"");

		mailSender.send(message);

		logger.info("E-MAIL SENT TO: " + to.toString() + ".\r\n["
				+ message.toString() + "]");

	}

}

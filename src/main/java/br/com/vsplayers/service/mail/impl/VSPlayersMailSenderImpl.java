package br.com.vsplayers.service.mail.impl;

import java.util.Properties;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

@Configuration
@Service("VSPlayersMailSenderImpl")
@PropertySource("classpath:email.properties")
public class VSPlayersMailSenderImpl extends JavaMailSenderImpl {
	
	@Autowired
	private Environment env;

	public VSPlayersMailSenderImpl() {
		super();
	}
	
	@PostConstruct
	public void init() {
		super.setHost(env.getProperty("mail.host"));
		super.setPort(Integer.valueOf(env.getProperty("mail.port")));
		super.setUsername(env.getProperty("mail.username"));
		super.setPassword(env.getProperty("mail.password"));

		Properties mailProps = new Properties();
		mailProps.setProperty("mail.transport.protocol", "smtp");
		mailProps.setProperty("mail.smtp.auth", "true");
		mailProps.setProperty("mail.smtp.starttls.enable", "true");
		mailProps.setProperty("mail.debug", "true");

		super.setJavaMailProperties(mailProps);
	}
	
}
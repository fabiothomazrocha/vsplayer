package br.com.vsplayers.service.mail;

public interface MailService {

	public void sendMail(String from, String[] enderecos, String subject,
			String body) throws Exception;

}

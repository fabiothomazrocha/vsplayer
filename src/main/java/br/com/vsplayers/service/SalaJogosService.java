package br.com.vsplayers.service;

import java.util.List;

import br.com.vsplayers.domain.SalaJogos;
import br.com.vsplayers.dto.salajogos.SalaJogosDTO;
import br.com.vsplayers.exception.VsPlayersException;

public interface SalaJogosService {

	public List<SalaJogosDTO> listarTodosSalaJogos();

	public void inserirSalaJogos(SalaJogos novoSalaJogos) throws VsPlayersException;

	

	void atualizarSalaJogos(SalaJogos salaJogos) throws VsPlayersException;

	SalaJogos getById(Long idSalaJogosSelecionado);

	Boolean deleteSalaJogos(Long idSalaJogosSelecionado);

	List<SalaJogos> getAllSalaJogos();
	
	
	
}

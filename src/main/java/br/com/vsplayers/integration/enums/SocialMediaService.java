package br.com.vsplayers.integration.enums;

public enum SocialMediaService {
    FACEBOOK,
    TWITTER
}
package br.com.vsplayers.integration.enums;

public enum Role {
	
	ADMINISTRADOR, USUARIO, USUARIO_SOCIAL_FACEBOOK;
	
}
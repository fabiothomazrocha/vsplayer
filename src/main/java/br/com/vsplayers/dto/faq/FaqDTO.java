package br.com.vsplayers.dto.faq;

import java.util.Date;
import java.util.List;

import br.com.vsplayers.dto.itemfaq.ItemFaqDTO;

public class FaqDTO {

	private Long id;
	private String nmFaq;
	private Date dtAtualizacao;
	private List<ItemFaqDTO> listaItemFAQ;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmFaq() {
		return nmFaq;
	}

	public void setNmFaq(String nmFaq) {
		this.nmFaq = nmFaq;
	}

	public Date getDtAtualizacao() {
		return dtAtualizacao;
	}

	public void setDtAtualizacao(Date dtAtualizacao) {
		this.dtAtualizacao = dtAtualizacao;
	}

	public List<ItemFaqDTO> getListaItemFAQ() {
		return listaItemFAQ;
	}

	public void setListaItemFAQ(List<ItemFaqDTO> listaItemFAQ) {
		this.listaItemFAQ = listaItemFAQ;
	}

}

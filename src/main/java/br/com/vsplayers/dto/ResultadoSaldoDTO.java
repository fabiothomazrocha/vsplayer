package br.com.vsplayers.dto;

import java.io.Serializable;

public class ResultadoSaldoDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String saldo;

	public String getSaldo() {
		return saldo;
	}

	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}

}
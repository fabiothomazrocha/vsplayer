package br.com.vsplayers.dto.rest;

import java.io.Serializable;

public class AutenticacaoRestDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String tokenSeguranca;
	
	public String getTokenSeguranca() {
		return tokenSeguranca;
	}
	
	public void setTokenSeguranca(String tokenSeguranca) {
		this.tokenSeguranca = tokenSeguranca;
	}

}

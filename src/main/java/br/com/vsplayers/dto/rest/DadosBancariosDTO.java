package br.com.vsplayers.dto.rest;

public class DadosBancariosDTO {
	
	private String nomeBanco;
	private String agencia;
	private String contaCorrente;
	private String valorRetirada;
	private String cpf;
	
	public String getNomeBanco() {
		return nomeBanco;
	}
	public void setNomeBanco(String nomeBanco) {
		this.nomeBanco = nomeBanco;
	}
	public String getAgencia() {
		return agencia;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public String getContaCorrente() {
		return contaCorrente;
	}
	public void setContaCorrente(String contaCorrente) {
		this.contaCorrente = contaCorrente;
	}
	public String getValorRetirada() {
		return valorRetirada;
	}
	public void setValorRetirada(String valorRetirada) {
		this.valorRetirada = valorRetirada;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}
package br.com.vsplayers.dto.console;

import br.com.vsplayers.domain.enums.TipoAcessoRedeJogo;
import br.com.vsplayers.domain.enums.TipoConsole;

public class ConsoleDTO {

	private String nomeConsole;
	private Long idConsole;
	private TipoConsole tipoConsole;
	private TipoAcessoRedeJogo tipoRede;

	public TipoAcessoRedeJogo getTipoRede() {
		return tipoRede;
	}

	public void setTipoRede(TipoAcessoRedeJogo tipoRede) {
		this.tipoRede = tipoRede;
	}

	public String getNomeConsole() {
		return nomeConsole;
	}

	public void setNomeConsole(String nomeConsole) {
		this.nomeConsole = nomeConsole;
	}

	public Long getIdConsole() {
		return idConsole;
	}

	public void setIdConsole(Long idConsole) {
		this.idConsole = idConsole;
	}

	public TipoConsole getTipoConsole() {
		return tipoConsole;
	}

	public void setTipoConsole(TipoConsole tipoConsole) {
		this.tipoConsole = tipoConsole;
	}

}
package br.com.vsplayers.dto.acessoredejogo;

public class AcessoRedeJogoDTO {

	private Long idAcessoRedeJogo;
	private String dataCriacao;
	private String userId;
	private String redeAcesso;
	
	public Long getIdAcessoRedeJogo() {
		return idAcessoRedeJogo;
	}
	public void setIdAcessoRedeJogo(Long idAcessoRedeJogo) {
		this.idAcessoRedeJogo = idAcessoRedeJogo;
	}
	public String getDataCriacao() {
		return dataCriacao;
	}
	public void setDataCriacao(String dataCriacao) {
		this.dataCriacao = dataCriacao;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getRedeAcesso() {
		return redeAcesso;
	}
	public void setRedeAcesso(String redeAcesso) {
		this.redeAcesso = redeAcesso;
	}
	
}

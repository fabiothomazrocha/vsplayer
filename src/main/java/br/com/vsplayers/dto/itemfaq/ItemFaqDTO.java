package br.com.vsplayers.dto.itemfaq;


public class ItemFaqDTO {

	private Long id;
	private String nomeFaq;
	private String dsTituloItem;
	private String txItem;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomeFaq() {
		return nomeFaq;
	}

	public void setNomeFaq(String nomeFaq) {
		this.nomeFaq = nomeFaq;
	}

	public String getDsTituloItem() {
		return dsTituloItem;
	}

	public void setDsTituloItem(String dsTituloItem) {
		this.dsTituloItem = dsTituloItem;
	}

	public String getTxItem() {
		return txItem;
	}

	public void setTxItem(String txItem) {
		this.txItem = txItem;
	}

}

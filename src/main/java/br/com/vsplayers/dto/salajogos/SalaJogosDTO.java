package br.com.vsplayers.dto.salajogos;

import java.math.BigDecimal;
import java.util.Date;

import br.com.vsplayers.domain.enums.TipoConsole;

public class SalaJogosDTO {
	private Long id;
	private BigDecimal valorSala;
	private String nmSalaJogos;
	private Date dtCriacao;
	private Date dtAtualizacao;
	private Integer posicaoSala;
	private String nomeJogo;
	private String nomeConsole;
	private TipoConsole tipoConsole;
	private int qtdUsuario;

	public String getNomeConsole() {
		return nomeConsole;
	}

	public void setNomeConsole(String nomeConsole) {
		this.nomeConsole = nomeConsole;
	}

	public TipoConsole getTipoConsole() {
		return tipoConsole;
	}

	public void setTipoConsole(TipoConsole tipoConsole) {
		this.tipoConsole = tipoConsole;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getValorSala() {
		return valorSala;
	}

	public void setValorSala(BigDecimal valorSala) {
		this.valorSala = valorSala;
	}

	public String getNmSalaJogos() {
		return nmSalaJogos;
	}

	public void setNmSalaJogos(String nmSalaJogos) {
		this.nmSalaJogos = nmSalaJogos;
	}

	public Date getDtCriacao() {
		return dtCriacao;
	}

	public void setDtCriacao(Date dtCriacao) {
		this.dtCriacao = dtCriacao;
	}

	public Date getDtAtualizacao() {
		return dtAtualizacao;
	}

	public void setDtAtualizacao(Date dtAtualizacao) {
		this.dtAtualizacao = dtAtualizacao;
	}

	public Integer getPosicaoSala() {
		return posicaoSala;
	}

	public void setPosicaoSala(Integer posicaoSala) {
		this.posicaoSala = posicaoSala;
	}

	public String getNomeJogo() {
		return nomeJogo;
	}

	public void setNomeJogo(String nomeJogo) {
		this.nomeJogo = nomeJogo;
	}

	public int getQtdUsuario() {
		return qtdUsuario;
	}

	public void setQtdUsuario(int qtdUsuario) {
		this.qtdUsuario = qtdUsuario;
	}

}

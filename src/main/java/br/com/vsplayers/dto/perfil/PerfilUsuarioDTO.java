package br.com.vsplayers.dto.perfil;

public class PerfilUsuarioDTO {
	
	private Long idPerfilUsuario;
	private String dtCriacaoFormatada;
	private String dtAtualizacaoFormatada;
	private String apelido;
	private String sobre;
	private String pathImagemPerfil;
	
	public Long getIdPerfilUsuario() {
		return idPerfilUsuario;
	}
	public void setIdPerfilUsuario(Long idPerfilUsuario) {
		this.idPerfilUsuario = idPerfilUsuario;
	}
	public String getDtCriacaoFormatada() {
		return dtCriacaoFormatada;
	}
	public void setDtCriacaoFormatada(String dtCriacaoFormatada) {
		this.dtCriacaoFormatada = dtCriacaoFormatada;
	}
	public String getDtAtualizacaoFormatada() {
		return dtAtualizacaoFormatada;
	}
	public void setDtAtualizacaoFormatada(String dtAtualizacaoFormatada) {
		this.dtAtualizacaoFormatada = dtAtualizacaoFormatada;
	}
	public String getApelido() {
		return apelido;
	}
	public void setApelido(String apelido) {
		this.apelido = apelido;
	}
	public String getSobre() {
		return sobre;
	}
	public void setSobre(String sobre) {
		this.sobre = sobre;
	}
	public String getPathImagemPerfil() {
		return pathImagemPerfil;
	}
	public void setPathImagemPerfil(String pathImagemPerfil) {
		this.pathImagemPerfil = pathImagemPerfil;
	}
	
}
package br.com.vsplayers.dto;

import br.com.vsplayers.domain.enums.Flag;

public class TransacaoFinanceiraDTO {

	private Long idTransacao;
	private String nmTransacao;
	private String dataTransacao;
	private String dataAprovacao;
	private Flag aprovada;
	private String valorFormatado;
	
	private String nomeUsuario;
	
	// ------------ dados da solicitacao
	private String nomeBancoSolic;
	private String agenciaBancoSolic;
	private String cpfTitularSolic;
	private String numeroContaBancoSolic;
	private String nomeBanco;
	
	public Long getIdTransacao() {
		return idTransacao;
	}
	public void setIdTransacao(Long idTransacao) {
		this.idTransacao = idTransacao;
	}
	public String getNmTransacao() {
		return nmTransacao;
	}
	public void setNmTransacao(String nmTransacao) {
		this.nmTransacao = nmTransacao;
	}
	public String getDataTransacao() {
		return dataTransacao;
	}
	public void setDataTransacao(String dataTransacao) {
		this.dataTransacao = dataTransacao;
	}
	public String getDataAprovacao() {
		return dataAprovacao;
	}
	public void setDataAprovacao(String dataAprovacao) {
		this.dataAprovacao = dataAprovacao;
	}
	public Flag getAprovada() {
		return aprovada;
	}
	public void setAprovada(Flag aprovada) {
		this.aprovada = aprovada;
	}
	public String getValorFormatado() {
		return valorFormatado;
	}
	public void setValorFormatado(String valorFormatado) {
		this.valorFormatado = valorFormatado;
	}
	public String getNomeBancoSolic() {
		return nomeBancoSolic;
	}
	public void setNomeBancoSolic(String nomeBancoSolic) {
		this.nomeBancoSolic = nomeBancoSolic;
	}
	public String getAgenciaBancoSolic() {
		return agenciaBancoSolic;
	}
	public void setAgenciaBancoSolic(String agenciaBancoSolic) {
		this.agenciaBancoSolic = agenciaBancoSolic;
	}
	public String getCpfTitularSolic() {
		return cpfTitularSolic;
	}
	public void setCpfTitularSolic(String cpfTitularSolic) {
		this.cpfTitularSolic = cpfTitularSolic;
	}
	public String getNumeroContaBancoSolic() {
		return numeroContaBancoSolic;
	}
	public void setNumeroContaBancoSolic(String numeroContaBancoSolic) {
		this.numeroContaBancoSolic = numeroContaBancoSolic;
	}
	public String getNomeBanco() {
		return nomeBanco;
	}
	public void setNomeBanco(String nomeBanco) {
		this.nomeBanco = nomeBanco;
	}
	public String getNomeUsuario() {
		return nomeUsuario;
	}
	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

}
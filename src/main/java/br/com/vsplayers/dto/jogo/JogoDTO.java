package br.com.vsplayers.dto.jogo;

import br.com.vsplayers.domain.enums.TipoJogo;

public class JogoDTO {
	private long id;
	private String nomeJogo;
	private TipoJogo tipoJogo;
	private String nomeConsole;
	
	public String getNomeJogo() {
		return nomeJogo;
	}
	public void setNomeJogo(String nomeJogo) {
		this.nomeJogo = nomeJogo;
	}
	public TipoJogo getTipoJogo() {
		return tipoJogo;
	}
	public void setTipoJogo(TipoJogo tipoJogo) {
		this.tipoJogo = tipoJogo;
	}
	public String getNomeConsole() {
		return nomeConsole;
	}
	public void setNomeConsole(String nomeConsole) {
		this.nomeConsole = nomeConsole;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
}

package br.com.vsplayers.application.init;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import br.com.vsplayers.domain.Papel;
import br.com.vsplayers.domain.VsPlayers;
import br.com.vsplayers.infra.dao.PapelDAO;
import br.com.vsplayers.infra.dao.VsPlayersDAO;

@Component
public class AplicacaoIncializadaListenter implements
		ApplicationListener<ContextRefreshedEvent> {

	private final static Logger logger = LoggerFactory
			.getLogger(AplicacaoIncializadaListenter.class);

	@Autowired
	@Qualifier("PapelDAOImpl")
	private PapelDAO papelDAO;

	@Autowired
	@Qualifier("VsPlayersDAOImpl")
	private VsPlayersDAO vsPlayersDAO;

	private boolean executed;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {

		if (executed) {
			return;
		}

		logger.info("#------------ RODANDO APLICATIVO DE INICIALIZACAO ------------#");

		Papel pUsuario = papelDAO.getPapelByName("USUARIO");

		if (pUsuario == null) {
			Papel novoPapel = new Papel();
			novoPapel.setDsDescricao("USUARIO");
			novoPapel.setNmNome("USUARIO");
			papelDAO.inserirPapel(novoPapel);

			logger.info("# APLICATIVO DE INICIALIZACAO inseriu um papel: Usuario");
		}

		Papel pAdmin = papelDAO.getPapelByName("ADMINISTRADOR");

		if (pAdmin == null) {
			Papel novoPapel = new Papel();
			novoPapel.setDsDescricao("ADMINISTRADOR");
			novoPapel.setNmNome("ADMINISTRADOR");
			papelDAO.inserirPapel(novoPapel);

			logger.info("# APLICATIVO DE INICIALIZACAO inseriu um papel: Administrador");
		}

		VsPlayers vsInstance = vsPlayersDAO.getFirstResult();

		if (vsInstance == null) {
			VsPlayers vsPlayers = new VsPlayers();
			vsPlayers.setNmInstancia("INICIALIZADA AUTOMATICAMENTE");
			vsPlayersDAO.inserirNovaInstancia(vsPlayers);
		}
	}

}
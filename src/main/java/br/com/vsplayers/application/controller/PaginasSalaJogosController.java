package br.com.vsplayers.application.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.vsplayers.application.controller.forms.CadastrarSalaJogosForm;
import br.com.vsplayers.domain.AcessoRedeJogo;
import br.com.vsplayers.domain.Console;
import br.com.vsplayers.domain.Jogo;
import br.com.vsplayers.domain.SalaJogos;
import br.com.vsplayers.domain.Usuario;
import br.com.vsplayers.dto.jogo.JogoDTO;
import br.com.vsplayers.dto.salajogos.SalaJogosDTO;
import br.com.vsplayers.exception.VsPlayersException;
import br.com.vsplayers.infra.utils.StringUtils;
import br.com.vsplayers.integration.beans.VsPlayersUserDetail;
import br.com.vsplayers.service.JogoService;
import br.com.vsplayers.service.SalaJogosService;
import br.com.vsplayers.service.UsuarioService;

@Controller
public class PaginasSalaJogosController {
	private static final Logger logger = LoggerFactory
			.getLogger(PaginasSalaJogosController.class);

	@Autowired
	@Qualifier("SalaJogosServiceImpl")
	private SalaJogosService salaJogosService;

	@Autowired
	@Qualifier("UsuarioServiceImpl")
	private UsuarioService usuarioService;

	@Autowired
	@Qualifier("JogoServiceImpl")
	private JogoService jogoService;

	@RequestMapping(value = "/admin/salaJogos/listSalaJogos", method = RequestMethod.GET)
	public ModelAndView listarSalaJogos(HttpServletRequest request) {

		List<SalaJogosDTO> listaSalaJogos = salaJogosService
				.listarTodosSalaJogos();

		ModelAndView returnObj = new ModelAndView(
				"/admin/salaJogos/listSalaJogos");
		returnObj.addObject("listaSalaJogos", listaSalaJogos);

		return returnObj;
	}

	@RequestMapping(value = "/admin/salaJogos/createSalaJogos", method = RequestMethod.GET)
	public ModelAndView cadastrarJogo() {
		ModelAndView model = new ModelAndView();
		List<JogoDTO> listaJogos = jogoService.listarTodosJogos();
		model.addObject("cadastrarSalaJogosForm", new CadastrarSalaJogosForm());
		model.addObject("listaJogos", listaJogos);
		model.setViewName("/admin/salaJogos/createSalaJogos");
		return model;
	}

	@RequestMapping(value = "/admin/salaJogos/cadastrarSalaJogos", method = RequestMethod.POST)
	public ModelAndView cadastrar(
			@Valid @ModelAttribute CadastrarSalaJogosForm cadastrarSalaJogosForm,
			BindingResult bindingResult, HttpServletRequest request) {
		List<JogoDTO> listaJogos = jogoService.listarTodosJogos();
		ModelAndView model = new ModelAndView();
		model.addObject("cadastrarSalaJogosForm", cadastrarSalaJogosForm);
		model.setViewName("/admin/salaJogos/createSalaJogos");

		if (bindingResult.hasErrors()) {
			model.addObject("listaJogos", listaJogos);
			return model;
		} else {
			if (cadastrarSalaJogosForm.getNmSalaJogos() == null) {
				bindingResult.addError(new ObjectError("Nome da Sala de Jogos",
						"Por favor insira um nome para a Sala de Jogos!"));

				return model;
			} else if (cadastrarSalaJogosForm.getIdJogo() == 0) {
				bindingResult.addError(new ObjectError("Nome do Jogo",
						"Por favor insira o nome do Jogo!"));
				return model;
			} else if (cadastrarSalaJogosForm.getValorSala() == null) {
				bindingResult.addError(new ObjectError("Valor da Sala",
						"Por favor insira o Valor da Sala!"));
				model.addObject("listaJogos", listaJogos);
				return model;
			}

			else {
				try {
					SalaJogos novaSalaJogo = new SalaJogos();

					novaSalaJogo.setDtCriacao(new Date());
					novaSalaJogo.setNmSalaJogos(cadastrarSalaJogosForm
							.getNmSalaJogos());
					novaSalaJogo.setPosicaoSala(new Integer(1));

					try {
						novaSalaJogo.setValorSala(new BigDecimal(StringUtils
								.stringMonetarioToDouble(cadastrarSalaJogosForm
										.getValorSala())));
					} catch (ParseException e1) {
						throw new VsPlayersException(e1);
					}
					novaSalaJogo.setJogo(jogoService
							.getById(cadastrarSalaJogosForm.getIdJogo()));

					salaJogosService.inserirSalaJogos(novaSalaJogo);

				} catch (VsPlayersException e) {
					logger.error(e.getMessage());
					e.printStackTrace();
					bindingResult
							.addError(new ObjectError("ERRO",
									"Ocorreu um erro ao tentar gravar uma Sala de Jogo, contate o administrador"));
					return model;

				}
			}
		}
		return this.listarSalaJogos(request);

	}

	@RequestMapping(value = "/admin/salaJogos/editSalaJogos", method = RequestMethod.GET)
	public ModelAndView editSalaJogos(
			@RequestParam(value = "idSalaJogosSelecionado") Long idSalaJogosSelecionado,
			HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		List<JogoDTO> listaJogos = jogoService.listarTodosJogos();
		SalaJogos salaJogosEdicao = salaJogosService
				.getById(idSalaJogosSelecionado);

		CadastrarSalaJogosForm formEdicao = new CadastrarSalaJogosForm();

		formEdicao.setId(salaJogosEdicao.getId());

		formEdicao.setNmSalaJogos(salaJogosEdicao.getNmSalaJogos());

		formEdicao.setDtAtualizacao(salaJogosEdicao.getDtAtualizacao());

		formEdicao.setDtCriacao(salaJogosEdicao.getDtCriacao());

		formEdicao.setPosicaoSala(salaJogosEdicao.getPosicaoSala());

		formEdicao.setIdJogo(salaJogosEdicao.getJogo().getId());

		formEdicao.setValorSala(salaJogosEdicao.getValorSala().toString());

		model.addObject("listaJogos", listaJogos);

		model.addObject("cadastrarSalaJogosForm", formEdicao);

		model.setViewName("/admin/salaJogos/editSalaJogos");

		return model;
	}

	@RequestMapping(value = "/admin/salaJogos/editarSalaJogos", method = RequestMethod.POST)
	public ModelAndView editar(

	@Valid @ModelAttribute CadastrarSalaJogosForm cadastrarSalaJogosForm,
			BindingResult bindingResult, HttpServletRequest request) {
		List<JogoDTO> listaJogos = jogoService.listarTodosJogos();
		ModelAndView model = new ModelAndView();
		model.addObject("cadastrarSalaJogosForm", cadastrarSalaJogosForm);
		model.setViewName("/admin/salaJogos/editSalaJogos");

		if (bindingResult.hasErrors()) {
			model.addObject("listaJogos", listaJogos);
			return model;
		} else {
			if (cadastrarSalaJogosForm.getNmSalaJogos() == null) {
				bindingResult.addError(new ObjectError("Nome da Sala de Jogos",
						"Por favor insira um nome para a Sala de Jogos!"));

				return model;
			} else if (cadastrarSalaJogosForm.getIdJogo() == 0) {
				bindingResult.addError(new ObjectError("Nome do Jogo",
						"Por favor insira o nome do Jogo!"));
				return model;
			} else if (cadastrarSalaJogosForm.getValorSala() == null) {
				bindingResult.addError(new ObjectError("Valor da Sala",
						"Por favor insira o Valor da Sala!"));
				model.addObject("listaJogos", listaJogos);
				return model;
			}

			else {
				try {
					SalaJogos novaSalaJogo = salaJogosService
							.getById(cadastrarSalaJogosForm.getId());

					novaSalaJogo.setDtAtualizacao(new Date());

					novaSalaJogo.setNmSalaJogos(cadastrarSalaJogosForm
							.getNmSalaJogos());
					novaSalaJogo.setPosicaoSala(new Integer(1));
					try {
						novaSalaJogo.setValorSala(new BigDecimal(StringUtils
								.stringMonetarioToDouble(cadastrarSalaJogosForm
										.getValorSala())));
					} catch (ParseException e) {
						throw new VsPlayersException(e);
					}
					novaSalaJogo.setJogo(jogoService
							.getById(cadastrarSalaJogosForm.getIdJogo()));

					salaJogosService.atualizarSalaJogos(novaSalaJogo);

				} catch (VsPlayersException e) {
					logger.error(e.getMessage());
					e.printStackTrace();
					bindingResult
							.addError(new ObjectError("ERRO",
									"Ocorreu um erro ao tentar gravar uma Sala de Jogo, contate o administrador"));
					return model;

				}
			}
		}
		return this.listarSalaJogos(request);

	}

	@RequestMapping(value = "/admin/salaJogos/deleteSalaJogos", method = RequestMethod.GET)
	public ModelAndView deleteJogo(
			@RequestParam(value = "idSalaJogosSelecionado") Long idSalaJogosSelecionado,
			HttpServletRequest request) {
		salaJogosService.deleteSalaJogos(idSalaJogosSelecionado);
		return this.listarSalaJogos(request);
	}

	@RequestMapping(value = "/admin/salaJogos/viewSalaJogos/{id}", method = RequestMethod.GET)
	public ModelAndView viewPerfil(@PathVariable Long id) {
		SalaJogos salaJogos = salaJogosService.getById(id);
		if (salaJogos != null) {
			ModelAndView model = new ModelAndView();
			model.setViewName("/admin/salaJogos/viewSalaJogos");
			model.addObject("salaJogos", salaJogos);
			return model;
		} else {
			return null;
		}
	}

	@RequestMapping(value = "/restricted/desafio/desafioSalaJogos", method = RequestMethod.GET)
	public ModelAndView desafioSalaJogos(HttpServletRequest request) {
		List<SalaJogosDTO> salaJogos = salaJogosService.listarTodosSalaJogos();

		VsPlayersUserDetail user = (VsPlayersUserDetail) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		List<Usuario> listUsuarioOn = new ArrayList<Usuario>();
		listUsuarioOn.add(usuarioService.getById(user.getUsuario().getId()));
		if (salaJogos != null) {
			for (SalaJogosDTO salaJogo : salaJogos) {
				for (Usuario usuarioOn : listUsuarioOn) {
					for (AcessoRedeJogo acesso : usuarioOn
							.getListaAcessoRedeJogo()) {

						for (Console console : acesso.getConsoles()) {
							if (console.getNmConsole().equals(
									salaJogo.getNomeConsole())) {
								for (Jogo jogo : console.getListaJogos()) {
									if (jogo.getNmJogo().equals(
											salaJogo.getNomeJogo())) {
										salaJogo.setQtdUsuario(+1);

									}
								}
							}
						}
					}

				}
			}

			// List<SalaJogosDTO> salaJogos =
			// salaJogosService.listarTodosSalaJogos();

			ModelAndView model = new ModelAndView();

			model.addObject("usuariosOnline", SecurityContextHolder
					.getContext().getAuthentication().getAuthorities().size());

			model.setViewName("/restricted/desafio/desafioSalaJogos");
			model.addObject("listSalaJogos", salaJogos);
			return model;
		} else {
			return null;
		}
	}
}

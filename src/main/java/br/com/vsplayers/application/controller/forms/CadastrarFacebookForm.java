package br.com.vsplayers.application.controller.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CadastrarFacebookForm {

	@NotNull(message = "Seu nome deve ser informado")
	@Size(min = 5, max = 255, message = "Valor informado para o nome é inválido")
	private String nomeUser;

	@NotNull(message = "Seu e-mail deve ser informado")
	@Size(min = 2, max = 255, message = "O e-mail informado é inválido")
	private String emailUser;

	@NotNull(message = "Seu CPF deve ser informado no formado 000.000.000-00")
	@Size(min = 14, max = 14, message = "O CPF informado é inválido")
	private String cpf;

	public String getNomeUser() {
		return nomeUser;
	}

	public void setNomeUser(String nomeUser) {
		this.nomeUser = nomeUser;
	}

	public String getEmailUser() {
		return emailUser;
	}

	public void setEmailUser(String emailUser) {
		this.emailUser = emailUser;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
}
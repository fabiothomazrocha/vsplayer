package br.com.vsplayers.application.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.vsplayers.application.controller.forms.CadastrarFaqForm;
import br.com.vsplayers.domain.Faq;
import br.com.vsplayers.domain.VsPlayers;
import br.com.vsplayers.dto.faq.FaqDTO;
import br.com.vsplayers.exception.VsPlayersException;
import br.com.vsplayers.infra.dao.VsPlayersDAO;
import br.com.vsplayers.service.FaqService;

@Controller
public class PaginasFaqController {
	private static final Logger logger = LoggerFactory
			.getLogger(PaginasFaqController.class);

	@Autowired
	@Qualifier("FaqServiceImpl")
	private FaqService faqService;
	@Autowired
	@Qualifier("VsPlayersDAOImpl")
	private VsPlayersDAO vsPlayersDAO;

	@RequestMapping(value = "/admin/faq/listFaq", method = RequestMethod.GET)
	public ModelAndView listarFaqs(HttpServletRequest request) {
		List<FaqDTO> listaFaq = faqService.listarTodosFaqs();
		ModelAndView returnObj = new ModelAndView("/admin/faq/listFaq");
		returnObj.addObject("listaFaq", listaFaq);
		return returnObj;
	}

	@RequestMapping(value = "/admin/faq/createFaq", method = RequestMethod.GET)
	public ModelAndView cadastrarFaq() {
		ModelAndView model = new ModelAndView();
		CadastrarFaqForm cadastrarFaqForm = new CadastrarFaqForm();
		model.addObject("cadastrarFaqForm", cadastrarFaqForm);
		model.setViewName("/admin/faq/createFaq");
		return model;
	}

	@RequestMapping(value = "/admin/faq/cadastrarFaq", method = RequestMethod.POST)
	public ModelAndView cadastrar(
			@Valid @ModelAttribute CadastrarFaqForm cadastrarFaqForm,
			BindingResult bindingResult, HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		model.addObject("cadastrarFaqForm", cadastrarFaqForm);
		model.setViewName("/admin/faq/createFaq");
		VsPlayers vsInstance = vsPlayersDAO.getFirstResult();
		if (bindingResult.hasErrors()) {
			return model;
		} else {
			if (cadastrarFaqForm.getNmFaq() == null) {
				bindingResult.addError(new ObjectError("Nome do Faq",
						"Por favor insira um nome para o FAQ!"));

				return model;
			}
			if (vsInstance == null) {
				bindingResult.addError(new ObjectError("VsPlayers Erro",
						"Erro ao tentar gravar um FAQ"));
				logger.error("VsPlayers esta Null!");
				return model;
			}

			else {
				try {
					Faq faq = new Faq();
					faq.setDtAtualizacao(new Date());
					faq.setVsPlayers(vsInstance);
					faq.setNmFaq(cadastrarFaqForm.getNmFaq());
					faqService.inserirFaq(faq);
				} catch (VsPlayersException e) {
					logger.error(e.getMessage());
					e.printStackTrace();
					bindingResult
							.addError(new ObjectError("ERRO",
									"Ocorreu um erro ao tentar gravar um FAQ, contate o administrador"));
					return model;

				}
			}
		}
		return this.listarFaqs(request);
	}

	@RequestMapping(value = "/admin/faq/editFaq", method = RequestMethod.GET)
	public ModelAndView editFaq(
			@RequestParam(value = "idFaqSelecionado") Long idFaqSelecionado,
			HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		Faq faqEdicao = faqService.getById(idFaqSelecionado);
		CadastrarFaqForm formEdicao = new CadastrarFaqForm();
		formEdicao.setId(faqEdicao.getId());
		formEdicao.setNmFaq(faqEdicao.getNmFaq());

		model.addObject("cadastrarFaqForm", formEdicao);
		model.setViewName("/admin/faq/editFaq");
		return model;
	}

	@RequestMapping(value = "/admin/faq/editarFaq", method = RequestMethod.POST)
	public ModelAndView editar(
			@Valid @ModelAttribute CadastrarFaqForm cadastrarFaqForm,
			BindingResult bindingResult, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		model.addObject("cadastrarFaqForm", cadastrarFaqForm);
		model.setViewName("/admin/faq/editFaq");
		if (bindingResult.hasErrors()) {
			return model;
		} else {
			if (cadastrarFaqForm.getNmFaq() == null) {
				bindingResult.addError(new ObjectError("Nome do FAQ",
						"Por favor insira um nome para o FAQ!"));
				return model;
			}
			if (vsPlayersDAO.getFirstResult() == null) {
				bindingResult
						.addError(new ObjectError("ERRO",
								"Ocorreu um erro ao tentar gravar um FAQ, contate o administrador"));
				logger.error("vsPlayersDAO.getFirstResult()--> Está Null!");
				return model;
			} else {
				Faq faqEdicao = faqService.getById(cadastrarFaqForm.getId());
				faqEdicao.setDtAtualizacao(new Date());
				faqEdicao.setVsPlayers(vsPlayersDAO.getFirstResult());
				faqEdicao.setNmFaq(cadastrarFaqForm.getNmFaq());
				try {
					faqService.atualizarFaq(faqEdicao);
				} catch (VsPlayersException e) {
					logger.error(e.getMessage());
					bindingResult
							.addError(new ObjectError("ERRO",
									"Ocorreu um erro ao tentar gravar um FAQ, contate o administrador"));
					return model;
				}
			}
		}
		return listarFaqs(request);
	}

	@RequestMapping(value = "/admin/faq/deleteFaq", method = RequestMethod.GET)
	public ModelAndView deleteFaq(
			@RequestParam(value = "idFaqSelecionado") Long idFaqSelecionado,
			HttpServletRequest request) {
		faqService.deleteFaq(idFaqSelecionado);
		return this.listarFaqs(request);
	}

	@RequestMapping(value = "/admin/faq/viewFaq/{id}", method = RequestMethod.GET)
	public ModelAndView viewFaq(@PathVariable Long id) {
		Faq faq = faqService.getById(id);
		if (faq != null) {
			ModelAndView model = new ModelAndView();
			model.setViewName("/admin/faq/viewFaq");
			model.addObject("faq", faq);
			return model;
		} else {
			return null;
		}
	}

	@RequestMapping(value = "/faqPublico", method = RequestMethod.GET)
	public ModelAndView listarFaqsPublico(HttpServletRequest request) {
		List<FaqDTO> listaFaq = faqService.listarTodosFaqs();
		ModelAndView returnObj = new ModelAndView("/faqPublico");
		returnObj.addObject("listaFaq", listaFaq);
		return returnObj;
	}

	@RequestMapping(value = "/faqPublico/{nome}", method = RequestMethod.GET)
	public ModelAndView viewFaqPublico(@PathVariable String nome) {
		Faq faq = faqService.getByName(nome);
	
		if (faq != null) {
			List<FaqDTO> listaFaq = faqService.listarTodosFaqs();
			ModelAndView model = new ModelAndView();
			model.setViewName("/faqPublico");
			model.addObject("listaFaq", listaFaq);
			model.addObject("faq", faq);
			return model;
		} else {
			return null;
		}
	}
}

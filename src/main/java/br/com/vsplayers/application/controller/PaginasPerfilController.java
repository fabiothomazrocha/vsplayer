package br.com.vsplayers.application.controller;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.vsplayers.application.controller.forms.CadastrarPerfilForm;
import br.com.vsplayers.domain.PerfilUsuario;
import br.com.vsplayers.dto.perfil.PerfilUsuarioDTO;
import br.com.vsplayers.integration.beans.VsPlayersUserDetail;
import br.com.vsplayers.service.PerfilService;
import br.com.vsplayers.service.UsuarioService;

@Controller
public class PaginasPerfilController {

	private static final Logger logger = LoggerFactory
			.getLogger(PaginasPerfilController.class);

	@Autowired
	@Qualifier("UsuarioServiceImpl")
	private UsuarioService usuarioService;

	@Autowired
	@Qualifier("PerfilServiceImpl")
	private PerfilService perfilService;

	@Autowired
	ServletContext context;

	@RequestMapping(value = "/restricted/perfil/createPerfil", method = RequestMethod.GET)
	public ModelAndView createPerfil() {
		ModelAndView model = new ModelAndView();
		model.addObject("cadastrarPerfilForm", new CadastrarPerfilForm());
		model.setViewName("/restricted/perfil/createPerfil");
		return model;
	}

	@RequestMapping(value = "/restricted/perfil/listPerfil", method = RequestMethod.GET)
	public ModelAndView listPerfil(HttpServletRequest request) {

		VsPlayersUserDetail user = (VsPlayersUserDetail) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		List<PerfilUsuarioDTO> perfisUsuario = perfilService
				.listarPerfisUsuario(user.getUsuario().getId());

		request.getSession(false).removeAttribute("imageTempProfile");

		ModelAndView model = new ModelAndView();
		model.addObject("listaPerfis", perfisUsuario);
		model.setViewName("/restricted/perfil/listPerfil");
		return model;
	}

	@RequestMapping(value = "/restricted/perfil/editPerfil", method = RequestMethod.GET)
	public ModelAndView editPerfil(
			@RequestParam(value = "idPerfilSelecionado") Long idPerfilSelecionado,
			HttpServletRequest request) {

		ModelAndView model = new ModelAndView();

		PerfilUsuario perfilEdicao = perfilService.getById(idPerfilSelecionado);

		CadastrarPerfilForm formEdicao = new CadastrarPerfilForm();
		formEdicao.setApelido(perfilEdicao.getApelido());
		formEdicao.setSobre(perfilEdicao.getDsSobre());
		formEdicao.setIdPerfilEdicao(perfilEdicao.getId());

		if (perfilEdicao.getImagemPerfil() != null) {

			String filePath = this.context
					.getRealPath("/resources/images/profile/")
					+ File.separator
					+ perfilEdicao.getImagemPerfil().getNmArquivo();

			File f = new File(filePath);
			if (f.exists()) {
				try {
					BufferedImage image = ImageIO.read(f);
					if (image != null) {
						request.getSession(false).setAttribute(
								"imageTempProfile", image);
					}
				} catch (IOException e) {
					logger.error(e.getMessage());
				}
			}

		}

		model.addObject("cadastrarPerfilForm", formEdicao);
		model.setViewName("/restricted/perfil/editPerfil");
		return model;
	}

	@RequestMapping(value = "/restricted/perfil/editarPerfil", method = RequestMethod.POST)
	public ModelAndView editar(
			@Valid @ModelAttribute CadastrarPerfilForm cadastrarPerfilform,
			BindingResult bindingResult, HttpServletRequest request) {

		PerfilUsuario perfilEdicao = perfilService.getById(cadastrarPerfilform
				.getIdPerfilEdicao());

		boolean removeImagem = false;
		boolean atualizaImagem = false;
		Image image = null;

		ModelAndView model = new ModelAndView();
		model.addObject("cadastrarPerfilForm", cadastrarPerfilform);
		model.setViewName("/restricted/perfil/editPerfil");

		if (cadastrarPerfilform.getRemoverImagem()) {
			request.getSession(false).removeAttribute("imageTempProfile");
			cadastrarPerfilform.setFotoPerfil(null);
			removeImagem = true;
		}

		if (!removeImagem
				&& cadastrarPerfilform.getFotoPerfil() != null
				&& !StringUtils.isEmpty(cadastrarPerfilform.getFotoPerfil()
						.getOriginalFilename())) {

			try {

				InputStream is = new ByteArrayInputStream(cadastrarPerfilform
						.getFotoPerfil().getBytes());

				image = ImageIO.read(is);

				atualizaImagem = true;

			} catch (IOException ex) {
				logger.error(ex.getMessage());
			}
		} else {
			atualizaImagem = false;
		}

		if (bindingResult.hasErrors()) {

			return model;

		} else {

			Boolean mudou = false;
			Boolean jaExiste = false;
			String apelido = cadastrarPerfilform.getApelido();
			String sobre = cadastrarPerfilform.getSobre();

			if (!perfilEdicao.getApelido().equals(apelido)) {
				mudou = true;
			}

			perfilEdicao.setApelido(apelido);
			perfilEdicao.setDsSobre(sobre);

			if (mudou) {
				jaExiste = perfilService.verificaApelidoJaExiste(apelido);
			}

			if (jaExiste && mudou) {
				bindingResult.addError(new ObjectError("Apelido",
						"Este apelido já está sendo usado por outro usuário"));

				return model;
			}

			try {

				perfilService.atualizarPerfil(perfilEdicao,
						perfilEdicao.getImagemPerfil() != null, removeImagem,
						atualizaImagem,
						this.context.getRealPath("/resources/images/profile/"),
						image);

			} catch (Throwable ex) {
				bindingResult
						.addError(new ObjectError("ERRO",
								"Ocorreu um erro ao tentar gravar o perfil, contate o administrador"));
				return model;
			}

		}

		request.getSession(false).removeAttribute("imageTempProfile");
		return this.listPerfil(request);
	}

	@RequestMapping(value = "/restricted/perfil/deletePerfil", method = RequestMethod.GET)
	public ModelAndView deletePerfil(
			@RequestParam(value = "idPerfilSelecionado") Long idPerfilSelecionado,
			HttpServletRequest request) {

		perfilService.deletePerfil(idPerfilSelecionado);

		return this.listPerfil(request);
	}

	@RequestMapping(value = "/restricted/perfil/cadastrarPerfil", method = RequestMethod.POST)
	public ModelAndView cadastrar(
			@Valid @ModelAttribute CadastrarPerfilForm cadastrarPerfilform,
			BindingResult bindingResult, HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		model.addObject("cadastrarPerfilForm", cadastrarPerfilform);
		model.setViewName("/restricted/perfil/createPerfil");
		Boolean removerImagem = cadastrarPerfilform.getRemoverImagem();

		if (removerImagem) {
			request.getSession(false).removeAttribute("imageTempProfile");
			cadastrarPerfilform.setFotoPerfil(null);
		}

		if (!removerImagem
				&& cadastrarPerfilform.getFotoPerfil() != null
				&& !StringUtils.isEmpty(cadastrarPerfilform.getFotoPerfil()
						.getOriginalFilename())) {

			try {

				InputStream is = new ByteArrayInputStream(cadastrarPerfilform
						.getFotoPerfil().getBytes());

				Image image = ImageIO.read(is);

				if (image != null) {

					double bytes = cadastrarPerfilform.getFotoPerfil()
							.getBytes().length;
					double kilobytes = (bytes / 1024);

					logger.info("Fazendo upload de um arquivo de: " + kilobytes
							+ " kb.");

					if (kilobytes > 150) {

						bindingResult
								.addError(new ObjectError("ERRO",
										"Formato da imagem inválido, a imagem deve ter no máximo 150kb"));

						return model;

					} else {

						BufferedImage buffered = (BufferedImage) image;
						request.getSession(false).setAttribute(
								"imageTempProfile", buffered);
					}
				}

			} catch (IOException ex) {
				logger.error(ex.getMessage());
			}
		}

		VsPlayersUserDetail user = (VsPlayersUserDetail) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		if (bindingResult.hasErrors()) {

			return model;

		} else {

			String apelido = cadastrarPerfilform.getApelido();
			String sobre = cadastrarPerfilform.getSobre();

			Boolean jaExiste = perfilService.verificaApelidoJaExiste(apelido);

			if (jaExiste) {
				bindingResult.addError(new ObjectError("Apelido",
						"Este apelido já está sendo usado por outro usuário"));

				return model;
			}

			try {

				BufferedImage image = null;

				if (!removerImagem) {

					image = null;

					if (request.getSession(false).getAttribute(
							"imageTempProfile") != null
							& StringUtils.isEmpty(cadastrarPerfilform
									.getFotoPerfil().getOriginalFilename())) {

						image = (BufferedImage) request.getSession(false)
								.getAttribute("imageTempProfile");

					} else {

						if (cadastrarPerfilform.getFotoPerfil() != null
								&& !StringUtils.isEmpty(cadastrarPerfilform
										.getFotoPerfil().getOriginalFilename())) {

							InputStream is = new ByteArrayInputStream(
									cadastrarPerfilform.getFotoPerfil()
											.getBytes());

							image = ImageIO.read(is);

						}
					}

				}

				if (image != null) {

					Boolean quadrada = ((image.getWidth() + image.getHeight()) / 2) == image
							.getWidth();

					if (!quadrada) {

						bindingResult
								.addError(new ObjectError(
										"ERRO",
										"Formato da imagem inválido, a imagem deve ser quadrada"
												+ " (ex: 100x100) e ter no máximo 150x150 pixels"));

						return model;

					}

				}

				perfilService.inserirPerfil(apelido, sobre, user.getUsuario()
						.getId(), image != null, this.context
						.getRealPath("/resources/images/profile/"), image);

			} catch (Throwable ex) {
				bindingResult
						.addError(new ObjectError("ERRO",
								"Ocorreu um erro ao tentar gravar o perfil, contate o administrador"));
				return model;
			}

		}

		request.getSession(false).removeAttribute("imageTempProfile");
		return this.listPerfil(request);
	}

	@RequestMapping(value = "/restricted/perfil/getSessionImage", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<byte[]> getSessionImage(HttpServletRequest request)
			throws IOException {

		BufferedImage image = (BufferedImage) request.getSession(false)
				.getAttribute("imageTempProfile");

		if (image == null) {
			Image defImg = ImageIO.read(PaginasPerfilController.class
					.getResource("/default.png"));
			image = (BufferedImage) defImg;
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(image, "png", baos);
		baos.flush();
		byte[] imageInByte = baos.toByteArray();
		baos.close();

		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.IMAGE_PNG);

		return new ResponseEntity<byte[]>(imageInByte, headers,
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/restricted/perfil/viewPerfil/{id}", method = RequestMethod.GET)
	public ModelAndView viewPerfil(@PathVariable Long id) {

		PerfilUsuarioDTO perfil = perfilService.getDTOById(id);
		if (perfil != null) {
			ModelAndView model = new ModelAndView();
			model.setViewName("/restricted/perfil/viewPerfil");
			if (!StringUtils.isEmpty(perfil.getPathImagemPerfil())) {
				perfil.setPathImagemPerfil("/resources/images/profile/"
						+ perfil.getPathImagemPerfil());
			}
			model.addObject("perfil", perfil);
			return model;
		} else {
			return null;
		}
	}

}
package br.com.vsplayers.application.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.vsplayers.application.controller.forms.CadastrarItemFaqForm;
import br.com.vsplayers.domain.Faq;
import br.com.vsplayers.domain.ItemFaq;
import br.com.vsplayers.dto.faq.FaqDTO;
import br.com.vsplayers.dto.itemfaq.ItemFaqDTO;
import br.com.vsplayers.exception.VsPlayersException;
import br.com.vsplayers.service.FaqService;
import br.com.vsplayers.service.ItemFaqService;

@Controller
public class PaginasItemFaqController {

	private static final Logger logger = LoggerFactory
			.getLogger(PaginasFaqController.class);

	@Autowired
	@Qualifier("ItemFaqServiceImpl")
	private ItemFaqService itemFaqService;
	@Autowired
	@Qualifier("FaqServiceImpl")
	private FaqService faqService;

	@RequestMapping(value = "/admin/itemFaq/listItemFaq", method = RequestMethod.GET)
	public ModelAndView listarItemFaqs(HttpServletRequest request) {
		List<ItemFaqDTO> listaItemFaq = itemFaqService.listarTodosItemFaqs();
		ModelAndView returnObj = new ModelAndView("/admin/itemFaq/listItemFaq");
		returnObj.addObject("listaItemFaq", listaItemFaq);
		return returnObj;
	}

	@RequestMapping(value = "/admin/itemFaq/createItemFaq", method = RequestMethod.GET)
	public ModelAndView cadastrarItemFaq() {
		ModelAndView model = new ModelAndView();

		List<FaqDTO> listaFaq = faqService.listarTodosFaqs();
		CadastrarItemFaqForm cadastrarItemFaqForm = new CadastrarItemFaqForm();
		model.addObject("listaFaq", listaFaq);
		model.addObject("cadastrarItemFaqForm", cadastrarItemFaqForm);
		model.setViewName("/admin/itemFaq/createItemFaq");
		return model;
	}

	@RequestMapping(value = "/admin/itemFaq/editItemFaq", method = RequestMethod.GET)
	public ModelAndView editItemFaq(
			@RequestParam(value = "idItemFaqSelecionado") Long idItemFaqSelecionado,
			HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		ItemFaq itemFaqEdicao = itemFaqService.getById(idItemFaqSelecionado);
		CadastrarItemFaqForm formEdicao = new CadastrarItemFaqForm();
		List<FaqDTO> listaFaq = faqService.listarTodosFaqs();
		formEdicao.setId(itemFaqEdicao.getId());
		formEdicao.setDsTituloItem(itemFaqEdicao.getDsTituloItem());
		formEdicao.setTxItem(itemFaqEdicao.getTxItem());
		formEdicao.setIdFaq(itemFaqEdicao.getFaq().getId());
		model.addObject("listaFaq",listaFaq);
		model.addObject("cadastrarItemFaqForm",formEdicao);
		model.setViewName("/admin/itemFaq/editItemFaq");
		return model;
	}

	@RequestMapping(value = "/admin/itemFaq/cadastrarItemFaq", method = RequestMethod.POST)
	public ModelAndView cadastrar(
			@Valid @ModelAttribute CadastrarItemFaqForm cadastrarItemFaqform,
			BindingResult bindingResult, HttpServletRequest request) {
		List<FaqDTO> listaFaq = faqService.listarTodosFaqs();
		ModelAndView model = new ModelAndView();
		model.addObject("cadastrarItemFaqForm", cadastrarItemFaqform);
		model.setViewName("/admin/itemFaq/createItemFaq");

		if (bindingResult.hasErrors()) {
			model.addObject("listaFaq",listaFaq);
			return model;
		} else {
			if (cadastrarItemFaqform.getDsTituloItem() == null) {
				bindingResult.addError(new ObjectError("Pergunta FAQ",
						"Por favor insira uma pergunta para o FAQ!"));
				
			} else if (cadastrarItemFaqform.getIdFaq() == 0) {
				bindingResult.addError(new ObjectError("FAQ",
						"Por favor insira o nome do FAQ"));
				
			} else if (cadastrarItemFaqform.getTxItem() == null) {
				bindingResult.addError(new ObjectError("Resposta do FAQ",
						"Por favor insira uma resposta para o FAQ"));
			}

			else {
				try {
					ItemFaq itemFaq = new ItemFaq();
					Faq faq = faqService.getById(cadastrarItemFaqform
							.getIdFaq());
					itemFaq.setDsTituloItem(cadastrarItemFaqform
							.getDsTituloItem());
					itemFaq.setTxItem(cadastrarItemFaqform.getTxItem());
					itemFaq.setFaq(faq);
					itemFaqService.inserirItemFaq(itemFaq);
				} catch (VsPlayersException e) {
					logger.error(e.getMessage());
					e.printStackTrace();
					bindingResult
							.addError(new ObjectError("ERRO",
									"Ocorreu um erro ao tentar gravar uma ItemFAQ, contate o administrador"));
					return model;
				}
			}
		}
		return this.listarItemFaqs(request);
	}

	@RequestMapping(value = "/admin/itemFaq/editarItemFaq", method = RequestMethod.POST)
	public ModelAndView editar(
			@Valid @ModelAttribute CadastrarItemFaqForm cadastrarItemFaqForm,
			BindingResult bindingResult, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		List<FaqDTO> listaFaq = faqService.listarTodosFaqs();
		model.addObject("listaFaq", listaFaq);
		model.addObject("cadastrarItemFaqForm", cadastrarItemFaqForm);
		model.setViewName("/admin/itemFaq/editItemFaq");
		if (bindingResult.hasErrors()) {
			model.addObject("listaFaq", listaFaq);
			return model;
		} else {
			if (cadastrarItemFaqForm.getDsTituloItem() == null) {
				bindingResult.addError(new ObjectError("Pergunta FAQ",
						"Por favor insira uma pergunta para o FAQ!"));

				return model;
			} else if (cadastrarItemFaqForm.getId() == 0) {
				bindingResult.addError(new ObjectError("FAQ",
						"Por favor insira o nome do FAQ"));

				return model;
			} else if (cadastrarItemFaqForm.getTxItem() == null) {
				bindingResult.addError(new ObjectError("Resposta do FAQ",
						"Por favor insira uma resposta para o FAQ"));

			} else {
				ItemFaq itemFaqEdicao = itemFaqService
						.getById(cadastrarItemFaqForm.getId());
				Faq faq = faqService.getById(cadastrarItemFaqForm.getIdFaq());
				itemFaqEdicao.setDsTituloItem(cadastrarItemFaqForm
						.getDsTituloItem());
				itemFaqEdicao.setTxItem(cadastrarItemFaqForm.getTxItem());
				itemFaqEdicao.setFaq(faq);
				try {
					itemFaqService.atualizarItemFaq(itemFaqEdicao);
				} catch (VsPlayersException e) {
					logger.error(e.getMessage());
					bindingResult
							.addError(new ObjectError("ERRO",
									"Ocorreu um erro ao tentar gravar um ItemFaq, contate o administrador"));
					return model;
				}
			}
		}
		return listarItemFaqs(request);
	}

	@RequestMapping(value = "/admin/itemFaq/deleteItemFaq", method = RequestMethod.GET)
	public ModelAndView deleteItemFaq(
			@RequestParam(value = "idItemFaqSelecionado") Long idItemFaqSelecionado,
			HttpServletRequest request) {
		itemFaqService.deleteItemFaq(idItemFaqSelecionado);
		return this.listarItemFaqs(request);
	}

	@RequestMapping(value = "/admin/itemFaq/viewItemFaq/{id}", method = RequestMethod.GET)
	public ModelAndView viewFaq(@PathVariable Long id) {
		ItemFaq itemFaq = itemFaqService.getById(id);
		if (itemFaq != null) {
			ModelAndView model = new ModelAndView();
			model.setViewName("/admin/itemFaq/viewItemFaq");
			model.addObject("itemFaq", itemFaq);
			return model;
		} else {
			return null;
		}
	}

}

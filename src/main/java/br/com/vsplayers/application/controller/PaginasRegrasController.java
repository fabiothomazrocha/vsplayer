package br.com.vsplayers.application.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.vsplayers.application.controller.forms.CadastrarRegraForm;
import br.com.vsplayers.domain.Regra;
import br.com.vsplayers.domain.VsPlayers;
import br.com.vsplayers.exception.VsPlayersException;
import br.com.vsplayers.infra.dao.VsPlayersDAO;
import br.com.vsplayers.service.RegrasService;

@Controller
public class PaginasRegrasController {
	

	private static final Logger logger = LoggerFactory
			.getLogger(PaginasRegrasController.class);

	@Autowired
	@Qualifier("VsPlayersDAOImpl")
	private VsPlayersDAO vsPlayersDAO;

	@Autowired
	@Qualifier("RegrasServiceImpl")
	private RegrasService regrasService;

	@RequestMapping(value = "/admin/regras/createRegra", method = RequestMethod.GET)
	public ModelAndView cadastrarRegra() {
		ModelAndView model = new ModelAndView();
		model.addObject("cadastrarRegraForm", new CadastrarRegraForm());
		model.setViewName("/admin/regras/createRegra");
		return model;
	}

	@RequestMapping(value = "/admin/regras/cadastrarRegra", method = RequestMethod.POST)
	public ModelAndView cadastrar(
			@Valid @ModelAttribute CadastrarRegraForm cadastrarRegraform,
			BindingResult bindingResult, HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		model.addObject("cadastrarRegraForm", cadastrarRegraform);
		model.setViewName("/admin/regras/createRegra");
		
		VsPlayers vsInstance = vsPlayersDAO.getFirstResult();
		
		if (bindingResult.hasErrors()) {
			return model;
		} else {
			if (cadastrarRegraform.getNmInstancia() == null) {
				bindingResult.addError(new ObjectError("Nome da Regra",
						"Por favor insira um nome para a regra!"));

				return model;
			} else if (cadastrarRegraform.getTxItem() == null) {
				bindingResult.addError(new ObjectError("Descrição da Regra",
						"Por favor insira uma descrição para a regra!"));

				return model;
			} else if (vsInstance == null) {
				bindingResult
				.addError(new ObjectError("ERRO",
						"Ocorreu um erro ao tentar gravar uma regra, contate o administrador"));
				logger.error("Erro: Objeto vsIstance do VsPlayers esta null." );
			}

			else {
				try {

					Regra novaRegra = new Regra();
					novaRegra.setVsPlayers(vsInstance);
					novaRegra.setNmInstancia(cadastrarRegraform
							.getNmInstancia());
					novaRegra.setTxItem(cadastrarRegraform.getTxItem());
					regrasService.inserirRegra(novaRegra);

				} catch (VsPlayersException e) {
					logger.error(e.getMessage());
					bindingResult
							.addError(new ObjectError("ERRO",
									"Ocorreu um erro ao tentar gravar uma regra, contate o administrador"));
					return model;

				}
			}
		}
		return this.listRegras(request);

	}

	@RequestMapping(value = "/admin/regras/editRegra", method = RequestMethod.GET)
	public ModelAndView editRegra(
			@RequestParam(value = "idRegraSelecionado") Long idRegraSelecionado,
			HttpServletRequest request) {

		ModelAndView model = new ModelAndView();

		Regra regraEdicao = regrasService.getById(idRegraSelecionado);

		CadastrarRegraForm formEdicao = new CadastrarRegraForm();
		formEdicao.setId(regraEdicao.getId());
		formEdicao.setNmInstancia(regraEdicao.getNmInstancia());
		formEdicao.setTxItem(regraEdicao.getTxItem());
		

		model.addObject("cadastrarRegraForm", formEdicao);
		model.setViewName("/admin/regras/editRegra");
		return model;
	}

	@RequestMapping(value = "/admin/regras/listRegra", method = RequestMethod.GET)
	public ModelAndView listRegras(HttpServletRequest request) {

		VsPlayers vsInstance = vsPlayersDAO.getFirstResult();
		
		List<Regra> regra = regrasService.listarRegras(vsInstance.getId());

		ModelAndView model = new ModelAndView();
		model.addObject("listRegras", regra);
		model.setViewName("/admin/regras/listRegra");
		return model;
	}

	@RequestMapping(value = "/admin/regras/deleteRegra", method = RequestMethod.GET)
	public ModelAndView deleteRegra(
			@RequestParam(value = "idRegraSelecionado") Long idRegraSelecionado,
			HttpServletRequest request) {
		regrasService.deleteRegra(idRegraSelecionado);
		return this.listRegras(request);
	}

	@RequestMapping(value = "/admin/regras/editarRegra", method = RequestMethod.POST)
	public ModelAndView editar(
			@Valid @ModelAttribute CadastrarRegraForm cadastrarRegraForm,
			BindingResult bindingResult, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		model.addObject("cadastrarRegraForm", cadastrarRegraForm);
		model.setViewName("/admin/regras/editRegra");
		VsPlayers vsInstance = vsPlayersDAO.getFirstResult();

		
		if (bindingResult.hasErrors()) {
			return model;
		} else {
			Regra regraEdicao = regrasService.getById(cadastrarRegraForm
					.getId());

			regraEdicao.setNmInstancia(cadastrarRegraForm.getNmInstancia());
			regraEdicao.setTxItem(cadastrarRegraForm.getTxItem());
			regraEdicao.setVsPlayers(vsInstance);

			try {
				regrasService.atualizarRegra(regraEdicao);
			} catch (VsPlayersException e) {
				logger.error(e.getMessage());
				bindingResult
						.addError(new ObjectError("ERRO",
								"Ocorreu um erro ao tentar gravar uma Regra, contate o administrador"));
				return model;
			}
		}
		return listRegras(request);

	}

	@RequestMapping(value = "/admin/regras/viewRegra/{id}", method = RequestMethod.GET)
	public ModelAndView viewPerfil(@PathVariable Long id) {
		Regra regra = regrasService.getById(id);
		if (regra != null) {
			ModelAndView model = new ModelAndView();
			model.setViewName("/admin/regras/viewRegra");
			model.addObject("regra", regra);
			return model;
		} else {
			return null;
		}
	}

}

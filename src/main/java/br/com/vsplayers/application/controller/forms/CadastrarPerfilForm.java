package br.com.vsplayers.application.controller.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

public class CadastrarPerfilForm {

	@NotNull(message = "Seu apelido deve ser informado")
	@Size(min = 5, max = 255, message = "Valor informado para o apelido é inválido")
	private String apelido;

	@Size(min = 10, max = 400, message = "O tamanho da sua descrição deve ser de 10 a 255 caracteres.")
	private String sobre;

	private Boolean removerImagem;

	private MultipartFile fotoPerfil;

	private Long idPerfilEdicao;

	public String getApelido() {
		return apelido;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}

	public String getSobre() {
		return sobre;
	}

	public void setSobre(String sobre) {
		this.sobre = sobre;
	}

	public Boolean getRemoverImagem() {
		return removerImagem;
	}

	public void setRemoverImagem(Boolean removerImagem) {
		this.removerImagem = removerImagem;
	}

	public MultipartFile getFotoPerfil() {
		return fotoPerfil;
	}

	public void setFotoPerfil(MultipartFile fotoPerfil) {
		this.fotoPerfil = fotoPerfil;
	}

	public Long getIdPerfilEdicao() {
		return idPerfilEdicao;
	}

	public void setIdPerfilEdicao(Long idPerfilEdicao) {
		this.idPerfilEdicao = idPerfilEdicao;
	}

}
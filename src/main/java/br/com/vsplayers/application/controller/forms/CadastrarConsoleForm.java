package br.com.vsplayers.application.controller.forms;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.vsplayers.domain.enums.TipoAcessoRedeJogo;
import br.com.vsplayers.domain.enums.TipoConsole;
import br.com.vsplayers.dto.jogo.JogoDTO;

public class CadastrarConsoleForm {
	private Long id;
	@NotNull(message = "O nome do console deve ser informado")
	@Size(min = 3, max = 10, message = "Valor informado para o nome do Console é inválido")
	private String nmConsole;
	@NotNull(message = "O nome do Tipo de rede do console deve ser informado")
	private TipoAcessoRedeJogo tipoRede;
	@NotNull(message = "O tipo de Console deve ser informado")
	private TipoConsole tipoConsole;
	private List<JogoDTO> listaJogos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmConsole() {
		return nmConsole;
	}

	public void setNmConsole(String nmConsole) {
		this.nmConsole = nmConsole;
	}

	public TipoAcessoRedeJogo getTipoRede() {
		return tipoRede;
	}

	public void setTipoRede(TipoAcessoRedeJogo tipoRede) {
		this.tipoRede = tipoRede;
	}

	public TipoConsole getTipoConsole() {
		return tipoConsole;
	}

	public void setTipoConsole(TipoConsole tipoConsole) {
		this.tipoConsole = tipoConsole;
	}

	public List<JogoDTO> getListaJogos() {
		return listaJogos;
	}

	public void setListaJogos(List<JogoDTO> listaJogos) {
		this.listaJogos = listaJogos;
	}

	
}

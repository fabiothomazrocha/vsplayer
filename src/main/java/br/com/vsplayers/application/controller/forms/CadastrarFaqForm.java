package br.com.vsplayers.application.controller.forms;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.vsplayers.dto.itemfaq.ItemFaqDTO;

public class CadastrarFaqForm {

	private Long id;
	@NotNull(message = "O nome do FAQ deve ser informado")
	@Size(min = 3, max = 255, message = "Valor informado para o nome do FAQ é inválido")
	private String nmFaq;
	private Date dtAtualizacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmFaq() {
		return nmFaq;
	}

	public void setNmFaq(String nmFaq) {
		this.nmFaq = nmFaq;
	}

	public Date getDtAtualizacao() {
		return dtAtualizacao;
	}

	public void setDtAtualizacao(Date dtAtualizacao) {
		this.dtAtualizacao = dtAtualizacao;
	}

	public List<ItemFaqDTO> getListaItemFAQ() {
		return listaItemFAQ;
	}

	public void setListaItemFAQ(List<ItemFaqDTO> listaItemFAQ) {
		this.listaItemFAQ = listaItemFAQ;
	}

	private List<ItemFaqDTO> listaItemFAQ;
}

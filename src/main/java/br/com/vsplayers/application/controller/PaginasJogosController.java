package br.com.vsplayers.application.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.vsplayers.application.controller.forms.CadastrarJogoForm;
import br.com.vsplayers.domain.Console;
import br.com.vsplayers.domain.Jogo;
import br.com.vsplayers.dto.console.ConsoleDTO;
import br.com.vsplayers.dto.jogo.JogoDTO;
import br.com.vsplayers.exception.VsPlayersException;
import br.com.vsplayers.service.ConsoleService;
import br.com.vsplayers.service.JogoService;

@Controller
public class PaginasJogosController {
	private static final Logger logger = LoggerFactory
			.getLogger(PaginasJogosController.class);
	@Autowired
	@Qualifier("JogoServiceImpl")
	private JogoService jogoService;

	@Autowired
	@Qualifier("ConsoleServiceImpl")
	private ConsoleService consoleService;

	@RequestMapping(value = "/admin/jogos/listJogos", method = RequestMethod.GET)
	public ModelAndView listarJogos(HttpServletRequest request) {

		List<JogoDTO> listaJogos = jogoService.listarTodosJogos();

		ModelAndView returnObj = new ModelAndView("/admin/jogo/listJogo");
		returnObj.addObject("listaJogos", listaJogos);
		return returnObj;
	}

	@RequestMapping(value = "/admin/jogo/createJogo", method = RequestMethod.GET)
	public ModelAndView cadastrarJogo() {
		ModelAndView model = new ModelAndView();
		List<ConsoleDTO> listaConsoles = consoleService.listaTodosConsoles();
		model.addObject("cadastrarJogoForm", new CadastrarJogoForm());
		model.addObject("listaConsoles", listaConsoles);
		model.setViewName("/admin/jogo/createJogo");
		return model;
	}

	@RequestMapping(value = "/admin/jogo/cadastrarJogo", method = RequestMethod.POST)
	public ModelAndView cadastrar(
			@Valid @ModelAttribute CadastrarJogoForm cadastrarJogoform,
			BindingResult bindingResult, HttpServletRequest request) {
		List<ConsoleDTO> listaConsoles = consoleService.listaTodosConsoles();
		ModelAndView model = new ModelAndView();
		model.addObject("cadastrarJogoForm", cadastrarJogoform);
		model.setViewName("/admin/jogo/createJogo");

		if (bindingResult.hasErrors()) {
			model.addObject("listaConsoles", listaConsoles);
			return model;
		} else {
			if (cadastrarJogoform.getNomeJogo() == null) {
				bindingResult.addError(new ObjectError("Nome do Jogo",
						"Por favor insira um nome para o Jogo!"));

				return model;
			} else if (cadastrarJogoform.getIdConsole() == null) {
				bindingResult.addError(new ObjectError("Nome do Console",
						"Por favor insira o nome do console!"));

				return model;
			} else if (cadastrarJogoform.getTipoJogo() == null) {
				bindingResult.addError(new ObjectError("Tipo de Jogo",
						"Por favor insira o tipo de jogo!"));

			}

			else {
				try {
					Jogo novoJogo = new Jogo();

					novoJogo.setNmJogo(cadastrarJogoform.getNomeJogo());
					novoJogo.setDtCadastro(new Date());
					novoJogo.setTipoJogo(cadastrarJogoform.getTipoJogo());
					Console console = new Console();
					console = consoleService.ConsolegetById(cadastrarJogoform
							.getIdConsole());
					novoJogo.setConsole(console);
					jogoService.inserirJogo(novoJogo);

				} catch (VsPlayersException e) {
					logger.error(e.getMessage());
					e.printStackTrace();
					bindingResult
							.addError(new ObjectError("ERRO",
									"Ocorreu um erro ao tentar gravar uma Jogo, contate o administrador"));
					return model;

				}
			}
		}
		return this.listarJogos(request);

	}

	@RequestMapping(value = "/admin/jogo/editJogo", method = RequestMethod.GET)
	public ModelAndView editJogo(
			@RequestParam(value = "idJogoSelecionado") Long idJogoSelecionado,
			HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		List<ConsoleDTO> listaConsoles = consoleService.listaTodosConsoles();
		Jogo jogoEdicao = jogoService.getById(idJogoSelecionado);

		CadastrarJogoForm formEdicao = new CadastrarJogoForm();
		formEdicao.setId(jogoEdicao.getId());
		formEdicao.setNomeJogo(jogoEdicao.getNmJogo());

		formEdicao.setIdConsole(jogoEdicao.getConsole().getId());
		formEdicao.setTipoJogo(jogoEdicao.getTipoJogo());
		model.addObject("listaConsoles", listaConsoles);
		model.addObject("cadastrarJogoForm", formEdicao);
		model.setViewName("/admin/jogo/editJogo");
		return model;
	}

	@RequestMapping(value = "/admin/jogo/editarJogo", method = RequestMethod.POST)
	public ModelAndView editar(
			@Valid @ModelAttribute CadastrarJogoForm cadastrarJogoForm,
			BindingResult bindingResult, HttpServletRequest request) {
		List<ConsoleDTO> listaConsoles = consoleService.listaTodosConsoles();
		ModelAndView model = new ModelAndView();
		model.addObject("cadastrarJogoForm", cadastrarJogoForm);
		model.setViewName("/admin/jogo/editJogo");

		if (bindingResult.hasErrors()) {
			model.addObject("listaConsoles", listaConsoles);
			return model;
		} else {
			if (cadastrarJogoForm.getNomeJogo() == null) {
				bindingResult.addError(new ObjectError("Nome do Jogo",
						"Por favor insira um nome para a Jogo!"));

				return model;
			} else if (cadastrarJogoForm.getIdConsole() == null) {
				bindingResult.addError(new ObjectError("Nome do Console",
						"Por favor insira o nome do console"));

				return model;
			} else if (cadastrarJogoForm.getTipoJogo() == null) {
				bindingResult.addError(new ObjectError("Tipo de Jogo",
						"Por favor insira o tipo de jogo"));

			}

			else {
			Jogo jogoEdicao = jogoService.getById(cadastrarJogoForm.getId());
			Console console = new Console();
			console = consoleService.ConsolegetById(cadastrarJogoForm
					.getIdConsole());
			jogoEdicao.setNmJogo(cadastrarJogoForm.getNomeJogo());
			jogoEdicao.setTipoJogo(cadastrarJogoForm.getTipoJogo());
			jogoEdicao.setConsole(console);

			try {
				jogoService.atualizarJogo(jogoEdicao);
			} catch (VsPlayersException e) {
				logger.error(e.getMessage());
				bindingResult
						.addError(new ObjectError("ERRO",
								"Ocorreu um erro ao tentar gravar um Jogo, contate o administrador"));
				return model;
			}
		}
		}
		return listarJogos(request);

	}

	@RequestMapping(value = "/admin/jogo/deleteJogo", method = RequestMethod.GET)
	public ModelAndView deleteJogo(
			@RequestParam(value = "idJogoSelecionado") Long idJogoSelecionado,
			HttpServletRequest request) {
		jogoService.deleteJogo(idJogoSelecionado);
		return this.listarJogos(request);
	}

	@RequestMapping(value = "/admin/jogo/viewJogo/{id}", method = RequestMethod.GET)
	public ModelAndView viewPerfil(@PathVariable Long id) {
		Jogo jogo = jogoService.getById(id);
		if (jogo != null) {
			ModelAndView model = new ModelAndView();
			model.setViewName("/admin/jogo/viewJogo");
			model.addObject("jogo", jogo);
			return model;
		} else {
			return null;
		}
	}

	
}

package br.com.vsplayers.application.controller.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CadastrarItemFaqForm {
	private long id;
	private long idFaq;
	@NotNull(message = "O titulo do Item deve ser informado")
	@Size(min = 3, max = 255, message = "Valor informado para o titulo do item é inválido")
	private String dsTituloItem;
	@NotNull(message = "O texto do Item deve ser informado")
	@Size(min = 3, message = "Valor informado para a resposta do item é inválido")
	private String txItem;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getIdFaq() {
		return idFaq;
	}

	public void setIdFaq(long idFaq) {
		this.idFaq = idFaq;
	}

	public String getDsTituloItem() {
		return dsTituloItem;
	}

	public void setDsTituloItem(String dsTituloItem) {
		this.dsTituloItem = dsTituloItem;
	}

	public String getTxItem() {
		return txItem;
	}

	public void setTxItem(String txItem) {
		this.txItem = txItem;
	}

}

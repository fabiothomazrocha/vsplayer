package br.com.vsplayers.application.controller.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.vsplayers.domain.enums.TipoJogo;

public class CadastrarJogoForm {
	
	private long id;
	@NotNull(message = "O nome do jogo deve ser informado")
	@Size(min = 3, max = 255, message = "Valor informado para o nome do jogo é inválido")
	private String nomeJogo;
	@NotNull(message = "O tipo do jogo deve ser informado")
	private TipoJogo tipoJogo;
	private Long idConsole;
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getNomeJogo() {
		return nomeJogo;
	}

	public void setNomeJogo(String nomeJogo) {
		this.nomeJogo = nomeJogo;
	}

	public TipoJogo getTipoJogo() {
		return tipoJogo;
	}

	public void setTipoJogo(TipoJogo tipoJogo) {
		this.tipoJogo = tipoJogo;
	}

	public Long getIdConsole() {
		return idConsole;
	}

	public void setIdConsole(Long idConsole) {
		this.idConsole = idConsole;
	}

	

}

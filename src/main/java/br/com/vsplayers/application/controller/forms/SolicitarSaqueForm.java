package br.com.vsplayers.application.controller.forms;

import org.hibernate.validator.constraints.NotEmpty;

public class SolicitarSaqueForm {

	@NotEmpty(message = "Informe o Banco")
	private String nomeBanco;
	
	@NotEmpty(message = "Informe o CPF")
	private String cpf;

	@NotEmpty(message = "Informe a agência")
	private String agencia;

	@NotEmpty(message = "Informe o número da Conta")
	private String numeroConta;

	@NotEmpty(message = "Informe o nome do Titular")
	private String nomeTitular;
	
	private String valor;

	public String getNomeBanco() {
		return nomeBanco;
	}

	public void setNomeBanco(String nomeBanco) {
		this.nomeBanco = nomeBanco;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getNumeroConta() {
		return numeroConta;
	}

	public void setNumeroConta(String numeroConta) {
		this.numeroConta = numeroConta;
	}

	public String getNomeTitular() {
		return nomeTitular;
	}

	public void setNomeTitular(String nomeTitular) {
		this.nomeTitular = nomeTitular;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
}
package br.com.vsplayers.application.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.vsplayers.application.controller.forms.CadastrarAcessoRedeJogoForm;
import br.com.vsplayers.domain.AcessoRedeJogo;
import br.com.vsplayers.domain.Console;
import br.com.vsplayers.domain.enums.Flag;
import br.com.vsplayers.dto.acessoredejogo.AcessoRedeJogoDTO;
import br.com.vsplayers.dto.console.ConsoleDTO;
import br.com.vsplayers.integration.beans.VsPlayersUserDetail;
import br.com.vsplayers.service.AcessoRedeJogoService;
import br.com.vsplayers.service.ConsoleService;

@Controller
public class PaginasAcessoRedeJogoController {

	@Autowired
	@Qualifier("AcessoRedeJogoServiceImpl")
	private AcessoRedeJogoService acessoRedeJogoService;

	@Autowired
	@Qualifier("ConsoleServiceImpl")
	private ConsoleService consoleService;

	@RequestMapping(value = "/restricted/acessoRedeJogo/createAcessoRedeJogo", method = RequestMethod.GET)
	public ModelAndView cadastrarNovo() {

		CadastrarAcessoRedeJogoForm cadForm = new CadastrarAcessoRedeJogoForm();

		ModelAndView model = new ModelAndView();
		model.addObject("cadastrarAcessoRedeJogoForm", cadForm);
		model.setViewName("/restricted/acessoRedeJogo/createAcessoRedeJogo");

		return model;
	}

	@RequestMapping(value = "/restricted/acessoRedeJogo/finalizarCadastro", method = RequestMethod.POST)
	public ModelAndView finalizarCadastro(
			@Valid @ModelAttribute CadastrarAcessoRedeJogoForm cadastrarAcessoRedeJogoForm,
			BindingResult bindingResult, HttpServletRequest request) {

		if (cadastrarAcessoRedeJogoForm.getConsolesSelecionados() == null
				|| cadastrarAcessoRedeJogoForm.getConsolesSelecionados()
						.isEmpty()) {

			bindingResult.addError(new ObjectError("Consoles",
					"É necessário selecionar ao menos um Console"));

			ModelAndView back = new ModelAndView(
					"/restricted/acessoRedeJogo/cadstrarPasso2");

			List<ConsoleDTO> listaConsole = consoleService
					.listarConsolePorTipoAcesso(cadastrarAcessoRedeJogoForm
							.getTipoAcessoRedeJogo());

			back.addObject("listaConsole", listaConsole);
			back.addObject("cadastrarAcessoRedeJogoForm",
					cadastrarAcessoRedeJogoForm);

			return back;

		}

		VsPlayersUserDetail user = (VsPlayersUserDetail) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		AcessoRedeJogo acs = new AcessoRedeJogo();
		acs.setAtivo(Flag.S);
		acs.setDataCriacao(new Date());
		acs.setTipoAcessoRedeJogo(cadastrarAcessoRedeJogoForm
				.getTipoAcessoRedeJogo());
		acs.setUserId(cadastrarAcessoRedeJogoForm.getUsuarioRede());
		acs.setUsuario(user.getUsuario());

		List<Console> consoles = consoleService
				.getConsolesByConsoleNames(cadastrarAcessoRedeJogoForm
						.getConsolesSelecionados());

		acs.setConsoles(consoles);

		acessoRedeJogoService.inserirNovoAcessoRede(acs);
		return this.listAcessoRede(request);
	}

	@RequestMapping(value = "/restricted/acessoRedeJogo/cadastrarAcessoRede", method = RequestMethod.POST)
	public ModelAndView passo2(
			@Valid @ModelAttribute CadastrarAcessoRedeJogoForm cadastrarAcessoRedeJogoForm,
			BindingResult bindingResult, HttpServletRequest request) {

		ModelAndView back = new ModelAndView(
				"/restricted/acessoRedeJogo/createAcessoRedeJogo");

		if (StringUtils.isEmpty(cadastrarAcessoRedeJogoForm.getUsuarioRede())) {

			bindingResult.addError(new ObjectError("AcessoRede",
					"É necessário informar o apelido da sua Rede"));

			return back;
		}

		if (cadastrarAcessoRedeJogoForm.getTipoAcessoRedeJogo() == null) {

			bindingResult
					.addError(new ObjectError("AcessoRede",
							"É necessário informar a rede em que você está cadastrado"));

			return back;
		}

		List<ConsoleDTO> listaConsole = consoleService
				.listarConsolePorTipoAcesso(cadastrarAcessoRedeJogoForm
						.getTipoAcessoRedeJogo());

		ModelAndView model = new ModelAndView();
		model.addObject("cadastrarAcessoRedeJogoForm",
				cadastrarAcessoRedeJogoForm);

		model.addObject("listaConsole", listaConsole);
		model.setViewName("/restricted/acessoRedeJogo/cadstrarPasso2");

		return model;
	}

	@RequestMapping(value = "/restricted/acessoRedeJogo/listAcessoRedeJogo", method = RequestMethod.GET)
	public ModelAndView listAcessoRede(HttpServletRequest request) {

		VsPlayersUserDetail user = (VsPlayersUserDetail) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		List<AcessoRedeJogoDTO> acessoRedeList = acessoRedeJogoService
				.listAllByUser(user.getUsuario().getId());

		ModelAndView model = new ModelAndView();
		model.addObject("listaAcesso", acessoRedeList);
		model.setViewName("/restricted/acessoRedeJogo/listAcessoRedeJogo");
		return model;
	}

	@RequestMapping(value = "/restricted/acessoRedeJogo/deleteAcessoRedeJogo", method = RequestMethod.GET)
	public ModelAndView deletePerfil(
			@RequestParam(value = "idAcessoRedeJogo") Long idAcessoRedeJogo,
			HttpServletRequest request) {

		acessoRedeJogoService.deletePerfil(idAcessoRedeJogo);

		return this.listAcessoRede(request);
	}

}

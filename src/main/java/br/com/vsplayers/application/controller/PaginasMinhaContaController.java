package br.com.vsplayers.application.controller;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import br.com.vsplayers.application.controller.forms.AlterarDadosForm;
import br.com.vsplayers.application.controller.forms.AlterarSenhaForm;
import br.com.vsplayers.domain.Usuario;
import br.com.vsplayers.infra.utils.CriptografiaUtils;
import br.com.vsplayers.integration.beans.VsPlayersUserDetail;
import br.com.vsplayers.service.AcessoRedeJogoService;
import br.com.vsplayers.service.PerfilService;
import br.com.vsplayers.service.UsuarioService;

@Controller
public class PaginasMinhaContaController {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(PaginasMinhaContaController.class);

	@Autowired
	@Qualifier("UsuarioServiceImpl")
	private UsuarioService usuarioService;

	@Autowired
	@Qualifier("PerfilServiceImpl")
	private PerfilService perfilService;

	@Autowired
	@Qualifier("AcessoRedeJogoServiceImpl")
	private AcessoRedeJogoService acessoRedeJogoService;

	@RequestMapping(value = "/restricted/minhaConta/alterarDados", method = RequestMethod.GET)
	public ModelAndView alterarDados() {

		VsPlayersUserDetail user = (VsPlayersUserDetail) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		Usuario usuario = user.getUsuario();

		ModelAndView model = new ModelAndView();
		model.setViewName("/restricted/minhaConta/alterarDados");
		model.addObject("usuario", usuario);
		model.addObject("alterarDadosForm", new AlterarDadosForm());
		return model;
	}

	@RequestMapping(value = "/restricted/minhaConta/alterarDados", method = RequestMethod.POST)
	public ModelAndView alteraSenha(@Valid AlterarDadosForm alterarDadosForm,
			BindingResult bindingResult, WebRequest request) {

		ModelAndView model = new ModelAndView(
				"/restricted/minhaConta/alterarDados");

		String novoEmail = alterarDadosForm.getNovoEmail();

		model.addObject("alterarDadosForm", alterarDadosForm);

		VsPlayersUserDetail user = (VsPlayersUserDetail) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		Usuario usuario = user.getUsuario();

		if (bindingResult.hasErrors()) {
			return model;
		} else {
			Boolean jaExiste = usuarioService
					.verificaUsuarioExistePorEmail(novoEmail);
			if (jaExiste) {
				bindingResult.addError(new ObjectError("erro",
						"Este e-mail já está cadastrado no sistema!"));
				return model;
			}
		}

		Boolean alterouEmail = usuarioService.alterarEmail(usuario.getId(),
				alterarDadosForm.getNovoEmail());

		if (alterouEmail) {
			
			SecurityContextHolder
			.getContext().getAuthentication().setAuthenticated(false);
			
			ModelAndView modelAlterado = new ModelAndView("/emailAlterado");
			return modelAlterado;
		} else {
			bindingResult.addError(new ObjectError("erro",
					"Ocorreu um erro ao tentar alterar o e-mail"));
			return model;
		}
	}

	@RequestMapping(value = "/restricted/minhaConta/viewConta", method = RequestMethod.GET)
	public ModelAndView minhaConta() {

		VsPlayersUserDetail user = (VsPlayersUserDetail) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		Usuario usuario = user.getUsuario();

		Integer perfilCount = perfilService.countPerfisAtivosUsuario(usuario
				.getId());
		Integer acessoRedeCount = acessoRedeJogoService
				.countAcessoRedeAtivoUsuario(usuario.getId());

		ModelAndView model = new ModelAndView();
		model.addObject("usuario", usuario);
		model.addObject("acessoRedeCount", acessoRedeCount);
		model.addObject("perfilCount", perfilCount);
		model.setViewName("/restricted/minhaConta/viewConta");

		return model;
	}

	@RequestMapping(value = "/restricted/minhaConta/alterarSenha", method = RequestMethod.GET)
	public ModelAndView alterarSenha() {

		VsPlayersUserDetail user = (VsPlayersUserDetail) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		Usuario usuario = user.getUsuario();

		ModelAndView model = new ModelAndView(
				"/restricted/minhaConta/alterarSenha");
		model.addObject("usuario", usuario);
		model.addObject("alterarSenhaForm", new AlterarSenhaForm());
		return model;
	}

	@RequestMapping(value = "/restricted/minhaConta/alterarSenha", method = RequestMethod.POST)
	public ModelAndView alteraSenha(@Valid AlterarSenhaForm alterarSenhaForm,
			BindingResult bindingResult, WebRequest request) {

		VsPlayersUserDetail user = (VsPlayersUserDetail) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		Usuario usuario = user.getUsuario();

		ModelAndView model = new ModelAndView(
				"/restricted/minhaConta/alterarSenha");
		model.addObject("alterarSenhaForm", alterarSenhaForm);

		if (bindingResult.hasErrors()) {
			return model;
		}

		if (StringUtils.isEmpty(alterarSenhaForm.getSenhaAntiga())) {
			bindingResult.addError(new ObjectError("Erro",
					"Informar a senha atual"));
			return model;
		} else {
			if (!CriptografiaUtils.toMD5(alterarSenhaForm.getSenhaAntiga())
					.equals(usuario.getDsSenha())) {
				bindingResult.addError(new ObjectError("Erro",
						"Senha antiga inválida"));
				return model;
			}
		}

		if (alterarSenhaForm.getNovaSenha().equals(
				alterarSenhaForm.getRepitaNovaSenha())) {

			Boolean result = usuarioService.alterarSenha(usuario.getId(),
					alterarSenhaForm.getNovaSenha());

			if (result) {

				ModelAndView modelOk = new ModelAndView();
				modelOk.addObject("mensagemSucesso",
						"Senha alterada com sucesso!");
				modelOk.setViewName("/restricted/minhaConta/alterarSenha");

				usuario.setDsSenha(CriptografiaUtils.toMD5(alterarSenhaForm
						.getNovaSenha()));

				return modelOk;

			} else {
				bindingResult.addError(new ObjectError("Erro",
						"Ocorreu um erro, contate o administrador"));
				return model;
			}

		} else {
			bindingResult.addError(new ObjectError("Senha",
					"As senhas digitadas devem ser iguais"));
			return model;
		}
	}

}
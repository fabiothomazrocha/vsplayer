package br.com.vsplayers.application.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.vsplayers.infra.constant.URL;
import br.com.vsplayers.infra.constant.View;

@Controller
public class HomeController {

	@RequestMapping(URL.HOME)
	public ModelAndView home(HttpServletResponse response) throws IOException {
		return new ModelAndView(View.HOME);
	}
	
	@RequestMapping(value = "/restricted/welcome", method = RequestMethod.GET)
	public ModelAndView testeUser() {
	  ModelAndView model = new ModelAndView();
	  model.setViewName("/restricted/welcome");
	  return model;
	}
	
	@RequestMapping(value = "/admin/welcome", method = RequestMethod.GET)
	public ModelAndView testeAdmin() {
	  ModelAndView model = new ModelAndView();
	  model.setViewName("/admin/welcome");
	  return model;
	}
}

package br.com.vsplayers.application.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.vsplayers.application.controller.forms.CadastrarConsoleForm;
import br.com.vsplayers.domain.Console;
import br.com.vsplayers.domain.enums.Flag;
import br.com.vsplayers.dto.console.ConsoleDTO;
import br.com.vsplayers.exception.VsPlayersException;
import br.com.vsplayers.service.ConsoleService;
import br.com.vsplayers.service.JogoService;

@Controller
public class PaginasConsoleController {
	private static final Logger logger = LoggerFactory
			.getLogger(PaginasConsoleController.class);
	@Autowired
	@Qualifier("ConsoleServiceImpl")
	private ConsoleService consoleService;

	@Autowired
	@Qualifier("JogoServiceImpl")
	private JogoService jogoService;

	public JogoService getJogoService() {
		return jogoService;
	}

	public void setJogoService(JogoService jogoService) {
		this.jogoService = jogoService;
	}

	@RequestMapping(value = "/admin/console/listConsole", method = RequestMethod.GET)
	public ModelAndView listarConsoles(HttpServletRequest request) {

		List<ConsoleDTO> listaConsole = consoleService.listaTodosConsoles();

		ModelAndView returnObj = new ModelAndView("/admin/console/listConsole");
		returnObj.addObject("listaConsole", listaConsole);
		return returnObj;
	}

	@RequestMapping(value = "/admin/console/cadastrarConsole", method = RequestMethod.POST)
	public ModelAndView cadastrar(
			@Valid @ModelAttribute CadastrarConsoleForm cadastrarConsoleform,
			BindingResult bindingResult, HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		model.addObject("cadastrarConsoleForm", cadastrarConsoleform);
		model.setViewName("/admin/console/createConsole");

		if (bindingResult.hasErrors()) {
			return model;
		} else {
			if (cadastrarConsoleform.getNmConsole() == null) {
				bindingResult.addError(new ObjectError("Nome do Console",
						"Por favor insira um nome para o Console!"));

				return model;
			} else if (cadastrarConsoleform.getTipoConsole() == null) {
				bindingResult.addError(new ObjectError("Tipo de Console",
						"Por favor insira o tipo de Console"));

				return model;
			} else if (cadastrarConsoleform.getTipoRede() == null) {
				bindingResult.addError(new ObjectError("Tipo de Rede",
						"Por favor insira o tipo de rede"));

			}

			else {
				try {

					Console console = new Console();
					console.setNmConsole(cadastrarConsoleform.getNmConsole());
					console.setTipoConsole(cadastrarConsoleform
							.getTipoConsole());
					console.setTipoRede(cadastrarConsoleform.getTipoRede());
					console.setAtivo(Flag.S);
					consoleService.inserirConsole(console);

				} catch (VsPlayersException e) {
					logger.error(e.getMessage());
					e.printStackTrace();
					bindingResult
							.addError(new ObjectError("ERRO",
									"Ocorreu um erro ao tentar gravar um Console, contate o administrador"));
					return model;

				}
			}
		}
		return this.listarConsoles(request);

	}

	@RequestMapping(value = "/admin/console/editConsole", method = RequestMethod.GET)
	public ModelAndView editConsole(
			@RequestParam(value = "idConsoleSelecionado") Long idConsoleSelecionado,
			HttpServletRequest request) {

		ModelAndView model = new ModelAndView();

		Console consoleEdicao = consoleService
				.ConsolegetById(idConsoleSelecionado);

		CadastrarConsoleForm formEdicao = new CadastrarConsoleForm();
		formEdicao.setId(consoleEdicao.getId());
		formEdicao.setNmConsole(consoleEdicao.getNmConsole());
		formEdicao.setTipoConsole(consoleEdicao.getTipoConsole());
		formEdicao.setTipoRede(consoleEdicao.getTipoRede());

		model.addObject("cadastrarConsoleForm", formEdicao);
		model.setViewName("/admin/console/editConsole");
		return model;
	}

	@RequestMapping(value = "/admin/console/editarConsole", method = RequestMethod.POST)
	public ModelAndView editar(
			@Valid @ModelAttribute CadastrarConsoleForm cadastrarConsoleForm,
			BindingResult bindingResult, HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		model.addObject("cadastrarConsoleForm", cadastrarConsoleForm);
		model.setViewName("/admin/console/editConsole");

		if (bindingResult.hasErrors()) {
			return model;
		} else {

			Console consoleEdicao = consoleService
					.ConsolegetById(cadastrarConsoleForm.getId());
			consoleEdicao.setNmConsole(cadastrarConsoleForm.getNmConsole());
			consoleEdicao.setTipoConsole(cadastrarConsoleForm.getTipoConsole());
			consoleEdicao.setTipoRede(cadastrarConsoleForm.getTipoRede());
			try {
				consoleService.atualizarConsole(consoleEdicao);
			} catch (VsPlayersException e) {
				logger.error(e.getMessage());
				bindingResult
						.addError(new ObjectError("ERRO",
								"Ocorreu um erro ao tentar gravar um Console, contate o administrador"));
				return model;
			}
		}
		return listarConsoles(request);

	}

	@RequestMapping(value = "/admin/console/createConsole", method = RequestMethod.GET)
	public ModelAndView cadastrarConsole() {
		ModelAndView model = new ModelAndView();
		CadastrarConsoleForm cadastrarConsoleForm = new CadastrarConsoleForm();
		model.addObject("cadastrarConsoleForm", cadastrarConsoleForm);
		model.setViewName("/admin/console/createConsole");
		return model;
	}

	@RequestMapping(value = "/admin/console/deleteConsole", method = RequestMethod.GET)
	public ModelAndView deleteConsole(
			@RequestParam(value = "idConsoleSelecionado") Long idConsoleSelecionado,
			HttpServletRequest request) {
		consoleService.deleteConsole(idConsoleSelecionado);
		return this.listarConsoles(request);
	}

	@RequestMapping(value = "/admin/console/viewConsole/{id}", method = RequestMethod.GET)
	public ModelAndView viewPerfil(@PathVariable Long id) {
		Console console = consoleService.ConsolegetById(id);
		if (console != null) {
			ModelAndView model = new ModelAndView();
			model.setViewName("/admin/console/viewConsole");
			model.addObject("console", console);
			return model;
		} else {
			return null;
		}
	}

}

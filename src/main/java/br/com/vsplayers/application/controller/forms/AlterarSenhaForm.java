package br.com.vsplayers.application.controller.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AlterarSenhaForm {
	
	private String senhaAntiga;
	
	@NotNull(message = "Sua senha deve ser informada")
	@Size(min = 5, max = 20, message = "O valor informado para a senha é inválido")
	private String novaSenha;
	
	@NotNull(message = "Sua senha deve ser informada")
	@Size(min = 5, max = 20, message = "O valor informado para a confirmação de senha é inválido")
	private String repitaNovaSenha;
	
	private String codigoAlterarSenha;
	
	public String getNovaSenha() {
		return novaSenha;
	}
	
	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}
	
	public String getRepitaNovaSenha() {
		return repitaNovaSenha;
	}
	
	public void setRepitaNovaSenha(String repitaNovaSenha) {
		this.repitaNovaSenha = repitaNovaSenha;
	}

	public String getCodigoAlterarSenha() {
		return codigoAlterarSenha;
	}

	public void setCodigoAlterarSenha(String codigoAlterarSenha) {
		this.codigoAlterarSenha = codigoAlterarSenha;
	}

	public String getSenhaAntiga() {
		return senhaAntiga;
	}

	public void setSenhaAntiga(String senhaAntiga) {
		this.senhaAntiga = senhaAntiga;
	}

}

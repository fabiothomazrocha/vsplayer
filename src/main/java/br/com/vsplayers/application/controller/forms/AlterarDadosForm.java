package br.com.vsplayers.application.controller.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AlterarDadosForm {
	
	@NotNull(message = "Sua novo e-mail deve ser informado")
	@Size(min = 5, max = 255, message = "O informado para o e-mail é inválido")
	private String novoEmail;

	public String getNovoEmail() {
		return novoEmail;
	}

	public void setNovoEmail(String novoEmail) {
		this.novoEmail = novoEmail;
	}

}

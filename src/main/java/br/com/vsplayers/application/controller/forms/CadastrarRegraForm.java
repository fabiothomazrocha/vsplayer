package br.com.vsplayers.application.controller.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CadastrarRegraForm {

	private Long id;
	@NotNull(message = "O nome da regra deve ser informado")
	@Size(min = 5, max = 255, message = "Valor informado para o nome da regra é inválido")
	private String nmInstancia;
	@NotNull(message = "A descrição da regra deve ser informada")
	@Size(min = 10, max = 400, message = "O tamanho da sua descrição deve ser de 10 a 255 caracteres.")
	private String txItem;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmInstancia() {
		return nmInstancia;
	}

	public void setNmInstancia(String nmInstancia) {
		this.nmInstancia = nmInstancia;
	}

	public String getTxItem() {
		return txItem;
	}

	public void setTxItem(String txItem) {
		this.txItem = txItem;
	}

	
	
}

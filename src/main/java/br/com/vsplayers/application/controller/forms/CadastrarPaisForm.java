package br.com.vsplayers.application.controller.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CadastrarPaisForm {
	private Long id;
	@NotNull(message = "O nome do País deve ser informado")
	@Size(min = 3, max = 30, message = "Valor informado para o nome do País é inválido")
	private String nmPais;
	@NotNull(message = "O código do País deve ser informado")
	@Size(min = 1, max = 255, message = "Valor informado para o código do País é inválido")
	private String cdPais;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmPais() {
		return nmPais;
	}

	public void setNmPais(String nmPais) {
		this.nmPais = nmPais;
	}

	public String getCdPais() {
		return cdPais;
	}

	public void setCdPais(String cdPais) {
		this.cdPais = cdPais;
	}

}

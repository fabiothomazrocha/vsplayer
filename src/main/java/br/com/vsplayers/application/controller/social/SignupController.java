/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.vsplayers.application.controller.social;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import br.com.vsplayers.application.controller.forms.CadastrarFacebookForm;
import br.com.vsplayers.domain.Usuario;
import br.com.vsplayers.infra.utils.ValidaCPF;
import br.com.vsplayers.service.UsuarioService;
import br.com.vsplayers.service.social.SimpleSocialUsersDetailService;

@Controller
public class SignupController {

	@Autowired
	@Qualifier("UsuarioServiceImpl")
	private UsuarioService usuarioService;

	@Autowired
	@Qualifier("SimpleSocialUsersDetailService")
	SimpleSocialUsersDetailService simpleSocialUsersDetailService;

	private final ProviderSignInUtils providerSignInUtils = new ProviderSignInUtils();

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public ModelAndView signupForm(WebRequest request) {

		Connection<?> connection = providerSignInUtils
				.getConnectionFromSession(request);
		
		CadastrarFacebookForm formCadastro = new CadastrarFacebookForm();

		if (connection != null) {
			UserProfile userProfile = connection.fetchUserProfile();
			formCadastro.setNomeUser(userProfile.getName());

			if (!StringUtils.isEmpty(userProfile.getEmail())) {
				formCadastro.setEmailUser(userProfile.getEmail());
			}
			
			request.setAttribute("facebook", Boolean.TRUE,
					WebRequest.SCOPE_REQUEST);

			ModelAndView model = new ModelAndView();
			model.addObject("cadastrarFacebookForm", formCadastro);
			model.setViewName("/signup");
			return model;
		}
		
		return new ModelAndView("/error");
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public ModelAndView signup(@Valid CadastrarFacebookForm cadastrar,
			BindingResult bindingResult, WebRequest request) {

		ModelAndView model = new ModelAndView();
		model.setViewName("/signup");

		String email = cadastrar.getEmailUser();

		if (bindingResult.hasErrors()) {

			request.setAttribute("facebook", Boolean.TRUE,
					WebRequest.SCOPE_REQUEST);

			return model;

		} else {

			if (usuarioService.verificaUsuarioExistePorEmail(email)) {

				bindingResult.addError(new ObjectError("E-mail",
						"O e-mail informado já está cadastrado no sistema."));

				request.setAttribute("facebook", Boolean.TRUE,
						WebRequest.SCOPE_REQUEST);

				return model;
			}
			
			String cpfnoMask = cadastrar.getCpf().
					replaceAll("\\D", "");
			
			if(!ValidaCPF.isCPF(cpfnoMask)) {
				bindingResult.addError(new ObjectError("CPF",
						"O CPF informado não é válido."));

				request.setAttribute("facebook", Boolean.TRUE,
						WebRequest.SCOPE_REQUEST);
				
				return model;
			}
			
		}
		
		String cpf = cadastrar.getCpf();
		String nomeCompleto = cadastrar.getNomeUser();

		Usuario usuario = usuarioService.inserirUsuarioFacebook(nomeCompleto,
				email, cpf);

		if (usuario != null) {

			request.setAttribute("nomeUsuario", usuario.getNmNome(),
					WebRequest.SCOPE_REQUEST);

			request.setAttribute("emailResend", usuario.getDsEmail(),
					WebRequest.SCOPE_REQUEST);
			
			providerSignInUtils.doPostSignUp(usuario.getDsEmail(), request);
			
			ModelAndView modelOk = new ModelAndView();
			modelOk.setViewName("/cadastroRealizado");
			return modelOk;

		} else {

			bindingResult.addError(new ObjectError("Erro!",
					"Ops... ocorreu um erro, tente novamente."));

			return model;
		}
	}

}

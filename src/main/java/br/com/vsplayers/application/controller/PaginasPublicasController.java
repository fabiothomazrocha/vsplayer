package br.com.vsplayers.application.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import br.com.vsplayers.application.controller.forms.AlterarSenhaForm;
import br.com.vsplayers.application.controller.forms.CadastrarForm;
import br.com.vsplayers.domain.Usuario;
import br.com.vsplayers.infra.constant.ConfigConstants;
import br.com.vsplayers.infra.utils.ValidaCPF;
import br.com.vsplayers.integration.beans.VsPlayersUserDetail;
import br.com.vsplayers.service.UsuarioAssembler;
import br.com.vsplayers.service.UsuarioService;

@Controller
public class PaginasPublicasController {

	@Autowired
	@Qualifier("UsuarioServiceImpl")
	private UsuarioService usuarioService;
	
	@Autowired
	@Qualifier("UsuarioAssemblerImpl")
	private UsuarioAssembler usuarioAssembler;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView loginMe() {
		return new ModelAndView("/login");
	}

	@RequestMapping(value = "/acessoNegado", method = RequestMethod.GET)
	public ModelAndView acessoNegado() {
		return new ModelAndView("/acessoNegado");
	}

	@RequestMapping(value = "/error-login", method = RequestMethod.GET)
	public ModelAndView invalidLogin() {
		ModelAndView modelAndView = new ModelAndView("/login");
		modelAndView.addObject("error", true);
		return modelAndView;
	}

	@RequestMapping(value = "/cadastroRealizado", method = RequestMethod.GET)
	public ModelAndView cadastroRealizado() {
		return new ModelAndView("/cadastroRealizado");
	}

	@RequestMapping(value = "/cadastrar", method = RequestMethod.GET)
	public ModelAndView getCadastrar(WebRequest request) {
		request.setAttribute("vsPlayers", Boolean.TRUE,
				WebRequest.SCOPE_REQUEST);
		ModelAndView model = new ModelAndView();

		CadastrarForm formCadastro = new CadastrarForm();
		model.addObject("cadastrarForm", formCadastro);
		model.setViewName("/signup");
		return model;
	}

	@RequestMapping(value = "/cadastrar", method = RequestMethod.POST)
	public ModelAndView cadastrar(@Valid CadastrarForm cadastrar,
			BindingResult bindingResult, WebRequest request) {

		ModelAndView model = new ModelAndView();
		model.setViewName("/signup");

		String email = cadastrar.getEmailUser();

		if (bindingResult.hasErrors()) {
			request.setAttribute("vsPlayers", Boolean.TRUE,
					WebRequest.SCOPE_REQUEST);
			return model;
		} else {
			if (usuarioService.verificaUsuarioExistePorEmail(email)) {

				bindingResult.addError(new ObjectError("E-mail",
						"O e-mail informado já está cadastrado no sistema."));

				request.setAttribute("vsPlayers", Boolean.TRUE,
						WebRequest.SCOPE_REQUEST);
				return model;
			}

			String cpfnoMask = cadastrar.getCpf().replaceAll("\\D", "");

			if (!ValidaCPF.isCPF(cpfnoMask)) {
				bindingResult.addError(new ObjectError("CPF",
						"O CPF informado não é válido."));

				request.setAttribute("vsPlayers", Boolean.TRUE,
						WebRequest.SCOPE_REQUEST);
				return model;
			}
		}

		String senha = cadastrar.getSenha();
		String cpf = cadastrar.getCpf();
		String nomeCompleto = cadastrar.getNomeUser();

		// cadastra o usuario no banco de dados
		Usuario usuarioResult = usuarioService.inserirUsuarioVsPlayers(
				nomeCompleto, email, senha, cpf);

		if (usuarioResult != null) {

			request.setAttribute("nomeUsuario", usuarioResult.getNmNome(),
					WebRequest.SCOPE_REQUEST);

			request.setAttribute("emailResend", usuarioResult.getDsEmail(),
					WebRequest.SCOPE_REQUEST);

			ModelAndView modelOk = new ModelAndView();
			modelOk.setViewName("/cadastroRealizado");
			return modelOk;

		} else {

			bindingResult.addError(new ObjectError("Erro!",
					"Ops... ocorreu um erro, tente novamente."));

			return model;
		}
	}

	@RequestMapping(value = "/gerarNovoCodigo", method = RequestMethod.POST)
	public String gerarNovoCodigo(
			@RequestParam(required = true, value = "email") String email) {

		if (usuarioService.reenviaCodigoAtivacao(email)) {
			return "redirect:/cadastroRealizado?resent=true";
		} else {
			return "redirect:/cadastroRealizado?resent=false";
		}
	}

	@RequestMapping(value = "/ativaUsuario/{cod}", method = RequestMethod.GET)
	public String ativaUsuario(@PathVariable String cod) {

		Usuario usuario = usuarioService.ativaUsuario(cod);

		if (usuario != null) {
			
			VsPlayersUserDetail vsUser = usuarioAssembler.buildUserFromUserEntity(usuario);
			
			Authentication authentication = new UsernamePasswordAuthenticationToken(vsUser, null,
		            vsUser.getAuthorities());
			
			SecurityContextHolder.getContext().setAuthentication(authentication);
	        
			return "redirect:/restricted/welcome";
		}

		return "redirect:/";
	}

	@RequestMapping(value = "/getPassword", method = RequestMethod.GET)
	public ModelAndView getPassword() {
		return new ModelAndView("/recuperarSenha");
	}

	@RequestMapping(value = "/getPassword", method = RequestMethod.POST)
	public ModelAndView enviarSolicitacaoAlteracao(
			@RequestParam(required = true, value = "email") String email,
			WebRequest request) {

		usuarioService.solicitarAlteracaoSenha(email);

		request.setAttribute("mensagem",
				"O e-mail foi enviado, verifique sua caixa de e-mail",
				WebRequest.SCOPE_REQUEST);

		return new ModelAndView("/recuperarSenha");
	}

	@RequestMapping(value = "/alteraSenha/{cod}", method = RequestMethod.GET)
	public ModelAndView getAlterarSenha(@PathVariable String cod,
			WebRequest request) {

		ModelAndView model = new ModelAndView("/alterarSenha");
		AlterarSenhaForm alteraForm = new AlterarSenhaForm();
		alteraForm.setCodigoAlterarSenha(cod);
		model.addObject("alterarSenhaForm", alteraForm);

		request.setAttribute("baseUrl", ConfigConstants.URL_SERVICO,
				WebRequest.SCOPE_REQUEST);

		return model;
	}

	@RequestMapping(value = "/alteraSenha", method = RequestMethod.POST)
	public ModelAndView alteraSenha(@Valid AlterarSenhaForm alterarSenhaForm,
			BindingResult bindingResult, WebRequest request) {

		ModelAndView model = new ModelAndView("/alterarSenha");
		model.addObject("alterarSenhaForm", alterarSenhaForm);

		if (bindingResult.hasErrors()) {
			return model;
		}

		if (alterarSenhaForm.getNovaSenha().equals(
				alterarSenhaForm.getRepitaNovaSenha())) {

			Boolean result = usuarioService.alterarSenha(
					alterarSenhaForm.getCodigoAlterarSenha(),
					alterarSenhaForm.getNovaSenha());

			if (result) {
				ModelAndView modelOk = new ModelAndView();
				modelOk.setViewName("/senhaAlterada");
				return modelOk;
			} else {
				bindingResult.addError(new ObjectError("Erro",
						"Ocorreu um erro, seu link parece inválido"));
				return model;
			}

		} else {
			bindingResult.addError(new ObjectError("Senha",
					"As senhas digitadas devem ser iguais"));
			return model;
		}
	}

	@RequestMapping(value = "/getCodigoAtivacao", method = RequestMethod.GET)
	public ModelAndView getCodigoAtivacao() {
		return new ModelAndView("/enviarCodigoAtivacao");
	}

	@RequestMapping(value = "/getCodigoAtivacao", method = RequestMethod.POST)
	public String getNovoCodigo(
			@RequestParam(required = true, value = "email") String email) {

		if (usuarioService.reenviaCodigoAtivacao(email)) {
			return "redirect:/getCodigoAtivacao?resent=true";
		} else {
			return "redirect:/getCodigoAtivacao?resent=false";
		}
	}

}
package br.com.vsplayers.application.controller.forms;

import org.hibernate.validator.constraints.NotEmpty;

public class DepositoForm {
	
	@NotEmpty(message = "Valor do Depósito.")
	private String valorDeposito;

	public String getValorDeposito() {
		return valorDeposito;
	}

	public void setValorDeposito(String valorDeposito) {
		this.valorDeposito = valorDeposito;
	}
	
}
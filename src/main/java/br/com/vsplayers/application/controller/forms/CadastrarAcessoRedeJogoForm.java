package br.com.vsplayers.application.controller.forms;

import java.util.List;

import br.com.vsplayers.domain.enums.TipoAcessoRedeJogo;

public class CadastrarAcessoRedeJogoForm {

	private String usuarioRede;

	private TipoAcessoRedeJogo tipoAcessoRedeJogo;

	private List<String> consolesSelecionados;

	public String getUsuarioRede() {
		return usuarioRede;
	}

	public void setUsuarioRede(String usuarioRede) {
		this.usuarioRede = usuarioRede;
	}

	public TipoAcessoRedeJogo getTipoAcessoRedeJogo() {
		return tipoAcessoRedeJogo;
	}

	public void setTipoAcessoRedeJogo(TipoAcessoRedeJogo tipoAcessoRedeJogo) {
		this.tipoAcessoRedeJogo = tipoAcessoRedeJogo;
	}

	public List<String> getConsolesSelecionados() {
		return consolesSelecionados;
	}

	public void setConsolesSelecionados(List<String> consolesSelecionados) {
		this.consolesSelecionados = consolesSelecionados;
	}

}
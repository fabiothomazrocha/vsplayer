package br.com.vsplayers.application.controller;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.vsplayers.application.controller.forms.DepositoForm;
import br.com.vsplayers.application.controller.forms.SolicitarSaqueForm;
import br.com.vsplayers.domain.TransacaoFinanceira;
import br.com.vsplayers.domain.Usuario;
import br.com.vsplayers.domain.enums.Flag;
import br.com.vsplayers.domain.enums.TipoTransacao;
import br.com.vsplayers.dto.OrderDTO;
import br.com.vsplayers.dto.TransacaoFinanceiraDTO;
import br.com.vsplayers.exception.VsPlayersException;
import br.com.vsplayers.infra.constant.ConfigConstants;
import br.com.vsplayers.infra.utils.PayPalUtils;
import br.com.vsplayers.infra.utils.StringUtils;
import br.com.vsplayers.infra.utils.ValidaCPF;
import br.com.vsplayers.integration.beans.VsPlayersUserDetail;
import br.com.vsplayers.service.TransacaoFinanceiraService;

import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payment;

@Controller
public class PaginasFinanceiroController {

	private static final Logger logger = LoggerFactory
			.getLogger(PaginasFinanceiroController.class);

	@Autowired
	@Qualifier("TransacaoFinanceiraServiceImpl")
	private TransacaoFinanceiraService transacaoFinanceiraService;

	public static final String UTF_8 = "UTF-8";

	@RequestMapping(value = "/restricted/financeiro/listFinanceiro", method = RequestMethod.GET)
	public ModelAndView listarFinanceiro() {

		VsPlayersUserDetail user = (VsPlayersUserDetail) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		Usuario usuario = user.getUsuario();

		List<TransacaoFinanceiraDTO> trxUsuario = transacaoFinanceiraService
				.listarTransacoesUsuario(usuario);

		ModelAndView returnObj = new ModelAndView(
				"/restricted/financeiro/listFinanceiro");

		BigDecimal saldo = transacaoFinanceiraService
				.verificaSaldoUsuario(usuario.getId());

		try {
			returnObj.addObject("saldoAtual",
					"R$ " + StringUtils.doubleToMonetarioString(saldo));
		} catch (ParseException e) {
			logger.error(e.getMessage());
		}

		returnObj.addObject("listaTrx", trxUsuario);
		return returnObj;
	}

	@RequestMapping(value = "/restricted/financeiro/novoDeposito", method = RequestMethod.GET)
	public ModelAndView novoDeposito() {

		ModelAndView returnObj = new ModelAndView(
				"/restricted/financeiro/novoDeposito");

		returnObj.addObject("depositoForm", new DepositoForm());
		return returnObj;
	}

	@RequestMapping(value = "/restricted/financeiro/confirmarDeposito", method = RequestMethod.POST)
	public ModelAndView confirmarDeposito(
			@Valid @ModelAttribute DepositoForm depositoForm,
			BindingResult bindingResult, HttpServletRequest request) {

		if (bindingResult.hasErrors()) {
			ModelAndView returnObj = new ModelAndView(
					"/restricted/financeiro/novoDeposito");
			returnObj.addObject("depositoForm", depositoForm);
			return returnObj;
		}

		ModelAndView returnObj = new ModelAndView(
				"/restricted/financeiro/revisarDeposito");
		returnObj.addObject("depositoForm", depositoForm);
		return returnObj;
	}

	@RequestMapping(value = "/restricted/financeiro/solicitarSaque", method = RequestMethod.GET)
	public ModelAndView solicitarSaque() {

		SolicitarSaqueForm solicitaSaqueForm = new SolicitarSaqueForm();

		ModelAndView returnObj = new ModelAndView(
				"/restricted/financeiro/solicitaSaque");

		returnObj.addObject("solicitarSaqueForm", solicitaSaqueForm);

		return returnObj;
	}

	@RequestMapping(value = "/restricted/financeiro/efetuarSolicitacao", method = RequestMethod.POST)
	public ModelAndView efetuarDeposito(
			@Valid @ModelAttribute SolicitarSaqueForm solicitarSaqueForm,
			BindingResult bindingResult, HttpServletRequest request) {

		ModelAndView returnError = new ModelAndView(
				"/restricted/financeiro/solicitaSaque");

		if (bindingResult.hasErrors()) {

			returnError.addObject("solicitarSaqueForm", solicitarSaqueForm);
			return returnError;

		} else {
			String cpfnoMask = solicitarSaqueForm.getCpf()
					.replaceAll("\\D", "");

			returnError.addObject("solicitarSaqueForm", solicitarSaqueForm);

			if (!ValidaCPF.isCPF(cpfnoMask)) {
				bindingResult.addError(new ObjectError("CPF",
						"O CPF informado não é válido."));
				return returnError;
			}
		}

		VsPlayersUserDetail user = (VsPlayersUserDetail) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		Usuario usuario = user.getUsuario();

		try {
			transacaoFinanceiraService.inserirNovaSolicitacaoSaque(
					usuario.getId(), solicitarSaqueForm.getValor(),
					solicitarSaqueForm.getAgencia(),
					solicitarSaqueForm.getNomeBanco(),
					solicitarSaqueForm.getNomeTitular(),
					solicitarSaqueForm.getNumeroConta(),
					solicitarSaqueForm.getCpf());
		} catch (VsPlayersException e) {

			ModelAndView returnErrorPage = new ModelAndView(
					"/restricted/financeiro/solicitaSaque");

			bindingResult.addError(new ObjectError("SALDO",
					"Saldo insuficiente ou Sistema indisponível"));

			return returnErrorPage;

		}

		ModelAndView returnSucces = new ModelAndView(
				"/restricted/financeiro/sucessoSolicitacao");

		return returnSucces;
	}

	@RequestMapping(value = "/admin/financeiro/listarTransacoesAprovar", method = RequestMethod.GET)
	public ModelAndView listarTransacoesParaAprovar() {

		ModelAndView returnObj = new ModelAndView(
				"/admin/financeiro/listaTransacoesAprovar");

		List<TransacaoFinanceiraDTO> trxUsuario = transacaoFinanceiraService
				.listarTransacoesTodosUsuarios();

		returnObj.addObject("listaTrx", trxUsuario);
		return returnObj;
	}

	@RequestMapping(value = "/restricted/financeiro/confirmarPagamentoAtual", method = RequestMethod.GET)
	public String confirmaTransacao(
			@RequestParam(value = "idTransacaoFinanceira") String idTransacaoFinanceira,
			@RequestParam(value = "token") String tokenAprovacao) {

		transacaoFinanceiraService.confirmaTransacao(
				Long.valueOf(idTransacaoFinanceira), tokenAprovacao);

		return "redirect:/restricted/financeiro/listFinanceiro";
	}

	@RequestMapping(value = "/admin/financeiro/listarTransacoesAprovadas", method = RequestMethod.GET)
	public ModelAndView listarTransacoesParaAprovadas() {

		ModelAndView returnObj = new ModelAndView(
				"/admin/financeiro/listaTransacoesAprovadas");

		List<TransacaoFinanceiraDTO> trxUsuario = transacaoFinanceiraService
				.listarTransacoesTodosUsuariosAprovadas();

		returnObj.addObject("listaTrx", trxUsuario);
		return returnObj;
	}

	@RequestMapping(value = "/restricted/financeiro/makePayment", method = RequestMethod.POST)
	public void processarPagamento(HttpServletResponse response,
			@RequestParam(value = "valorSaque") String valorSaque) {

		VsPlayersUserDetail user = (VsPlayersUserDetail) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		Usuario usuario = user.getUsuario();

		TransacaoFinanceira trx = new TransacaoFinanceira();
		trx.setAprovada(Flag.N);
		trx.setDtTransacao(new Date());
		trx.setNmTransacao("Compra de Créditos 1vs1");
		trx.setTipoTransacao(TipoTransacao.DEPOS);
		trx.setUsuario(usuario);
		trx.setValorTransacao(new BigDecimal(valorSaque));

		transacaoFinanceiraService.inserirNovoDeposito(trx);

		OrderDTO orderDetail = createOrderDetailFormRequest(trx
				.getValorTransacao().toString(), trx.getId());

		Payment payment = null;

		try {

			payment = PayPalUtils.createPayment(orderDetail);

			String redirectUrl = getApprovalURL(payment);
			response.sendRedirect(redirectUrl);

		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}

	}

	private String getApprovalURL(Payment payment)
			throws UnsupportedEncodingException {

		String redirectUrl = null;
		List<Links> links = payment.getLinks();
		for (Links l : links) {
			if (l.getRel().equalsIgnoreCase("approval_url")) {
				redirectUrl = URLDecoder.decode(l.getHref(), UTF_8);
				break;
			}
		}
		return redirectUrl;
	}

	private OrderDTO createOrderDetailFormRequest(String valor,
			Long idTransacaoFinanceira) {

		OrderDTO orderDetail = new OrderDTO();
		orderDetail.setOrderAmount(valor);
		orderDetail.setOrderDesc("Compra de Créditos 1vs1");
		orderDetail.setPaymentIntent("sale");
		orderDetail.setCurrency("BRL");
		orderDetail.setShipping("0");
		orderDetail.setTax("0");

		orderDetail
				.setCancelUrl(ConfigConstants.URL_SERVICO
						+ "/restricted/financeiro/cancelarPagamentoAtual?idTransacaoFinanceira="
						+ idTransacaoFinanceira);
		orderDetail
				.setReturnUrl(ConfigConstants.URL_SERVICO
						+ "/restricted/financeiro/confirmarPagamentoAtual?idTransacaoFinanceira="
						+ idTransacaoFinanceira);

		return orderDetail;
	}

	@RequestMapping(value = "/restricted/financeiro/viewTransacao/{id}", method = RequestMethod.GET)
	public ModelAndView viewTransacao(@PathVariable Long id) {
		TransacaoFinanceira transacaoFinanceira = transacaoFinanceiraService
				.getById(id);
		if (transacaoFinanceira != null) {
			ModelAndView model = new ModelAndView();
			model.setViewName("/restricted/financeiro/viewTransacao");
			model.addObject("transacaoFinanceira", transacaoFinanceira);
			return model;
		} else {
			return null;
		}
	}

	@RequestMapping(value = "/restricted/financeiro/relatFinanceiro", method = RequestMethod.GET)
	public ModelAndView relatorioFinanceiro() {

		VsPlayersUserDetail user = (VsPlayersUserDetail) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		Usuario usuario = user.getUsuario();

		List<TransacaoFinanceiraDTO> trxUsuario = transacaoFinanceiraService
				.listarTransacoesUsuario(usuario);
		if (trxUsuario != null) {

			ModelAndView model = new ModelAndView();

			BigDecimal saldo = transacaoFinanceiraService
					.verificaSaldoUsuario(usuario.getId());

			try {
				model.addObject("nomeUsuario",usuario.getNmNome());
				model.addObject("saldoAtual",
						"R$ " + StringUtils.doubleToMonetarioString(saldo));
			} catch (ParseException e) {
				logger.error(e.getMessage());
			}

			model.setViewName("/restricted/financeiro/relatFinanceiro");
			model.addObject("listTrx", trxUsuario);
			return model;
		} else {
			return null;
		}
	}

}
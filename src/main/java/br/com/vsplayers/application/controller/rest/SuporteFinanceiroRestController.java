package br.com.vsplayers.application.controller.rest;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.vsplayers.dto.ResultadoSaldoDTO;
import br.com.vsplayers.dto.TransacaoFinanceiraDTO;
import br.com.vsplayers.integration.beans.VsPlayersUserDetail;
import br.com.vsplayers.service.TransacaoFinanceiraService;

@Controller
public class SuporteFinanceiroRestController {

	@Autowired
	@Qualifier("TransacaoFinanceiraServiceImpl")
	private TransacaoFinanceiraService transacaoFinanceiraService;

	@RequestMapping(value = "/restricted/financeiro/rest/getSaldo", method = RequestMethod.GET)
	public @ResponseBody
	ResultadoSaldoDTO autenticaRest() {

		VsPlayersUserDetail user = (VsPlayersUserDetail) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		ResultadoSaldoDTO returnObject = new ResultadoSaldoDTO();

		BigDecimal saldo = transacaoFinanceiraService.verificaSaldoUsuario(user
				.getUsuario().getId());

		DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols(
				new Locale("pt", "BR"));

		decimalFormatSymbols.setDecimalSeparator(',');
		decimalFormatSymbols.setGroupingSeparator('.');
		DecimalFormat decimalFormat = new DecimalFormat("#,##0.00",
				decimalFormatSymbols);

		returnObject.setSaldo("R$: " + decimalFormat.format(saldo));

		return returnObject;
	}

	@RequestMapping(value = "/admin/financeiro/rest/getDadosParaDeposito/{idSolicitacao}", method = RequestMethod.GET)
	public @ResponseBody
	TransacaoFinanceiraDTO getDadosParaDeposito(
			@PathVariable String idSolicitacao) {

		TransacaoFinanceiraDTO trx = transacaoFinanceiraService
				.getDadosDeposito(Long.valueOf(idSolicitacao));

		return trx;
	}

	@RequestMapping(value = "/admin/financeiro/rest/confirmDeposito/{idSolicitacao}", method = RequestMethod.GET)
	public @ResponseBody
	boolean confirmDeposito(@PathVariable String idSolicitacao) {

		transacaoFinanceiraService.confirmDeposito(Long.valueOf(idSolicitacao));

		return true;
	}

}

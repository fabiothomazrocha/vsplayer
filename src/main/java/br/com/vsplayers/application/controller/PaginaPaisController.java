package br.com.vsplayers.application.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.vsplayers.application.controller.forms.CadastrarPaisForm;
import br.com.vsplayers.domain.Pais;
import br.com.vsplayers.dto.pais.PaisDTO;
import br.com.vsplayers.exception.VsPlayersException;
import br.com.vsplayers.service.PaisService;

@Controller
public class PaginaPaisController {
	private static final Logger logger = LoggerFactory
			.getLogger(PaginaPaisController.class);

	@Autowired
	@Qualifier("PaisServiceImpl")
	private PaisService paisService;

	@RequestMapping(value = "/admin/pais/listPais", method = RequestMethod.GET)
	public ModelAndView listarPaises(HttpServletRequest request) {
		List<PaisDTO> listaPais = paisService.listarTodosPaises();
		ModelAndView returnObj = new ModelAndView("/admin/pais/listPais");
		returnObj.addObject("listaPais", listaPais);
		return returnObj;
	}

	@RequestMapping(value = "/admin/pais/createPais", method = RequestMethod.GET)
	public ModelAndView cadastrarPais() {
		ModelAndView model = new ModelAndView();
		CadastrarPaisForm cadastrarPaisForm = new CadastrarPaisForm();
		model.addObject("cadastrarPaisForm", cadastrarPaisForm);
		model.setViewName("/admin/pais/createPais");
		return model;
	}

	@RequestMapping(value = "/admin/pais/cadastrarPais", method = RequestMethod.POST)
	public ModelAndView cadastrar(
			@Valid @ModelAttribute CadastrarPaisForm cadastrarPaisForm,
			BindingResult bindingResult, HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		model.addObject("cadastrarPaisForm", cadastrarPaisForm);
		model.setViewName("/admin/pais/createPais");

		if (bindingResult.hasErrors()) {
			return model;
		} else {
			if (cadastrarPaisForm.getNmPais() == null) {
				bindingResult.addError(new ObjectError("Nome do País",
						"Por favor insira um nome para o País!"));

				return model;
			}
			if (cadastrarPaisForm.getCdPais() == null) {
				bindingResult.addError(new ObjectError("Código do País",
						"Por favor insira um código para o País!"));

				return model;
			}

			else {
				try {
					Pais pais = new Pais();
					pais.setCdPais(cadastrarPaisForm.getCdPais());
					pais.setNmPais(cadastrarPaisForm.getNmPais());
					paisService.inserirPais(pais);
				} catch (VsPlayersException e) {
					logger.error(e.getMessage());

					bindingResult
							.addError(new ObjectError("ERRO",
									"Ocorreu um erro ao tentar gravar um Pais, contate o administrador"));
					return model;

				}
			}
		}
		return this.listarPaises(request);
	}

	@RequestMapping(value = "/admin/pais/editPais", method = RequestMethod.GET)
	public ModelAndView editFaq(
			@RequestParam(value = "idPaisSelecionado") Long idPaisSelecionado,
			HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		Pais paisEdicao = paisService.getById(idPaisSelecionado);
		CadastrarPaisForm formEdicao = new CadastrarPaisForm();
		formEdicao.setId(paisEdicao.getId());
		formEdicao.setNmPais(paisEdicao.getNmPais());
		formEdicao.setCdPais(paisEdicao.getCdPais());

		model.addObject("cadastrarPaisForm", formEdicao);
		model.setViewName("/admin/pais/editPais");
		return model;
	}

	@RequestMapping(value = "/admin/pais/editarPais", method = RequestMethod.POST)
	public ModelAndView editar(
			@Valid @ModelAttribute CadastrarPaisForm cadastrarPaisForm,
			BindingResult bindingResult, HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		model.addObject("cadastrarPaisForm", cadastrarPaisForm);
		model.setViewName("/admin/pais/editPais");

		if (bindingResult.hasErrors()) {
			return model;
		} else {
			if (cadastrarPaisForm.getNmPais() == null) {
				bindingResult.addError(new ObjectError("Nome do País",
						"Por favor insira um nome para o País!"));

				return model;
			}
			if (cadastrarPaisForm.getCdPais() == null) {
				bindingResult.addError(new ObjectError("Código do País",
						"Por favor insira um código para o País!"));

				return model;
			}

			else {
				try {
					Pais pais = paisService.getById(cadastrarPaisForm.getId());
					pais.setNmPais(cadastrarPaisForm.getNmPais());
					System.out.print("codigo------------------------------>"+ pais.getCdPais());
					pais.setCdPais(cadastrarPaisForm.getCdPais());
					paisService.atualizarPais(pais);
				} catch (VsPlayersException e) {
					logger.error(e.getMessage());
					bindingResult
							.addError(new ObjectError("ERRO",
									"Ocorreu um erro ao tentar gravar um Pais, contate o administrador"));
					return model;

				}
			}
		}
		return this.listarPaises(request);
	}

	@RequestMapping(value = "/admin/pais/deletePais", method = RequestMethod.GET)
	public ModelAndView deletePais(
			@RequestParam(value = "idPaisSelecionado") Long idPaisSelecionado,
			HttpServletRequest request) {
		paisService.deletePais(idPaisSelecionado);
		return this.listarPaises(request);
	}

	@RequestMapping(value = "/admin/pais/viewPais/{id}", method = RequestMethod.GET)
	public ModelAndView viewFaq(@PathVariable Long id) {
		Pais pais = paisService.getById(id);
		if (pais != null) {
			ModelAndView model = new ModelAndView();
			model.setViewName("/admin/pais/viewPais");
			model.addObject("pais", pais);
			return model;
		} else {
			return null;
		}
	}
}

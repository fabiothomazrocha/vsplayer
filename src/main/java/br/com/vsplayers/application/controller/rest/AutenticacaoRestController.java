package br.com.vsplayers.application.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.vsplayers.domain.Usuario;
import br.com.vsplayers.dto.rest.AutenticacaoRestDTO;
import br.com.vsplayers.service.RestTokenService;
import br.com.vsplayers.service.RestUsersManagerService;
import br.com.vsplayers.service.UsuarioService;

@Controller
public class AutenticacaoRestController {

	@Autowired
	@Qualifier("UsuarioServiceImpl")
	private UsuarioService usuarioService;
	
	@Autowired
	@Qualifier("RestTokenServiceImpl")
	private RestTokenService restTokenService;
	
	@Autowired
	@Qualifier("RestUsersManagerServiceImpl")
	private RestUsersManagerService restUsersManagerService;

	@RequestMapping(value = "/restAuth/user/{usuario}/password/{senha}", method = RequestMethod.GET)
	public @ResponseBody
	AutenticacaoRestDTO autenticaRest(@PathVariable String usuario,
			@PathVariable String senha) {

		AutenticacaoRestDTO returnObject = new AutenticacaoRestDTO();

		if (StringUtils.isEmpty(usuario) || StringUtils.isEmpty(senha)
				|| senha.length() != 32) {
			return new AutenticacaoRestDTO();
		}

		Usuario usuarioLogin = usuarioService.loginREST(usuario, senha);

		if (usuarioLogin != null) {
			// gerar hash de acesso
			String token = restTokenService.getTokenForUser(usuarioLogin.getDsEmail());
			Boolean b = restUsersManagerService.insertUserAuth(token);
			
			returnObject.setTokenSeguranca(token);
		}
		
		return returnObject;
	}

}

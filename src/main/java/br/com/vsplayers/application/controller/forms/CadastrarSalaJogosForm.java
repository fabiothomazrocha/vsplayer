package br.com.vsplayers.application.controller.forms;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CadastrarSalaJogosForm {

	private Long id;
	@NotNull(message = "O Valor da sala de jogos deve ser informado")
	@Size(min = 3, max = 10, message = "Valor informado para o valor da sala de jogos é inválido")
	private String valorSala;

	public String getValorSala() {
		return valorSala;
	}

	public void setValorSala(String valorSala) {
		this.valorSala = valorSala;
	}

	@NotNull(message = "O nome da sala de jogos deve ser informado")
	@Size(min = 3, max = 10, message = "Valor informado para o nome da sala de jogos é inválido")
	private String nmSalaJogos;

	private Date dtCriacao;

	private Date dtAtualizacao;

	private Integer posicaoSala;

	private long idJogo;

	// private List<DesafioDTO> listaDesafios;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmSalaJogos() {
		return nmSalaJogos;
	}

	public void setNmSalaJogos(String nmSalaJogos) {
		this.nmSalaJogos = nmSalaJogos;
	}

	public Date getDtCriacao() {
		return dtCriacao;
	}

	public void setDtCriacao(Date dtCriacao) {
		this.dtCriacao = dtCriacao;
	}

	public Date getDtAtualizacao() {
		return dtAtualizacao;
	}

	public void setDtAtualizacao(Date dtAtualizacao) {
		this.dtAtualizacao = dtAtualizacao;
	}

	public Integer getPosicaoSala() {
		return posicaoSala;
	}

	public void setPosicaoSala(Integer posicaoSala) {
		this.posicaoSala = posicaoSala;
	}

	public long getIdJogo() {
		return idJogo;
	}

	public void setIdJogo(long idJogo) {
		this.idJogo = idJogo;
	}
}

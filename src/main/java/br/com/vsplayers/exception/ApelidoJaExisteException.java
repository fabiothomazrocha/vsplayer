package br.com.vsplayers.exception;

public class ApelidoJaExisteException extends VsPlayersException {
	
	public ApelidoJaExisteException(Throwable parent) {
		super(parent);
	}

	private static final long serialVersionUID = 1L;

}

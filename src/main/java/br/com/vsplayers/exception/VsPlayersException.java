package br.com.vsplayers.exception;

public class VsPlayersException extends Throwable {

	private Throwable parent;

	public VsPlayersException(Throwable parent) {
		this.parent = parent;
	}

	public Throwable getParent() {
		return parent;
	}

	public void setParent(Throwable parent) {
		this.parent = parent;
	}

}

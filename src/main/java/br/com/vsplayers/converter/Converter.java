package br.com.vsplayers.converter;

public interface Converter<E, D> {
	
	public E getEntityFromDTO(D dto);
	public D getDTOFromEntity(E entity);

}

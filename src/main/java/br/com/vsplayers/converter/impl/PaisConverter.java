package br.com.vsplayers.converter.impl;

import javax.inject.Singleton;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import br.com.vsplayers.converter.Converter;
import br.com.vsplayers.domain.Pais;
import br.com.vsplayers.dto.pais.PaisDTO;
@Singleton
@Component
@Qualifier("PaisConverter")
public class PaisConverter implements Converter<Pais, PaisDTO>{

	@Override
	public Pais getEntityFromDTO(PaisDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaisDTO getDTOFromEntity(Pais entity) {
		PaisDTO returnObject = new PaisDTO();
		returnObject.setId(entity.getId());
		returnObject.setNmPais(entity.getNmPais());
		returnObject.setCdPais(entity.getCdPais());
		return returnObject;
	}

}

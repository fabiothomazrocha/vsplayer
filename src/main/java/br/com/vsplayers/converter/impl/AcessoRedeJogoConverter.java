package br.com.vsplayers.converter.impl;

import java.text.SimpleDateFormat;

import javax.inject.Singleton;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import br.com.vsplayers.converter.Converter;
import br.com.vsplayers.domain.AcessoRedeJogo;
import br.com.vsplayers.dto.acessoredejogo.AcessoRedeJogoDTO;

@Singleton
@Component
@Qualifier("AcessoRedeJogoConverter")
public class AcessoRedeJogoConverter implements
		Converter<AcessoRedeJogo, AcessoRedeJogoDTO> {

	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

	@Override
	public AcessoRedeJogo getEntityFromDTO(AcessoRedeJogoDTO dto) {
		return null;
	}

	@Override
	public AcessoRedeJogoDTO getDTOFromEntity(AcessoRedeJogo entity) {

		AcessoRedeJogoDTO returnObject = new AcessoRedeJogoDTO();
		returnObject.setIdAcessoRedeJogo(entity.getId());

		if (entity.getDataCriacao() != null) {
			returnObject.setDataCriacao(dateFormat.format(entity
					.getDataCriacao()));
		}

		returnObject.setUserId(entity.getUserId());
		returnObject.setRedeAcesso(entity.getTipoAcessoRedeJogo()
				.getTipoAcesso());

		return returnObject;
	}

}

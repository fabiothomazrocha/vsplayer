package br.com.vsplayers.converter.impl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import br.com.vsplayers.converter.Converter;
import br.com.vsplayers.domain.SalaJogos;
import br.com.vsplayers.dto.salajogos.SalaJogosDTO;

@Component
@Qualifier("SalaJogosConverter")
public class SalaJogosConverter implements Converter<SalaJogos, SalaJogosDTO> {

	@Override
	public SalaJogos getEntityFromDTO(SalaJogosDTO dto) {

		return null;
	}

	@Override
	public SalaJogosDTO getDTOFromEntity(SalaJogos entity) {
		SalaJogosDTO returnObject = new SalaJogosDTO();
		returnObject.setId(entity.getId());

		returnObject.setDtAtualizacao(entity.getDtAtualizacao());
		returnObject.setDtCriacao(entity.getDtCriacao());
		returnObject.setNmSalaJogos(entity.getNmSalaJogos());
		returnObject.setPosicaoSala(entity.getPosicaoSala());
		returnObject.setValorSala(entity.getValorSala());
		returnObject.setNomeConsole(entity.getJogo().getConsole()
				.getNmConsole());
		returnObject.setTipoConsole(entity.getJogo().getConsole()
				.getTipoConsole());
		if (entity.getJogo() != null) {
			returnObject.setNomeJogo(entity.getJogo().getNmJogo());
		}

		return returnObject;
	}
}

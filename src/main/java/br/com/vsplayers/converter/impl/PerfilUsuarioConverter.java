package br.com.vsplayers.converter.impl;

import java.text.SimpleDateFormat;

import javax.inject.Singleton;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import br.com.vsplayers.converter.Converter;
import br.com.vsplayers.domain.PerfilUsuario;
import br.com.vsplayers.dto.perfil.PerfilUsuarioDTO;

@Singleton
@Service
@Qualifier("PerfilUsuarioConverter")
public class PerfilUsuarioConverter implements
		Converter<PerfilUsuario, PerfilUsuarioDTO> {
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

	@Override
	public PerfilUsuario getEntityFromDTO(PerfilUsuarioDTO dto) {
		return null;
	}

	@Override
	public PerfilUsuarioDTO getDTOFromEntity(PerfilUsuario entity) {
		
		PerfilUsuarioDTO resultObject = new PerfilUsuarioDTO();
		
		resultObject.setIdPerfilUsuario(entity.getId());
		
		if(entity.getDtCriacao() != null) {
			resultObject.setDtCriacaoFormatada(dateFormat.format(entity.getDtCriacao()));
		}
		
		if(entity.getDtAtualizacao() != null) {
			resultObject.setDtAtualizacaoFormatada(dateFormat.format(entity.getDtAtualizacao()));
		}
		
		if(entity.getImagemPerfil() != null) {
			resultObject.setPathImagemPerfil(entity.getImagemPerfil().getNmArquivo());
		}
		
		resultObject.setSobre(entity.getDsSobre());
		resultObject.setApelido(entity.getApelido());
		
		return resultObject;
	}

}
package br.com.vsplayers.converter.impl;

import javax.inject.Singleton;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import br.com.vsplayers.converter.Converter;
import br.com.vsplayers.domain.Faq;
import br.com.vsplayers.dto.faq.FaqDTO;

@Singleton
@Component
@Qualifier("FaqConverter")
public class FaqConverter implements Converter<Faq, FaqDTO> {

	@Override
	public Faq getEntityFromDTO(FaqDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FaqDTO getDTOFromEntity(Faq entity) {

		FaqDTO returnObject = new FaqDTO();
		returnObject.setId(entity.getId());
		returnObject.setDtAtualizacao(entity.getDtAtualizacao());
		returnObject.setNmFaq(entity.getNmFaq());
		return returnObject;
	}

}

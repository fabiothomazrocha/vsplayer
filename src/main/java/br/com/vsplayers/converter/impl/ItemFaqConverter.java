package br.com.vsplayers.converter.impl;

import javax.inject.Singleton;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import br.com.vsplayers.converter.Converter;
import br.com.vsplayers.domain.ItemFaq;
import br.com.vsplayers.dto.itemfaq.ItemFaqDTO;

@Singleton
@Component
@Qualifier("ItemFaqConverter")
public class ItemFaqConverter implements Converter<ItemFaq, ItemFaqDTO> {

	@Override
	public ItemFaq getEntityFromDTO(ItemFaqDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ItemFaqDTO getDTOFromEntity(ItemFaq entity) {
		ItemFaqDTO returnObject = new ItemFaqDTO();

		returnObject.setDsTituloItem(entity.getDsTituloItem());
		returnObject.setId(entity.getId());
		returnObject.setNomeFaq(entity.getFaq().getNmFaq());
		returnObject.setTxItem(entity.getTxItem());
		

		return returnObject;
	}

}

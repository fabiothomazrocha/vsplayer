package br.com.vsplayers.converter.impl;

import javax.inject.Singleton;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import br.com.vsplayers.converter.Converter;
import br.com.vsplayers.domain.Console;
import br.com.vsplayers.dto.console.ConsoleDTO;

@Singleton
@Component
@Qualifier("ConsoleConverter")
public class ConsoleConverter implements Converter<Console, ConsoleDTO> {

	@Override
	public Console getEntityFromDTO(ConsoleDTO dto) {
		return null;
	}

	@Override
	public ConsoleDTO getDTOFromEntity(Console entity) {

		ConsoleDTO returnObject = new ConsoleDTO();

		returnObject.setNomeConsole(entity.getNmConsole());
		returnObject.setIdConsole(entity.getId());
		returnObject.setTipoConsole(entity.getTipoConsole());

		return returnObject;
	}

}
package br.com.vsplayers.converter.impl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import br.com.vsplayers.converter.Converter;
import br.com.vsplayers.domain.Jogo;
import br.com.vsplayers.dto.jogo.JogoDTO;

@Component
@Qualifier("JogoConverter")
public class JogoConverter implements Converter<Jogo, JogoDTO> {

	@Override
	public Jogo getEntityFromDTO(JogoDTO dto) {
		return null;
	}

	@Override
	public JogoDTO getDTOFromEntity(Jogo entity) {

		JogoDTO returnObject = new JogoDTO();
		returnObject.setId(entity.getId());
		returnObject.setNomeJogo(entity.getNmJogo());
		returnObject.setTipoJogo(entity.getTipoJogo());

		if (entity.getConsole() != null) {
			returnObject.setNomeConsole(entity.getConsole().getNmConsole());
		}

		return returnObject;
	}

}

package br.com.vsplayers.converter.impl;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Locale;

import br.com.vsplayers.converter.Converter;
import br.com.vsplayers.domain.DetalheSolicitacaoSaque;
import br.com.vsplayers.domain.TransacaoFinanceira;
import br.com.vsplayers.dto.TransacaoFinanceiraDTO;

public class TransacaoFinanceiraComDetalheConverter implements
		Converter<TransacaoFinanceira, TransacaoFinanceiraDTO> {

	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	private static final Locale BRAZIL = new Locale("pt", "BR");
	private static final DecimalFormatSymbols REAL = new DecimalFormatSymbols(
			BRAZIL);
	public static final DecimalFormat DINHEIRO_REAL = new DecimalFormat(
			"¤ ###,###,##0.00", REAL);

	@Override
	public TransacaoFinanceira getEntityFromDTO(TransacaoFinanceiraDTO dto) {
		return null;
	}

	@Override
	public TransacaoFinanceiraDTO getDTOFromEntity(TransacaoFinanceira entity) {

		TransacaoFinanceiraDTO returnObject = new TransacaoFinanceiraDTO();

		returnObject.setIdTransacao(entity.getId());
		returnObject.setAprovada(entity.getAprovada());
		returnObject.setNmTransacao(entity.getNmTransacao());

		if (entity.getDtTransacao() != null) {
			returnObject.setDataTransacao(dateFormat.format(entity
					.getDtTransacao()));
		}

		if (entity.getDtAprovacao() != null) {
			returnObject.setDataAprovacao(dateFormat.format(entity
					.getDtAprovacao()));
			returnObject.setNmTransacao(entity.getNmTransacao());
		}

		returnObject.setValorFormatado(DINHEIRO_REAL.format(entity
				.getValorTransacao()));

		if (entity.getDetalheSolic() != null
				&& !entity.getDetalheSolic().isEmpty()) {
			
			DetalheSolicitacaoSaque det = entity.getDetalheSolic().get(0);
			
			returnObject.setAgenciaBancoSolic(det.getAgencia());
			returnObject.setNumeroContaBancoSolic(det.getConta());
			returnObject.setCpfTitularSolic(det.getCpf());
			returnObject.setNomeBancoSolic(det.getTipoBanco().getDescricao());
			
		}

		return returnObject;
	}

}

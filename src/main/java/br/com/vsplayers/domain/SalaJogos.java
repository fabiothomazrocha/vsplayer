package br.com.vsplayers.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "T_SALA_JOGOS")
public class SalaJogos {
	
	@Id
	@Column(name = "ID_SALA_JOGOS")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="NR_VLR_SALA", nullable = false)
	private BigDecimal valorSala;
	
	@Column(name="NM_SALA_JOGOS", length=255, nullable=false)
	private String nmSalaJogos;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DT_CRIACAO", nullable = false)
	private Date dtCriacao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DT_ATUALIZACAO", nullable = true)
	private Date dtAtualizacao;
	
	@Column(name="NR_POSICAO_SALA", nullable = false)
	private Integer posicaoSala;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_JOGO")
	private Jogo jogo;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="salaJogos")
	private List<Desafio> listaDesafios;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getValorSala() {
		return valorSala;
	}

	public void setValorSala(BigDecimal valorSala) {
		this.valorSala = valorSala;
	}

	public String getNmSalaJogos() {
		return nmSalaJogos;
	}

	public void setNmSalaJogos(String nmSalaJogos) {
		this.nmSalaJogos = nmSalaJogos;
	}

	public Date getDtCriacao() {
		return dtCriacao;
	}

	public void setDtCriacao(Date dtCriacao) {
		this.dtCriacao = dtCriacao;
	}

	public Date getDtAtualizacao() {
		return dtAtualizacao;
	}

	public void setDtAtualizacao(Date dtAtualizacao) {
		this.dtAtualizacao = dtAtualizacao;
	}

	public Integer getPosicaoSala() {
		return posicaoSala;
	}

	public void setPosicaoSala(Integer posicaoSala) {
		this.posicaoSala = posicaoSala;
	}

	public Jogo getJogo() {
		return jogo;
	}

	public void setJogo(Jogo jogo) {
		this.jogo = jogo;
	}

	public List<Desafio> getListaDesafios() {
		return listaDesafios;
	}

	public void setListaDesafios(List<Desafio> listaDesafios) {
		this.listaDesafios = listaDesafios;
	}
	
}
package br.com.vsplayers.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_REGRA")
public class Regra {

	@Id
	@Column(name = "ID_REGRA")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "NM_REGRA", length = 255, nullable = false)
	private String nmInstancia;

	@Lob
	@Column(name = "TX_REGRA", length = 255, nullable = false)
	private String txItem;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_VSPLAYERS")
	private VsPlayers vsPlayers;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmInstancia() {
		return nmInstancia;
	}

	public void setNmInstancia(String nmInstancia) {
		this.nmInstancia = nmInstancia;
	}

	public String getTxItem() {
		return txItem;
	}

	public void setTxItem(String txItem) {
		this.txItem = txItem;
	}

	public VsPlayers getVsPlayers() {
		return vsPlayers;
	}

	public void setVsPlayers(VsPlayers vsPlayers) {
		this.vsPlayers = vsPlayers;
	}

}
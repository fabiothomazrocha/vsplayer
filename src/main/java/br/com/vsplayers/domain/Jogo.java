package br.com.vsplayers.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.vsplayers.domain.enums.TipoJogo;


@Entity
@Table(name = "T_JOGO")
public class Jogo {
	
	@Id
    @Column(name="ID_JOGO")
    @GeneratedValue
	private Long id;
	
	@Column(name="NM_JOGO", length = 255, nullable = false)
	private String nmJogo;
	
	@Temporal(TemporalType.DATE)
	@Column(name="DT_CADASTRO", nullable = false)
	private Date dtCadastro;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tpJogo")
	private TipoJogo tipoJogo;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CONSOLE")
	private Console console;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmJogo() {
		return nmJogo;
	}

	public void setNmJogo(String nmJogo) {
		this.nmJogo = nmJogo;
	}

	public Date getDtCadastro() {
		return dtCadastro;
	}

	public void setDtCadastro(Date dtCadastro) {
		this.dtCadastro = dtCadastro;
	}

	public TipoJogo getTipoJogo() {
		return tipoJogo;
	}

	public void setTipoJogo(TipoJogo tipoJogo) {
		this.tipoJogo = tipoJogo;
	}

	public Console getConsole() {
		return console;
	}

	public void setConsole(Console console) {
		this.console = console;
	}

}

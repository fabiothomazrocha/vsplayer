package br.com.vsplayers.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "T_VSPLAYERS")
public class VsPlayers {
	
	@Id
	@Column(name = "ID_VSPLAYERS")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="NM_INSTANCIA", length=255, nullable=false)
	private String nmInstancia;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="vsPlayers")
	private List<Faq> faq;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmInstancia() {
		return nmInstancia;
	}

	public void setNmInstancia(String nmInstancia) {
		this.nmInstancia = nmInstancia;
	}

	public List<Faq> getFaq() {
		return faq;
	}

	public void setFaq(List<Faq> faq) {
		this.faq = faq;
	}
	
}
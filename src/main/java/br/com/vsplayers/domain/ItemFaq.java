package br.com.vsplayers.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_ITEM_FAQ")
public class ItemFaq {

	@Id
	@Column(name = "ID_ITEM_FAQ")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_FAQ", nullable = false)
	private Faq faq;

	@Column(name = "DS_TITULO_ITEM", length = 255, nullable = false)
	private String dsTituloItem;

	@Lob
	@Column(name = "TX_ITEM", nullable = false)
	private String txItem;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Faq getFaq() {
		return faq;
	}

	public void setFaq(Faq faq) {
		this.faq = faq;
	}

	public String getDsTituloItem() {
		return dsTituloItem;
	}

	public void setDsTituloItem(String dsTituloItem) {
		this.dsTituloItem = dsTituloItem;
	}

	public String getTxItem() {
		return txItem;
	}

	public void setTxItem(String txItem) {
		this.txItem = txItem;
	}

}

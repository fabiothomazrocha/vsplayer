package br.com.vsplayers.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "T_FAQ")
public class Faq {
	
	@Id
	@Column(name = "ID_FAQ")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="NM_FAQ", length=255, nullable=false)
	private String nmFaq;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DT_ATUALIZACAO")
	private Date dtAtualizacao;
	
	@OneToMany(mappedBy="faq",fetch=FetchType.LAZY )
	private List<ItemFaq> listaItemFAQ;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_VSPLAYERS", nullable = false)
	private VsPlayers vsPlayers;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmFaq() {
		return nmFaq;
	}

	public void setNmFaq(String nmFaq) {
		this.nmFaq = nmFaq;
	}

	public Date getDtAtualizacao() {
		return dtAtualizacao;
	}

	public void setDtAtualizacao(Date dtAtualizacao) {
		this.dtAtualizacao = dtAtualizacao;
	}

	public List<ItemFaq> getListaItemFAQ() {
		return listaItemFAQ;
	}

	public void setListaItemFAQ(List<ItemFaq> listaItemFAQ) {
		this.listaItemFAQ = listaItemFAQ;
	}

	public VsPlayers getVsPlayers() {
		return vsPlayers;
	}

	public void setVsPlayers(VsPlayers vsPlayers) {
		this.vsPlayers = vsPlayers;
	}
	
}

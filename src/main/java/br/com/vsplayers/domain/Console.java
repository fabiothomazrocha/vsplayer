package br.com.vsplayers.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import br.com.vsplayers.domain.enums.Flag;
import br.com.vsplayers.domain.enums.TipoAcessoRedeJogo;
import br.com.vsplayers.domain.enums.TipoConsole;

@Entity
@Table(name = "T_CONSOLE")
public class Console {

	@Id
	@Column(name = "ID_CONSOLE")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "NM_CONSOLE", length = 255, nullable = false)
	private String nmConsole;

	@Enumerated(EnumType.STRING)
	@Column(name = "TP_ACS_REDE_JOGO", length = 4, nullable = false)
	private TipoAcessoRedeJogo tipoRede;

	@Enumerated(EnumType.STRING)
	@Column(name = "TP_CONSOLE", length = 3, nullable = false)
	private TipoConsole tipoConsole;
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "console", cascade=CascadeType.REMOVE, orphanRemoval=true)
	private List<Jogo> listaJogos;
	@Enumerated(EnumType.STRING)
	@Column(name="FL_ATIVO", length = 1, nullable = false)
	private Flag ativo;
	
	
	public Flag getAtivo() {
		return ativo;
	}

	public void setAtivo(Flag ativo) {
		this.ativo = ativo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmConsole() {
		return nmConsole;
	}

	public void setNmConsole(String nmConsole) {
		this.nmConsole = nmConsole;
	}

	public TipoAcessoRedeJogo getTipoRede() {
		return tipoRede;
	}

	public void setTipoRede(TipoAcessoRedeJogo tipoRede) {
		this.tipoRede = tipoRede;
	}

	public TipoConsole getTipoConsole() {
		return tipoConsole;
	}

	public void setTipoConsole(TipoConsole tipoConsole) {
		this.tipoConsole = tipoConsole;
	}

	public List<Jogo> getListaJogos() {
		return listaJogos;
	}

	public void setListaJogos(List<Jogo> listaJogos) {
		this.listaJogos = listaJogos;
	}

}
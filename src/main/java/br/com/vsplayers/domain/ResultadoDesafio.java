package br.com.vsplayers.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_RESULTADO_DESAFIO")
public class ResultadoDesafio {
	
	@Id
	@Column(name = "ID_RESULTADO_DESAFIO")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="NR_PLACAR_P1", nullable = false)
	private Integer nrPlacarP1;
	
	@Column(name="NR_PLACAR_P2", nullable = false)
	private Integer nrPlacarP2;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_DESAFIO", nullable = false)
	private Desafio desafio;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SOLIC_DVG_ADM", nullable = true)
	private SolicitacaoDivergenciaAdministrador solicDivergAdministrador;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNrPlacarP1() {
		return nrPlacarP1;
	}

	public void setNrPlacarP1(Integer nrPlacarP1) {
		this.nrPlacarP1 = nrPlacarP1;
	}

	public Integer getNrPlacarP2() {
		return nrPlacarP2;
	}

	public void setNrPlacarP2(Integer nrPlacarP2) {
		this.nrPlacarP2 = nrPlacarP2;
	}

	public Desafio getDesafio() {
		return desafio;
	}

	public void setDesafio(Desafio desafio) {
		this.desafio = desafio;
	}

	public SolicitacaoDivergenciaAdministrador getSolicDivergAdministrador() {
		return solicDivergAdministrador;
	}

	public void setSolicDivergAdministrador(
			SolicitacaoDivergenciaAdministrador solicDivergAdministrador) {
		this.solicDivergAdministrador = solicDivergAdministrador;
	}
	
}
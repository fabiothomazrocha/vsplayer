package br.com.vsplayers.domain.enums;

public enum TipoImagem {
	
	PER("PERFIL");
	
	private TipoImagem(String perfil) {
		this.perfil = perfil;
	}
	
	private String perfil;
	
	public String getPerfil() {
		return this.perfil;
	}

}

package br.com.vsplayers.domain.enums;

public enum Flag {

	S("Sim"), N("Não");

	private Flag(String flag) {
		this.descricao = flag;
	}

	private String descricao;

	public String getDescricao() {
		return this.descricao;
	}

}

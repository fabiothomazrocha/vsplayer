package br.com.vsplayers.domain.enums;

public enum TipoJogo {

	ESP("Esporte");

	private TipoJogo(String tipoJogo) {
		this.tipoJogo = tipoJogo;
	}

	String tipoJogo;

	public String getTipoJogo() {
		return this.tipoJogo;
	}
	
}
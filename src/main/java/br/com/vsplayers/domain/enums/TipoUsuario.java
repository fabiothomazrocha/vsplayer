package br.com.vsplayers.domain.enums;

public enum TipoUsuario {

	VSPLA("USUARIO VSPLAYERS"), FACEB("USUARIO FACEBOOK");

	private TipoUsuario(String descricao) {
		this.descricao = descricao;
	}

	private String descricao;

	public String getDescricao() {
		return this.descricao;
	}

}

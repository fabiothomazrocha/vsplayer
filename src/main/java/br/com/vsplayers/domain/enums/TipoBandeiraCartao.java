package br.com.vsplayers.domain.enums;

public enum TipoBandeiraCartao {

	VISA("Visa"), MASTERCARD("Mastercard"), AMEX("American Express"), DINERS(
			"Diners"), HIPERCARD("Hipercard");

	private String descricao;

	private TipoBandeiraCartao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return this.descricao;
	}

}
package br.com.vsplayers.domain.enums;

public enum TipoTransacao {

	DEPOS("Depósito"), SAQUE("Saque"), SLSA("Solicitação de Saque");

	private String descricao;

	private TipoTransacao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
package br.com.vsplayers.domain.enums;

public enum TipoAcessoRedeJogo {
	
	PSN("PLAYSTATION NETWORK"), LIVE("MICROSOFT LIVE NETWORK");
	
	private TipoAcessoRedeJogo(String tipoAcesso) {
		this.tipoAcesso = tipoAcesso;
	}
	
	private String tipoAcesso;
	
	public String getTipoAcesso() {
		return tipoAcesso;
	}

}

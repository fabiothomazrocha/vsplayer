package br.com.vsplayers.domain.enums;

import br.com.vsplayers.exception.VsPlayersException;

public enum TipoBanco {

	BB("Banco do Brasil"), ITAU("Itau"), BRADE("Bradesco"), HSBC("HSBC"), CAIXA(
			"Caixa"), SANTA("Santander");

	private String tipoBanco;

	private TipoBanco(String desc) {
		this.tipoBanco = desc;
	}

	public String getDescricao() {
		return tipoBanco;
	}

	public static TipoBanco getByString(String nomeBanco) throws VsPlayersException {
		for(TipoBanco tpBanco: TipoBanco.values()) {
			if(tpBanco.getDescricao().equals(nomeBanco)) {
				return tpBanco;
			}
		}
		throw new VsPlayersException(null);
	}

}

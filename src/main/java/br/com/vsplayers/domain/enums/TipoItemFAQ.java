package br.com.vsplayers.domain.enums;

public enum TipoItemFAQ {
	
	PER("Pergunta"), RES("Resposta");
	
	private TipoItemFAQ(String tipo) {
		this.tipo = tipo;
	}
	
	private String tipo;
	
	public String getTipo() {
		return this.tipo;
	}

}

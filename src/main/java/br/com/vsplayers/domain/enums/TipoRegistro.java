package br.com.vsplayers.domain.enums;

public enum TipoRegistro {
	
	CRE("Cr�dito");
	
	private TipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}
	
	private String tipoRegistro;
	
	public String getTipoRegistro() {
		return this.tipoRegistro;
	}

}

package br.com.vsplayers.domain.enums;

public enum TipoConsole {

	PS3("Playstation 3"), PS4("Playstation 4"), XBX("XBOX 360"), ONE("XBOX ONE");

	private TipoConsole(String nome) {
		this.nmConsole = nome;
	}

	private String nmConsole;

	public String getNmConsole() {
		return nmConsole;
	}

}

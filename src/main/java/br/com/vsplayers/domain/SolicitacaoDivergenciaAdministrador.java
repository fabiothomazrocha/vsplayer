package br.com.vsplayers.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.vsplayers.domain.enums.Flag;

@Entity
@Table(name = "T_SOLIC_DVG_ADM")
public class SolicitacaoDivergenciaAdministrador {
	
	@Id
	@Column(name = "ID_SOLIC_DVG_ADM")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DT_CRIACAO", nullable = false)
	private Date dtCriacao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DT_ATUALIZACAO", nullable = true)
	private Date dtAtualizacao;
	
	@Enumerated(EnumType.STRING)
	@Column(name="FL_TRATADA")
	private Flag tratada;
	
	@Lob
	@Column(name = "TX_EPLICATIVO", length = 255, nullable = false)
	private String txExplicativo;
	
	@OneToOne(fetch=FetchType.LAZY, mappedBy="solicDivergAdministrador")
	@JoinColumn(name="ID_SOLIC_DVG_ADM", nullable = false)
	private ResultadoDesafio resultadoDesafio;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDtCriacao() {
		return dtCriacao;
	}

	public void setDtCriacao(Date dtCriacao) {
		this.dtCriacao = dtCriacao;
	}

	public Date getDtAtualizacao() {
		return dtAtualizacao;
	}

	public void setDtAtualizacao(Date dtAtualizacao) {
		this.dtAtualizacao = dtAtualizacao;
	}

	public Flag getTratada() {
		return tratada;
	}

	public void setTratada(Flag tratada) {
		this.tratada = tratada;
	}

	public String getTxExplicativo() {
		return txExplicativo;
	}

	public void setTxExplicativo(String txExplicativo) {
		this.txExplicativo = txExplicativo;
	}

	public ResultadoDesafio getResultadoDesafio() {
		return resultadoDesafio;
	}

	public void setResultadoDesafio(ResultadoDesafio resultadoDesafio) {
		this.resultadoDesafio = resultadoDesafio;
	}
	
}
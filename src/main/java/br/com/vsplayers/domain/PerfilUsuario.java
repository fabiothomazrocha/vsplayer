package br.com.vsplayers.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.vsplayers.domain.enums.Flag;

@Entity
@Table(name = "T_PERFIL_USUARIO")
public class PerfilUsuario {
	
	@Id
	@Column(name = "ID_PERFIL_USUARIO")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="NM_APELIDO", length=50, nullable=false)
	private String apelido;
	
	@Column(name="DS_SOBRE", length=255, nullable=false)
	private String dsSobre;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DT_CRIACAO", nullable = false)
	private Date dtCriacao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DT_ATUALIZACAO", nullable = true)
	private Date dtAtualizacao;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_USUARIO")
	private Usuario usuario;
	
	@OneToOne(fetch=FetchType.LAZY, cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	@JoinColumn(name="ID_IMAGEM", nullable = true)
	private Imagem imagemPerfil;
	
	@Enumerated(EnumType.STRING)
	@Column(name="FL_ATIVO", length = 1, nullable = false)
	private Flag ativo;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getApelido() {
		return apelido;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}

	public String getDsSobre() {
		return dsSobre;
	}

	public void setDsSobre(String dsSobre) {
		this.dsSobre = dsSobre;
	}

	public Date getDtCriacao() {
		return dtCriacao;
	}

	public void setDtCriacao(Date dtCriacao) {
		this.dtCriacao = dtCriacao;
	}

	public Date getDtAtualizacao() {
		return dtAtualizacao;
	}

	public void setDtAtualizacao(Date dtAtualizacao) {
		this.dtAtualizacao = dtAtualizacao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Imagem getImagemPerfil() {
		return imagemPerfil;
	}

	public void setImagemPerfil(Imagem imagemPerfil) {
		this.imagemPerfil = imagemPerfil;
	}

	public Flag getAtivo() {
		return ativo;
	}

	public void setAtivo(Flag ativo) {
		this.ativo = ativo;
	}
	
}
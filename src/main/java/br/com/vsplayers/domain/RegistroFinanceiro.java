package br.com.vsplayers.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.vsplayers.domain.enums.TipoRegistro;

@Entity
@Table(name="T_REGISTRO_FINANCEIRO")
public class RegistroFinanceiro {
	
	@Id
    @Column(name="ID_REGISTRO_FINANCEIRO")
    @GeneratedValue
	private Long id;
	
	@Column(name="NR_VALOR", nullable = false)
	private BigDecimal nrValor;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_USUARIO")
	private Usuario usuario;
	
	@Enumerated
	@Column(name="TP_REGISTRO", length = 3, nullable = false)
	private TipoRegistro tipoRegistro;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_DESAFIO", nullable = false)
	private Desafio desafio;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getNrValor() {
		return nrValor;
	}

	public void setNrValor(BigDecimal nrValor) {
		this.nrValor = nrValor;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public TipoRegistro getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(TipoRegistro tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public Desafio getDesafio() {
		return desafio;
	}

	public void setDesafio(Desafio desafio) {
		this.desafio = desafio;
	}
	
}

package br.com.vsplayers.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.vsplayers.domain.enums.Flag;
import br.com.vsplayers.domain.enums.TipoTransacao;

@Entity
@Table(name="T_TRX_FINANCEIRA")
public class TransacaoFinanceira {
	
	@Id
    @Column(name="ID_TRX_FINANCEIRA")
    @GeneratedValue
	private Long id;
	
	@Column(name="NR_VLR_TRX", nullable = false)
	private BigDecimal valorTransacao;
	
	@Enumerated(EnumType.STRING)
	@Column(name="FL_APROVADA")
	private Flag aprovada;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DT_TRANSACAO", nullable = false)
	private Date dtTransacao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DT_APROVACAO", nullable = true)
	private Date dtAprovacao;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_USUARIO")
	private Usuario usuario;
	
	@Column(name="NM_TRX", length=255, nullable=false)
	private String nmTransacao;
	
	@OneToMany(cascade={CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name="ID_DETALHE_SOLIC", nullable = true)
	private List<DetalheSolicitacaoSaque> detalheSolic;
	
	@Enumerated(EnumType.STRING)
	@Column(name="TP_TRANSACAO", length = 5 , nullable = false)
	private TipoTransacao tipoTransacao;
	
	@Column(name="CD_TOKEN_APROVACAO", length=255, nullable=true)
	private String tokenAprovacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getValorTransacao() {
		return valorTransacao;
	}

	public void setValorTransacao(BigDecimal valorTransacao) {
		this.valorTransacao = valorTransacao;
	}

	public Flag getAprovada() {
		return aprovada;
	}

	public void setAprovada(Flag aprovada) {
		this.aprovada = aprovada;
	}

	public Date getDtTransacao() {
		return dtTransacao;
	}

	public void setDtTransacao(Date dtTransacao) {
		this.dtTransacao = dtTransacao;
	}

	public Date getDtAprovacao() {
		return dtAprovacao;
	}

	public void setDtAprovacao(Date dtAprovacao) {
		this.dtAprovacao = dtAprovacao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getNmTransacao() {
		return nmTransacao;
	}

	public void setNmTransacao(String nmTransacao) {
		this.nmTransacao = nmTransacao;
	}

	public List<DetalheSolicitacaoSaque> getDetalheSolic() {
		return detalheSolic;
	}

	public void setDetalheSolic(List<DetalheSolicitacaoSaque> detalheSolic) {
		this.detalheSolic = detalheSolic;
	}

	public TipoTransacao getTipoTransacao() {
		return tipoTransacao;
	}

	public void setTipoTransacao(TipoTransacao tipoTransacao) {
		this.tipoTransacao = tipoTransacao;
	}

	public String getTokenAprovacao() {
		return tokenAprovacao;
	}

	public void setTokenAprovacao(String tokenAprovacao) {
		this.tokenAprovacao = tokenAprovacao;
	}
	
}
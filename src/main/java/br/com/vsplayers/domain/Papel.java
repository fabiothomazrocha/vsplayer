package br.com.vsplayers.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="T_PAPEL")
@SuppressWarnings("serial")
public class Papel implements Serializable {
	
	@Id
    @Column(name="ID_PAPEL")
    @GeneratedValue
	private Long id;
	
	@Column(name="NM_NOME", nullable = false, length = 20)
	private String nmNome;
	
	@Column(name="DS_DESCRICAO", nullable = true, length = 255)
	private String dsDescricao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmNome() {
		return nmNome;
	}

	public void setNmNome(String nmNome) {
		this.nmNome = nmNome;
	}

	public String getDsDescricao() {
		return dsDescricao;
	}

	public void setDsDescricao(String dsDescricao) {
		this.dsDescricao = dsDescricao;
	}
	
}
package br.com.vsplayers.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.vsplayers.domain.enums.Flag;
import br.com.vsplayers.domain.enums.TipoUsuario;
import br.com.vsplayers.infra.utils.StringUtils;

/**
 * @author Rodolfo Martins - furstmartins
 */
@Entity
@Table(name = "T_USUARIO")
@SuppressWarnings("serial")
public class Usuario implements Serializable {

	@Id
	@Column(name = "ID_USUARIO")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "DS_NOME", nullable = true, length = 255)
	private String nmNome;

	@Column(name = "DS_CPF", nullable = true, length = 14)
	private String dsCpf;

	@Column(name = "DS_EMAIL", nullable = false, length = 255)
	private String dsEmail;

	@Column(name = "DS_SENHA", nullable = false, length = 32)
	private String dsSenha;

	@Column(name = "TP_USUARIO", length = 5, nullable = false)
	@Enumerated(EnumType.STRING)
	private TipoUsuario tipoUsuario;

	@ManyToMany(fetch = FetchType.LAZY, targetEntity = Papel.class)
	@JoinTable(name = "T_USUARIO_PAPEL", joinColumns = @JoinColumn(name = "ID_USUARIO"), inverseJoinColumns = @JoinColumn(name = "ID_PAPEL"))
	private List<Papel> papeis;

	@Enumerated(EnumType.STRING)
	@Column(name = "FL_ATIVADO", length = 1, nullable = false)
	private Flag ativado;

	@Column(name = "DS_CODIGO_ATIVACAO", length = 32, nullable = true)
	private String codigoAtivacao;

	@Enumerated(EnumType.STRING)
	@Column(name = "FL_SOLIC_ALTER", length = 1, nullable = true)
	private Flag solicitaAltSenha;

	@Column(name = "DS_CODIGO_ALTSENHA", length = 32, nullable = true)
	private String codigoAlteracaoSenha;

	@OneToMany(mappedBy = "usuario", fetch = FetchType.LAZY)
	private List<AcessoRedeJogo> listaAcessoRedeJogo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PAIS")
	private Pais pais;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DT_CADASTRO", nullable = false)
	private Date dataCadastro;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DT_ATIVACAO", nullable = true)
	private Date dataAtivacao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DT_ULT_ATUALIZACAO", nullable = false)
	private Date dataUltimaAtualizacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNmNome() {
		return nmNome;
	}

	public void setNmNome(String nmNome) {
		this.nmNome = nmNome;
	}

	public String getDsCpf() {
		return dsCpf;
	}

	public void setDsCpf(String dsCpf) {
		this.dsCpf = dsCpf;
	}

	public String getDsEmail() {
		return dsEmail;
	}

	public void setDsEmail(String dsEmail) {
		this.dsEmail = dsEmail;
	}

	public String getDsSenha() {
		return dsSenha;
	}

	public void setDsSenha(String dsSenha) {
		this.dsSenha = dsSenha;
	}

	public TipoUsuario getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public List<Papel> getPapeis() {
		return papeis;
	}

	public void setPapeis(List<Papel> papeis) {
		this.papeis = papeis;
	}

	public Flag getAtivado() {
		return ativado;
	}

	public void setAtivado(Flag ativado) {
		this.ativado = ativado;
	}

	public String getCodigoAtivacao() {
		return codigoAtivacao;
	}

	public void setCodigoAtivacao(String codigoAtivacao) {
		this.codigoAtivacao = codigoAtivacao;
	}

	public Flag getSolicitaAltSenha() {
		return solicitaAltSenha;
	}

	public void setSolicitaAltSenha(Flag solicitaAltSenha) {
		this.solicitaAltSenha = solicitaAltSenha;
	}

	public String getCodigoAlteracaoSenha() {
		return codigoAlteracaoSenha;
	}

	public void setCodigoAlteracaoSenha(String codigoAlteracaoSenha) {
		this.codigoAlteracaoSenha = codigoAlteracaoSenha;
	}

	public List<AcessoRedeJogo> getListaAcessoRedeJogo() {
		return listaAcessoRedeJogo;
	}

	public void setListaAcessoRedeJogo(List<AcessoRedeJogo> listaAcessoRedeJogo) {
		this.listaAcessoRedeJogo = listaAcessoRedeJogo;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataAtivacao() {
		return dataAtivacao;
	}

	public void setDataAtivacao(Date dataAtivacao) {
		this.dataAtivacao = dataAtivacao;
	}

	public Date getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}

	public void setDataUltimaAtualizacao(Date dataUltimaAtualizacao) {
		this.dataUltimaAtualizacao = dataUltimaAtualizacao;
	}

	@Override
	public String toString() {
		return StringUtils.normalizaToString("id=", this.id, "cpf=",
				this.dsCpf, "nome=", this.nmNome);
	}
}

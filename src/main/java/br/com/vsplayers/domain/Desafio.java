package br.com.vsplayers.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "T_DESAFIO")
public class Desafio {
	
	@Id
	@Column(name = "ID_DESAFIO")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_JOGO", nullable=false)
	private Jogo jogo;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="desafio")
	private List<RegistroFinanceiro> listaRegistroFinanceiro;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_USUARIO_P1")
	private Usuario usuarioP1;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_USUARIO_P2", nullable = false)
	private Usuario usuarioP2;
	
	@Temporal(TemporalType.DATE)
	@Column(name="DT_INI_DESAFIO", nullable = false)
	private Date dtInicioDesafio;
	
	@Temporal(TemporalType.DATE)
	@Column(name="DT_FIM_DESAFIO", nullable = true)
	private Date dtFimDesafio;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SALA_JOGOS", nullable = true)
	private SalaJogos salaJogos;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="desafio")
	private List<ResultadoDesafio> listaResultadoDesafio;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Jogo getJogo() {
		return jogo;
	}

	public void setJogo(Jogo jogo) {
		this.jogo = jogo;
	}

	public List<RegistroFinanceiro> getListaRegistroFinanceiro() {
		return listaRegistroFinanceiro;
	}

	public void setListaRegistroFinanceiro(
			List<RegistroFinanceiro> listaRegistroFinanceiro) {
		this.listaRegistroFinanceiro = listaRegistroFinanceiro;
	}

	public Usuario getUsuarioP1() {
		return usuarioP1;
	}

	public void setUsuarioP1(Usuario usuarioP1) {
		this.usuarioP1 = usuarioP1;
	}

	public Usuario getUsuarioP2() {
		return usuarioP2;
	}

	public void setUsuarioP2(Usuario usuarioP2) {
		this.usuarioP2 = usuarioP2;
	}

	public Date getDtInicioDesafio() {
		return dtInicioDesafio;
	}

	public void setDtInicioDesafio(Date dtInicioDesafio) {
		this.dtInicioDesafio = dtInicioDesafio;
	}

	public Date getDtFimDesafio() {
		return dtFimDesafio;
	}

	public void setDtFimDesafio(Date dtFimDesafio) {
		this.dtFimDesafio = dtFimDesafio;
	}

	public SalaJogos getSalaJogos() {
		return salaJogos;
	}

	public void setSalaJogos(SalaJogos salaJogos) {
		this.salaJogos = salaJogos;
	}

	public List<ResultadoDesafio> getListaResultadoDesafio() {
		return listaResultadoDesafio;
	}

	public void setListaResultadoDesafio(
			List<ResultadoDesafio> listaResultadoDesafio) {
		this.listaResultadoDesafio = listaResultadoDesafio;
	}
	

}
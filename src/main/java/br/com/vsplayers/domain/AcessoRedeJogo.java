package br.com.vsplayers.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import br.com.vsplayers.domain.enums.Flag;
import br.com.vsplayers.domain.enums.TipoAcessoRedeJogo;

@Entity
@Table(name = "T_ACSREDEJOGO")
public class AcessoRedeJogo {

	@Id
	@Column(name = "ID_ACSREDEJOGO")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_USUARIO", nullable = false)
	private Usuario usuario;
	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToMany
	@JoinTable(name = "T_ACESSO_CONSOLE", joinColumns = @JoinColumn(name = "ID_ACSREDEJOGO"), inverseJoinColumns = @JoinColumn(name = "ID_CONSOLE"))
	private List<Console> consoles;

	@Enumerated(EnumType.STRING)
	@Column(name = "TP_ACESSO", length = 4, nullable = false)
	private TipoAcessoRedeJogo tipoAcessoRedeJogo;

	@Column(name = "NM_USERID", length = 50, nullable = false)
	private String userId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DT_CRIACAO", nullable = false)
	private Date dataCriacao;
	
	@Enumerated(EnumType.STRING)
	@Column(name="FL_ATIVO")
	private Flag ativo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Console> getConsoles() {
		return consoles;
	}

	public void setConsoles(List<Console> consoles) {
		this.consoles = consoles;
	}

	public TipoAcessoRedeJogo getTipoAcessoRedeJogo() {
		return tipoAcessoRedeJogo;
	}

	public void setTipoAcessoRedeJogo(TipoAcessoRedeJogo tipoAcessoRedeJogo) {
		this.tipoAcessoRedeJogo = tipoAcessoRedeJogo;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Flag getAtivo() {
		return ativo;
	}

	public void setAtivo(Flag ativo) {
		this.ativo = ativo;
	}

}
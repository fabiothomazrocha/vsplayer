package br.com.vsplayers.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.vsplayers.domain.enums.TipoBanco;

@Entity
@Table(name = "T_DET_SOLICITACAO_SAQUE")
public class DetalheSolicitacaoSaque {

	@Id
	@Column(name = "ID_DET_SOLICITACAO_SAQUE")
	@GeneratedValue
	private Long id;

	@Column(name = "NR_CPF", nullable = false)
	private String cpf;

	@Column(name = "DS_AGENCIA", nullable = true)
	private String agencia;

	@Column(name = "DS_CONTA", nullable = true)
	private String conta;

	@Enumerated(EnumType.STRING)
	@Column(name = "TP_BANCO", length = 5)
	private TipoBanco tipoBanco;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getConta() {
		return conta;
	}

	public void setConta(String conta) {
		this.conta = conta;
	}

	public TipoBanco getTipoBanco() {
		return tipoBanco;
	}

	public void setTipoBanco(TipoBanco tipoBanco) {
		this.tipoBanco = tipoBanco;
	}
	
}
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="includes/header.jsp" />
</head>
<body>

	<jsp:include page="includes/navbarcomlogin.jsp" />

	<div id="form-login-div" class="container" style="top-margin: 200px;">

		<div class="well well-sm">Informe seu e-mail e senha para entrar
			no sistema</div>


		<c:if test="${param.error != null}">
			<div class="alert alert-info">Usuário e senha inválido(s).</div>
		</c:if>

		<br />

		<form action="<c:url value="/login"/>" method="post"
			class="form-horizontal" role="form">

			<div class="form-group">
				<label for="username" class="col-sm-2 control-label">Email</label>
				<div class="col-sm-10">
					<input type="text" id="username" name="username"
						placeholder="Informe seu e-mail" style="width: 300px;"
						class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label for="password" class="col-sm-2 control-label">Senha</label>
				<div class="col-sm-10">
					<input type="password" id="password" name="password"
						placeholder="Informe sua senha" style="width: 300px;"
						class="form-control" />
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">Entrar</button>
				</div>
			</div>

			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-9">
					<a href="<c:url value="/auth/facebook"/>"><img
						src="<c:url value="/resources/images/social/conectar_com_facebook.png"/>"></a>
				</div>
			</div>

			<br />

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-9">
					<a href="<c:url value="/getPassword"/>"><span>Esqueci
							minha senha</span></a> <span>&nbsp-&nbsp</span> <a
						href="<c:url value="/getCodigoAtivacao"/>"><span>Código
							de Ativação</span></a>
				</div>
			</div>
		</form>

	</div>
	</div>

	<jsp:include page="includes/footer.jsp" />

</body>
</html>
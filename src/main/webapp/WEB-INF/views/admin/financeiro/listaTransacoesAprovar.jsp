<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
</head>
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />

	<script>
		function novoDeposito() {
			window.location.href = '<c:url value="/restricted/financeiro/novoDeposito"/>';
		}
		function verTransacao(idTransacao) {
			formAcao.action = '<c:url value="/restricted/financeiro/viewTransacao"/>'
					+ '/' + idTransacao;
			formAcao.submit();
		}
		function abrirModalDetalhes() {
			
			var idTrxSelecionadoVal = $('input[name=idTrxSelecionado]:checked', '#formAcao').val();
			
			if(idTrxSelecionadoVal > 0) {
				jQuery.ajax({
					type : "GET",
					dataType : "json",
					url : "<c:url value="/admin/financeiro/rest/getDadosParaDeposito/"/>/" + idTrxSelecionadoVal ,
					success : function(data) {
						$("#dialogDetalhes").modal('show');
						$("#banco").text('Banco: ' + data.nomeBancoSolic);
						$("#agencia").text('Agência: ' + data.agenciaBancoSolic);
						$("#conta").text('Conta: ' + data.numeroContaBancoSolic);
						$("#cpf").text('CPF: ' + data.cpfTitularSolic);
						$("#titular").text('Titular: ' + data.nomeBanco);
					}
				});
			}
		}
		
		function confirmarDeposito() {
			
			var idTrxSelecionadoVal = $('input[name=idTrxSelecionado]:checked', '#formAcao').val();
			
			jQuery.ajax({
				type : "GET",
				dataType : "json",
				url : "<c:url value="/admin/financeiro/rest/confirmDeposito/"/>/" + idTrxSelecionadoVal ,
				success : function(data) {
					if(data) {
						$("#dialogDetalhes").modal('hide');
						window.location.href = "<c:url value="/admin/financeiro/listarTransacoesAprovar"/>";
					}
					
				}
			});
		}
		
		function fecharDetalhes() {
			$("#dialogDetalhes").modal('hide');
		}
		
	</script>

	<div class="container">
		<form name="formAcao" id="formAcao" action="" method="get">
			<br /> <br />

			<div class="panel panel-default">

				<div class="panel-heading">Transações Financeiras</div>

				<c:if test="${empty requestScope.listaTrx}">
					<div class="alert alert-warning">Você ainda não possui
						nenhuma transação.</div>
				</c:if>

				<c:if test="${not empty requestScope.listaTrx}">

					<table class="table">
						<thead>
							<tr style="background-color: #f0f0f0">
								<td width="5%"><span class="glyphicon glyphicon-ok-circle"></span></td>
								<td width="30%"><strong>Nome Transação</strong></td>
								<td width="15%"><strong>Data Transação</strong></td>
								<td width="15%"><strong>Data Aprovação</strong></td>
								<td width="10%"><strong>Valor (R$)</strong></td>
								<td width="20%"><strong>Usuário</strong></td>
								<td width="10%"><strong>Aprovada</strong></td>
								<td width="10%"><strong>&nbsp;</strong></td>
							</tr>
						</thead>

						<tbody>
							<c:forEach var="trx" items="${requestScope.listaTrx}"
								varStatus="statsVar">
								<tr>
									<td><input id="idTrxSelecionado" name="idTrxSelecionado"
										type="radio" value="${trx.idTransacao}"
										<c:if test="${statsVar.index == 0}"> checked="checked" </c:if> /></td>
									<td>${trx.nmTransacao}</td>
									<td>${trx.dataTransacao}</td>
									<td>${trx.dataAprovacao}</td>
									<td>${trx.valorFormatado}</td>
									<td>${trx.nomeUsuario}</td>
									<td>${trx.aprovada.descricao}</td>
									<td><button type="button" class="btn btn-primary btn-xs"
											onclick="verTransacao('${trx.idTransacao}');">Ver
											Transação</button></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>

				</c:if>
			</div>
			<div class="form-group">
				<div align="center">
					<button type="button" class="btn btn-primary"
						onclick="abrirModalDetalhes();">Ver detalhes / Confirmar
						Depósito</button>
				</div>
			</div>
		</form>
	</div>

	<div class="modal fade" id="dialogDetalhes" role="dialog"
		aria-labelledby="dialogDetalhes" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Dados da conta para depósito</h4>
				</div>
				<div class="modal-body">
					<p>
						<span id="banco"></span><br /> <span id="agencia"></span><br />
						<span id="conta"></span><br /> <span id="cpf"></span><br /> <span
							id="nomeTitular"></span><br />
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" id="confirm"
						onclick="confirmarDeposito();">Confirmar Depósito</button>

					<button type="button" class="btn btn-primary" id="close"
						onclick="fecharDetalhes();">Fechar Janela</button>
				</div>
			</div>
		</div>
	</div>

	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
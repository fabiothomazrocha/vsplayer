<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
<script>
	jQuery(document).ready(function() {
		$("#valorSala").maskMoney({
			showSymbol : true,
			symbol : "R$",
			decimal : ",",
			thousands : ".",
			allowZero : true
		});
	});
</script>
</head>
<body>
	<jsp:include page="../../includes/navbarcomlogin.jsp" />
	<div class="container">
		<h3>Cadastrar Sala de Jogos</h3>
		<br />
		<div>
			<sf:form action="editarSalaJogos"
				commandName="cadastrarSalaJogosForm" method="post"
				class="form-horizontal" enctype="multipart/form-data">
				<spring:hasBindErrors htmlEscape="true"
					name="cadastrarSalaJogosForm">
					<c:if test="${errors.errorCount gt 0}">
						<div class="alert alert-info">
							<sf:errors path="*" />
						</div>
					</c:if>
				</spring:hasBindErrors>
				<div class="form-group">
					<label for="nmSalaJogos" class="col-sm-2 control-label">Nome
						da Sala</label>
					<div class="col-sm-10">
						<sf:input path="nmSalaJogos" class="form-control" id="nmSalaJogos"
							placeholder="Informe o nome da sala" style="width: 300px;"
							tabindex="1" htmlEscape="true" maxlength="255"
							title="Sua Sala, Ex: Minha Sala" />
					</div>
				</div>
				<div class="form-group">
					<label for="nmFaq" class="col-sm-2 control-label">Valor da
						Sala</label>
					<div class="col-sm-10">
						<sf:input path="valorSala" class="form-control" id="valorSala"
							placeholder="Informe o valor da sala" style="width: 150px;"
							tabindex="1" htmlEscape="true" maxlength="255"
							title="20,00, Ex: 100,00" />
					</div>
				</div>

				<div class="form-group">
					<label for="idJogo" class="col-sm-2 control-label">Nome do
						Jogo</label>
					<div class="col-sm-10">
						<sf:select path="idJogo" class="selectpicker" id="idJogo"
							style="width: 300px;" tabindex="3" htmlEscape="true"
							maxlength="255">
							<sf:options items="${listaJogos}" itemValue="id"
								itemLabel="nomeJogo" />
						</sf:select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-default"
							onclick="document.cadastrarSalaJogosForm.submit();">Cadastrar</button>
						<button type="button" class="btn btn-default"
							onclick="javascript:window.location.href='<c:url value="/admin/salaJogos/listSalaJogos"/>'">Cancelar</button>
					</div>
				</div>
				<sf:hidden path="id" />
			</sf:form>
		</div>
	</div>

	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
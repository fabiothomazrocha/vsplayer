<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
</head>
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />

	<div class="container">

		<div class="form-group" style="padding-left: 100px; width: 500px;">
			<div class="media">

				<div class="media-body"></div>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-10">
				<h3>${requestScope.salaJogos.nmSalaJogos}</h3>
			</div>
		</div>

		<div class="form-group">

			<div class="col-sm-10">
				<label for="txItem" class="col-sm-2 control-label">Nome do
					Jogo: </label> <small>${requestScope.salaJogos.jogo.nmJogo}</small>
			</div>
		</div>
		<div class="form-group">

			<div class="col-sm-10">
				<label for="txItem" class="col-sm-2 control-label">Valor da
					Sala:</label> <small>R$ ${requestScope.salaJogos.valorSala}</small>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="button" class="btn btn-default"
					onclick="javascript:window.location.href='<c:url value="/admin/salaJogos/listSalaJogos"/>'">Voltar</button>
			</div>
		</div>
	</div>
	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
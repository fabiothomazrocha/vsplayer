<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
</head>
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />

	<script>
		function cadastrar() {
			formAcao.action = '<c:url value="/admin/regras/createRegra"/>';
			formAcao.submit();
		}

		function editar() {
			formAcao.action = '<c:url value="/admin/regras/editRegra"/>';
			formAcao.submit();
		}

		function excluir() {
			formAcao.action = '<c:url value="/admin/regras/deleteRegra"/>';
			formAcao.submit();
		}

		 function verRegra(idRegra) {
			formAcao.action = '<c:url value="/admin/regras/viewRegra"/>'
					+ '/' + idRegra;
			formAcao.submit();
		} 
	</script>

	<div class="container">
		<form name="formAcao" action="" method="get">
			<h2>Minhas Regras</h2>

			<div class="panel panel-default">

				<div class="panel-heading">Regras Ativas</div>

				<c:if test="${empty requestScope.listRegras}">

					<div class="alert alert-warning">Você ainda não possui nenhuma
						Regra cadastrada.</div>
				</c:if>

				<c:if test="${not empty requestScope.listRegras}">

					<table class="table">
						<thead>
							<tr style="background-color: #f0f0f0">
								<td width="5%"><span class="glyphicon glyphicon-ok-circle"></span></td>
								<td width="50%"><strong>Nome da Regra</strong></td>
								<td width="15%"><strong>Descricão</strong></td>
								<!-- <td width="15%"><strong>Usuario</strong></td> -->
								<td width="10%"><strong>&nbsp;</strong></td>
							</tr>
						</thead>

						<tbody>
							<c:forEach var="regra" items="${requestScope.listRegras}"
								varStatus="statsVar">
								<tr>
									<td><input id="idRegraSelecionada"
										name="idRegraSelecionado" type="radio"
										value="${regra.id}"
										<c:if test="${statsVar.index == 0}"> checked="checked" </c:if> /></td>
									<td>${regra.nmInstancia}</td>
									<td>${regra.txItem}</td>
									<%-- <td>${regra.vsPlayers.nmInstancia}</td> --%>
									 <td><button type="button" class="btn btn-primary btn-xs"
											onclick="verRegra('${regra.id}');">Ver
											Regra</button></td> 
								</tr>
							</c:forEach>
						</tbody>
					</table>

				</c:if>
			</div>


			<div class="form-group">
				<div align="center">
					<button type="button" class="btn btn-primary"
						onclick="cadastrar();">Cadastrar Nova Regra</button>
					<button <c:if test="${empty requestScope.listRegras}"> disabled="disabled" </c:if> type="button" class="btn btn-info" onclick="editar();">Editar</button>
					<button <c:if test="${empty requestScope.listRegras}"> disabled="disabled" </c:if> class="btn btn-danger" type="button" data-toggle="modal"
						data-target="#confirmExcluir" data-title="Excluir Regra"
						data-message="Tem certeza que deseja excluir esta Regra? ?">
						Excluir Regra</button>
				</div>
			</div>
		</form>
	</div>

	<!-- Modal Dialog -->

	<div class="modal fade" id="confirmExcluir" role="dialog"
		aria-labelledby="confirmDesativarLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Excluir Regra</h4>
				</div>
				<div class="modal-body">
					<p>Deseja realmente excluir esta Regra?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="button" class="btn btn-danger" id="confirm"
						onclick="excluir();">Sim, Excluir</button>
				</div>
			</div>
		</div>
	</div>


	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
</head>
<body>
	<jsp:include page="../../includes/navbarcomlogin.jsp" />
	<div class="container">
	<h3>Criar Regra</h3>
		
		<br/>
		<div>
			<sf:form action="cadastrarRegra" commandName="cadastrarRegraForm"
				method="post" class="form-horizontal" enctype="multipart/form-data">

				<spring:hasBindErrors htmlEscape="true" name="cadastrarRegraForm">
					<c:if test="${errors.errorCount gt 0}">
						<div class="alert alert-info">
							<sf:errors path="*" />
						</div>
					</c:if>
				</spring:hasBindErrors>
				
				<div class="form-group">
					<label for="nmInstancia" class="col-sm-2 control-label">Nome da Regra</label>
					<div class="col-sm-10">
						<sf:input path="nmInstancia" class="form-control" id="nmInstancia"
							placeholder="Informe o nome da regra" style="width: 300px;"
							tabindex="1" htmlEscape="true" maxlength="255"
							title="Sua Regra, Ex: Regra 1" />
					</div>
				</div>

				<div class="form-group">
					<label for="txItem" class="col-sm-2 control-label">Descrição da Regra</label>
					<div class="col-sm-10">
						<sf:textarea class="form-control" path="txItem" id="txItem" rows="3"
							placeholder="Descreva o que é a sua regra" style="width: 300px;"
							tabindex="2" htmlEscape="true" maxlength="400" />
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-default" onclick="document.cadastrarRegraForm.submit();">Cadastrar</button>
						<button type="button" class="btn btn-default" onclick="javascript:window.location.href='<c:url value="/admin/regras/listRegra"/>'">Cancelar</button>
					</div>
				</div>
				
				
				</sf:form>
</div>
	</div>

	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
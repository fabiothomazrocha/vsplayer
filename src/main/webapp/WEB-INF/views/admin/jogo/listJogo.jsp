<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
</head>
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />
<script>
		function cadastrar() {
			formAcao.action = '<c:url value="/admin/jogo/createJogo"/>';
			formAcao.submit();
		}

		function editar() {
			formAcao.action = '<c:url value="/admin/jogo/editJogo"/>';
			formAcao.submit();
		}

		function excluir() {
			formAcao.action = '<c:url value="/admin/jogo/deleteJogo"/>';
			formAcao.submit();
		}

		 function verJogo(idJogo) {
			formAcao.action = '<c:url value="/admin/jogo/viewJogo"/>'
					+ '/' + idJogo;
			formAcao.submit();
		} 
	</script>
	<div class="container">
		<form name="formAcao" action="" method="get">

			<h2>Jogos Cadastrados</h2>

			<div class="panel panel-default">

				<div class="panel-heading">Jogos Cadastrados</div>

				<c:if test="${empty requestScope.listaJogos}">

					<div class="alert alert-warning">Não existem Jogos
						cadastrados</div>
				</c:if>

				<c:if test="${not empty requestScope.listaJogos}">
					<table class="table">
						<thead>
							<tr style="background-color: #f0f0f0">
								<td width="5%"><span class="glyphicon glyphicon-ok-circle"></span></td>
								<td width="50%"><strong>Nome do Jogo</strong></td>
								<td width="25%"><strong>Tipo do Jogo</strong></td>
								<td width="25%"><strong>Console</strong></td>
								<td width="10%"><strong>&nbsp;</strong></td>
							</tr>
						</thead>

						<tbody>
							<c:forEach var="jogo" items="${requestScope.listaJogos}"
								varStatus="statsVar">
								<tr>
								<td><input id="idJogoSelecionado"
										name="idJogoSelecionado" type="radio"
										value="${jogo.id}"
										<c:if test="${statsVar.index == 0}"> checked="checked" </c:if> /></td>
									<td>${jogo.nomeJogo}</td>
									<td>${jogo.tipoJogo.tipoJogo}</td>
									<td>${jogo.nomeConsole}</td>
									 <td><button type="button" class="btn btn-primary btn-xs"
											onclick="verJogo('${jogo.id}');">Ver
											Jogo</button></td> 
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
			</div>
			
			
			<div class="form-group">
				<div align="center">
					<button type="button" class="btn btn-primary"
						onclick="cadastrar();">Cadastrar Jogo</button>
					<button <c:if test="${empty requestScope.listaJogos}"> disabled="disabled" </c:if> type="button" class="btn btn-info" onclick="editar();">Editar Jogo</button>
					<button <c:if test="${empty requestScope.listaJogos}"> disabled="disabled" </c:if> class="btn btn-danger" type="button" data-toggle="modal"
						data-target="#confirmExcluir" data-title="Excluir Jogo"
						data-message="Tem certeza que deseja excluir este Jogo?">
						Excluir Jogo</button>
				</div>
			</div>
		</form>
	</div>
<!-- Modal Dialog -->

	<div class="modal fade" id="confirmExcluir" role="dialog"
		aria-labelledby="confirmDesativarLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Excluir Jogo</h4>
				</div>
				<div class="modal-body">
					<p>Deseja realmente excluir esta Jogo?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="button" class="btn btn-danger" id="confirm"
						onclick="excluir();">Sim, Excluir</button>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
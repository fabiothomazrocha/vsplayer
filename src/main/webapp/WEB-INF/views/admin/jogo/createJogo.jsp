<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
</head>
<body>
	<jsp:include page="../../includes/navbarcomlogin.jsp" />
	<div class="container">
		<h3>Cadastrar Jogo</h3>

		<br />
		<div>
			<sf:form action="cadastrarJogo" commandName="cadastrarJogoForm"
				method="post" class="form-horizontal" enctype="multipart/form-data">
				<spring:hasBindErrors htmlEscape="true" name="cadastrarJogoForm">
					<c:if test="${errors.errorCount gt 0}">
						<div class="alert alert-info">
							<sf:errors path="*" />
						</div>
					</c:if>
				</spring:hasBindErrors>

				<div class="form-group">
					<label for="nomeJogo" class="col-sm-2 control-label">Nome
						da Jogo</label>
					<div class="col-sm-10">
						<sf:input path="nomeJogo" class="form-control" id="nomeJogo"
							placeholder="Informe o nome do jogo" style="width: 300px;"
							tabindex="1" htmlEscape="true" maxlength="255"
							title="Seu Jogo, Ex: Meu Jogo" />
					</div>
				</div>
				<div class="form-group">
					<label for="nomeJogo" class="col-sm-2 control-label">Tipo
						de Jogo</label>
					<div class="col-sm-10">
						<sf:select path="tipoJogo" class="selectpicker  show-menu-arrow"
							id="tipoJogo" placeholder="Selecio um tipo de jogo" tabindex="2">
							<sf:option value="">&nbsp;</sf:option>
							<sf:options items="${tipoJogoList}" itemLabel="tipoJogo" />
						</sf:select>
					</div>
				</div>
				<div class="form-group">
					<label for="txItem" class="col-sm-2 control-label">Nome do
						console</label>
					<div class="col-sm-10">
						<sf:select path="idConsole" class="selectpicker" id="idConsole"
							style="width: 300px;" tabindex="3" htmlEscape="true"
							maxlength="255">
							<sf:options items="${listaConsoles}" itemValue="idConsole"
								itemLabel="nomeConsole" />
						</sf:select>
					</div>
				</div>
	

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-default"
					onclick="document.cadastrarJogoForm.submit();">Cadastrar</button>
				<button type="button" class="btn btn-default"
					onclick="javascript:window.location.href='<c:url value="/admin/jogos/listJogos"/>'">Cancelar</button>
			</div>
		</div>

		</sf:form>
		</div>
	</div>


	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
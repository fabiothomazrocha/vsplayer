<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
</head>
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />

	<div class="container">

		<div class="form-group" style="padding-left: 100px; width: 500px;">
			<div class="media">

				<div class="media-body"></div>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-10">
				<h3>${requestScope.jogo.nmJogo}</h3>
			</div>
		</div>

		<div class="form-group">
		
			<div class="col-sm-10">
			<label for="txItem" class="col-sm-2 control-label">Tipo de jogo: </label>
				<small>${requestScope.jogo.tipoJogo.tipoJogo}</small>
			</div>
		</div>
		<div class="form-group">
	
			<div class="col-sm-10">
				<label for="txItem" class="col-sm-2 control-label">Console: </label>
				<small>${requestScope.jogo.console.nmConsole}</small>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">


				<button type="button" class="btn btn-default"
					onclick="javascript:window.location.href='<c:url value="/admin/jogos/listJogos"/>'">Voltar</button>
			</div>
		</div>
	</div>


	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
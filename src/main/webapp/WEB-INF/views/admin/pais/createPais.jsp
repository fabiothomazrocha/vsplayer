<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
</head>
<body>
	<jsp:include page="../../includes/navbarcomlogin.jsp" />
	<div class="container">
		<h3>Cadastrar País</h3>
		<br />
		<div>
			<sf:form action="cadastrarPais" commandName="cadastrarPaisForm"
				method="post" class="form-horizontal" enctype="multipart/form-data">
				<spring:hasBindErrors htmlEscape="true" name="cadastrarPaisForm">
					<c:if test="${errors.errorCount gt 0}">
						<div class="alert alert-info">
							<sf:errors path="*" />
						</div>
					</c:if>
				</spring:hasBindErrors>
				<div class="form-group">
					<label for="nmFaq" class="col-sm-2 control-label">Nome do
						País</label>
					<div class="col-sm-10">
						<sf:input path="nmPais" class="form-control" id="nmPais"
							placeholder="Informe o nome do País" style="width: 300px;"
							tabindex="1" htmlEscape="true" maxlength="255"
							title="Seu País, Ex: Brasil" />
					</div>
				</div>
				<div class="form-group">
					<label for="nmFaq" class="col-sm-2 control-label">Código do
						País</label>
					<div class="col-sm-10">
						<sf:input path="cdPais" class="form-control" id="cdPais"
							placeholder="Informe o código  do País" style="width: 300px;"
							tabindex="2" htmlEscape="true" maxlength="255"
							title="Seu código do
						País, Ex: 55" />
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-default"
							onclick="document.cadastrarPaisForm.submit();">Cadastrar</button>
						<button type="button" class="btn btn-default"
							onclick="javascript:window.location.href='<c:url value="/admin/pais/listPais"/>'">Cancelar</button>
					</div>
				</div>
			</sf:form>
		</div>
	</div>

	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
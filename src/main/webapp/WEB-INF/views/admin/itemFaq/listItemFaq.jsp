<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
</head>
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />
	<script>
		function cadastrar() {
			formAcao.action = '<c:url value="/admin/itemFaq/createItemFaq"/>';
			formAcao.submit();
		}

		function editar() {
			formAcao.action = '<c:url value="/admin/itemFaq/editItemFaq"/>';
			formAcao.submit();
		}

		function excluir() {
			formAcao.action = '<c:url value="/admin/itemFaq/deleteItemFaq"/>';
			formAcao.submit();
		}

		function verItemFaq(idItemFaq) {
			formAcao.action = '<c:url value="/admin/itemFaq/viewItemFaq"/>'
					+ '/' + idItemFaq;
			formAcao.submit();
		}
	</script>
	<div class="container">
		<form name="formAcao" action="" method="get">

			<h2>Itens Faq Cadastrados</h2>

			<div class="panel panel-default">

				<div class="panel-heading">Itens Faq Cadastrados</div>

				<c:if test="${empty requestScope.listaItemFaq}">

					<div class="alert alert-warning">Não existem FAQs cadastrados</div>
				</c:if>

				<c:if test="${not empty requestScope.listaItemFaq}">

					<table class="table">
						<thead>
							<tr style="background-color: #f0f0f0">
								<td width="5%"><span class="glyphicon glyphicon-ok-circle"></span></td>
								<td width="50%"><strong>Pergunta:</strong></td>
								<td width="50%"><strong>FAQ:</strong></td>
								<td width="10%"><strong>&nbsp;</strong></td>
							</tr>
						</thead>

						<tbody>
							<c:forEach var="itemFaq" items="${requestScope.listaItemFaq}"
								varStatus="statsVar">
								<tr>
									<td><input id="idItemFaqSelecionado"
										name="idItemFaqSelecionado" type="radio" value="${itemFaq.id}"
										<c:if test="${statsVar.index == 0}"> checked="checked" </c:if> /></td>
									<td>${itemFaq.dsTituloItem}</td>
									<td>${itemFaq.nomeFaq}</td>
									<td><button type="button" class="btn btn-primary btn-xs"
											onclick="verItemFaq('${itemFaq.id}');">Ver Item FAQ</button></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>

				</c:if>
			</div>
			<div class="form-group">
				<div align="center">
					<button type="button" class="btn btn-primary"
						onclick="cadastrar();">Cadastrar Item FAQ</button>
					<button
						<c:if test="${empty requestScope.listaItemFaq}"> disabled="disabled" </c:if>
						type="button" class="btn btn-info" onclick="editar();">Editar
						Item FAQ</button>
					<button
						<c:if test="${empty requestScope.listaItemFaq}"> disabled="disabled" </c:if>
						class="btn btn-danger" type="button" data-toggle="modal"
						data-target="#confirmExcluir" data-title="Excluir Item FAQ"
						data-message="Tem certeza que deseja excluir este Item FAQ?">
						Excluir Item FAQ</button>
				</div>
			</div>
		</form>
	</div>
	<div class="modal fade" id="confirmExcluir" role="dialog"
		aria-labelledby="confirmDesativarLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Excluir Item FAQ</h4>
				</div>
				<div class="modal-body">
					<p>Deseja realmente excluir este Item FAQ?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="button" class="btn btn-danger" id="confirm"
						onclick="excluir();">Sim, Excluir</button>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
</head>
<body>
	<jsp:include page="../../includes/navbarcomlogin.jsp" />
	<div class="container">
		<h3>Editar ItemFAQ</h3>
		<br />
		<div>
			<sf:form action="editarItemFaq" commandName="cadastrarItemFaqForm"
				method="post" class="form-horizontal" >
				<spring:hasBindErrors htmlEscape="true" name="cadastrarItemFaqForm">
					<c:if test="${errors.errorCount gt 0}">
						<div class="alert alert-info">
							<sf:errors path="*" />
						</div>
					</c:if>
				</spring:hasBindErrors>
				<div class="form-group">
					<label for="idFaq" class="col-sm-2 control-label">Tipo de
						FAQ:</label>
					<div class="col-sm-10">
						<sf:select path="idFaq" class="selectpicker" id="idFaq"
							style="width: 300px;" tabindex="3" htmlEscape="true"
							maxlength="255">
							<sf:options items="${listaFaq}" itemValue="id" itemLabel="nmFaq" />
						</sf:select> 
					</div>
				</div>
				<div class="form-group">
					<label for="dsTituloItem" class="col-sm-2 control-label">Pergunta:</label>
					<div class="col-sm-10">
						<sf:input path="dsTituloItem" class="form-control"
							id="dsTituloItem" placeholder="Informe uma Pergunta"
							style="width: 300px;" tabindex="1" htmlEscape="true"
							maxlength="255" title="Sua Pergunta, Ex: Minha pergunta ?" />
					</div>
				</div>
				<div class="form-group">
					<label for="txItem" class="col-sm-2 control-label">Resposta:</label>
					<div class="col-sm-10">
						<sf:textarea path="txItem" class="form-control" id="txItem"
							placeholder="Informe uma Resposta" style="width: 300px;"
							tabindex="1" htmlEscape="true" maxlength="255"
							title="Sua Pergunta, Ex: Minha pergunta ?" />
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-default"
							onclick="document.cadastrarItemFaqForm.submit();">Editar</button>
						<button type="button" class="btn btn-default"
							onclick="javascript:window.location.href='<c:url value="/admin/itemFaq/listItemFaq"/>'">Cancelar</button>
					</div>
				</div>
				<sf:hidden path="id"/>

			</sf:form>
		</div>
	</div>

	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
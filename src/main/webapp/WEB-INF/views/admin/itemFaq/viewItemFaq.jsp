<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>

<html lang="pt-br">
<head>
<jsp:include page="../../includes/header.jsp" />


</head>
<body>
	<jsp:include page="../../includes/navbarcomlogin.jsp" />
	<div class="container">
		<div class="form-group" style="padding-left: 100px; width: 500px;">
			<div class="media">
				<div class="media-body"></div>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-10">
				<h3>${requestScope.itemFaq.dsTituloItem}</h3>
			</div>
		</div>

		<div class="form-group">

		</div>
		<div class="form-group">

			<div class="col-sm-5">
				<label for="txItem" class="col-sm-3 control-label">
					Resposta:</label> <br> <small>${requestScope.itemFaq.txItem}</small>
			</div>
		</div>
		<br /> <br />
		<div class="form-group">
			<div class="col-sm-offset-1 col-sm-10">
				<button type="button" class="btn btn-default"
					onclick="javascript:window.location.href='<c:url value="/admin/itemFaq/listItemFaq"/>'">Voltar</button>
			</div>
		</div>
	</div>


	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
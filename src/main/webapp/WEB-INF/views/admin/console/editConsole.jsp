<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
</head>
<body>
	<jsp:include page="../../includes/navbarcomlogin.jsp" />
	<div class="container">
		<h3>Editar Console</h3>
		<br />
		<div>
			<sf:form action="editarConsole" commandName="cadastrarConsoleForm"
				method="post" class="form-horizontal" enctype="multipart/form-data">
				<spring:hasBindErrors htmlEscape="true" name="cadastrarConsoleForm">
					<c:if test="${errors.errorCount gt 0}">
						<div class="alert alert-info">
							<sf:errors path="*" />
						</div>
					</c:if>
				</spring:hasBindErrors>
				<div class="form-group">
					<label for="nmConsole" class="col-sm-2 control-label">Nome
						do console</label>
					<div class="col-sm-10">
						<sf:input path="nmConsole" class="form-control" id="nmConsole"
							placeholder="Informe o nome do jogo" style="width: 300px;"
							tabindex="1" htmlEscape="true" maxlength="255"
							title="Seu Console, Ex: Meu Console" />
					</div>
				</div>
				<div class="form-group">
					<label for="tipoConsole" class="col-sm-2 control-label">Tipo
						de Console</label>
					<div class="col-sm-10">
						<sf:select path="tipoConsole"
							class="selectpicker  show-menu-arrow" id="tipoConsole"
							placeholder="Selecio um tipo de Console" tabindex="3">

							<sf:option value="">&nbsp;</sf:option>
							<sf:options items="${tipoConsoleList}"  itemLabel="nmConsole" />
						</sf:select>
					</div>
				</div>
				<div class="form-group">
					<label for="tipoRede" class="col-sm-2 control-label">Tipo
						de Acesso a Rede</label>
					<div class="col-sm-10">
						<sf:select path="tipoRede" class="selectpicker  show-menu-arrow"
							id="tipoRede" placeholder="Selecio um tipo de Rede de Acesso"
							tabindex="3">

							<sf:option value="">&nbsp;</sf:option>
							<sf:options items="${tipoAcessoRedeList}" itemLabel="tipoAcesso"/>
						</sf:select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-default"
							onclick="document.cadastrarConsoleForm.submit();">Salvar</button>
						<button type="button" class="btn btn-default"
							onclick="javascript:window.location.href='<c:url value="/admin/console/listConsole"/>'">Cancelar</button>
					</div>
				</div>
					<sf:hidden path="id"/>
			</sf:form>
		</div>
	</div>

	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
</head>
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />

	<div class="container">

		<div class="form-group" style="padding-left: 100px; width: 500px;">
			<div class="media">

				<div class="media-body"></div>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-10">
				<h3>${requestScope.console.nmConsole}</h3>
			</div>
		</div>

		<div class="form-group">
		
			<div class="col-sm-10">
			<label for="txItem" class="col-sm-2 control-label">Tipo de Console: </label>
				<small>${requestScope.console.tipoConsole.nmConsole}</small>
			</div>
		</div>
		<div class="form-group">
	
			<div class="col-sm-10">
				<label for="txItem" class="col-sm-2 control-label">Tipo de Rede :</label>
				<small>${requestScope.console.tipoRede.tipoAcesso}</small>
			</div>
		</div>
		
		<div class="form-group">
		
		<c:if test="${not empty requestScope.console.listaJogos}">
					<table class="table">
						<thead>
							<tr style="background-color: #f0f0f0">
								<td width="5%"><span class="glyphicon glyphicon-ok-circle"></span></td>
								<td width="25%"><strong>Nome do Jogo</strong></td>
								<td width="50%"><strong>Tipo do Jogo</strong></td>
							</tr>
						</thead>

						<tbody>
							<c:forEach var="jogo" items="${requestScope.console.listaJogos}"
								varStatus="statsVar">
								<tr>
								<td width="5%"><strong>&nbsp;</strong></td>
									<td width="25%">${jogo.nmJogo}</td>
									<td width="50%">${jogo.tipoJogo.tipoJogo}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
		
		</div>
		
		
		
		
		
		
		
		
		
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">


				<button type="button" class="btn btn-default"
					onclick="javascript:window.location.href='<c:url value="/admin/console/listConsole"/>'">Voltar</button>
			</div>
		</div>
	</div>


	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
</head>
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />
	<script>
		function cadastrar() {
			formAcao.action = '<c:url value="/admin/console/createConsole"/>';
			formAcao.submit();
		}

		function editar() {
			formAcao.action = '<c:url value="/admin/console/editConsole"/>';
			formAcao.submit();
		}

		function excluir() {
			formAcao.action = '<c:url value="/admin/console/deleteConsole"/>';
			formAcao.submit();
		}

		function verConsole(idConsole) {
			formAcao.action = '<c:url value="/admin/console/viewConsole"/>'
					+ '/' + idConsole;
			formAcao.submit();
		}
	</script>
	<div class="container">
		<form name="formAcao" action="" method="get">

			<h2>Consoles Cadastrados</h2>

			<div class="panel panel-default">

				<div class="panel-heading">Consoles Cadastrados</div>

				<c:if test="${empty requestScope.listaConsole}">

					<div class="alert alert-warning">Não existem consoles
						cadastrados</div>
				</c:if>

				<c:if test="${not empty requestScope.listaConsole}">

					<table class="table">
						<thead>
							<tr style="background-color: #f0f0f0">
								<td width="5%"><span class="glyphicon glyphicon-ok-circle"></span></td>
								<td width="50%"><strong>Nome do Console</strong></td>
								<td width="15%"><strong>Tipo</strong></td>
								<td width="10%"><strong>&nbsp;</strong></td>
							</tr>
						</thead>

						<tbody>
							<c:forEach var="console" items="${requestScope.listaConsole}"
								varStatus="statsVar">
								<tr>
									<td><input id="idConsoleSelecionado"
										name="idConsoleSelecionado" type="radio" value="${console.idConsole}"
									<c:if test="${statsVar.index == 0}"> checked="checked" </c:if> /></td>
										
									<td>${console.nomeConsole}</td>
									<td>${console.tipoConsole.nmConsole}</td>
										 <td><button type="button" class="btn btn-primary btn-xs"
											onclick="verConsole('${console.idConsole}');">Ver
											Console</button></td> 
								</tr>
							</c:forEach>
						</tbody>
					</table>

				</c:if>
			</div>
			<div class="form-group">
				<div align="center">
					<button type="button" class="btn btn-primary"
						onclick="cadastrar();">Cadastrar Console</button>
					<button
						<c:if test="${empty requestScope.listaConsole}"> disabled="disabled" </c:if>
						type="button" class="btn btn-info" onclick="editar();">Editar
						Console</button>
					<button
						<c:if test="${empty requestScope.listaConsole}"> disabled="disabled" </c:if>
						class="btn btn-danger" type="button" data-toggle="modal"
						data-target="#confirmExcluir" data-title="Excluir Jogo"
						data-message="Tem certeza que deseja excluir este console?">
						Excluir Console</button>
				</div>
			</div>
		</form>
	</div>
<div class="modal fade" id="confirmExcluir" role="dialog"
		aria-labelledby="confirmDesativarLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Excluir Console</h4>
				</div>
				<div class="modal-body">
					<p>Deseja realmente excluir este Console?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="button" class="btn btn-danger" id="confirm"
						onclick="excluir();">Sim, Excluir</button>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="pt-br">

<head>
<jsp:include page="../../includes/header.jsp" />
<script src="//code.jquery.com/jquery-1.11.0.js"></script>
<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>

<script>
	$(function() {

		$("#accordion").accordion({
			collapsible : true,
			heightStyle : "auto"
			
		});

	});
</script>
</head>
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />

	<div class="container">

		<div class="form-group" style="padding-left: 100px; width: 500px;">
			<div class="media">
				<div class="media-body"></div>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-10">
				<h3>${requestScope.faq.nmFaq}</h3>
			</div>
		</div>

		<div class="form-group">

			<div class="col-sm-5">
				<label for="txItem" class="col-sm-5 control-label">Data de
					Atulaização:</label> <small> <fmt:formatDate
						pattern="dd/MM/yyyy HH:mm"
						value="${requestScope.faq.dtAtualizacao}" /></small>
			</div>
			<div class="col-sm-offset-4 col-sm-1">
				<button type="button" class="btn btn-default"
					onclick="javascript:window.location.href='<c:url value="/admin/faq/listFaq"/>'">Voltar</button>
			</div>

		</div>




		<div class="form-group">

			<div id="accordion" class="col-sm-offset-5 col-sm-10">

				<c:forEach var="itemFaq" items="${requestScope.faq.listaItemFAQ}"
					varStatus="statsVar">

					<h3>${itemFaq.dsTituloItem}</h3>
					<div>
						<p>${itemFaq.txItem}</p>
					</div>

				</c:forEach>


			</div>
		</div>
	</div>


	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="pt-br">

<jsp:include page="includes/header.jsp" />
</head>

<body>

	<!-- Fixed navbar -->
	<jsp:include page="includes/navbarcomlogin.jsp" />

	<!-- Begin page content -->
	<div class="container">
		<div class="page-header">
			<h1>
				Encontre adversários <small>Descruba seu talento</small>
			</h1>
		</div>

	</div>
	<jsp:include page="includes/footer.jsp" />

</body>
</html>

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">

<jsp:include page="includes/header.jsp" />
<script>
	jQuery(document).ready(function() {
		$("#cpf").mask("999.999.999-99");
	});
</script>
</head>

<body>

	<!-- Fixed navbar -->
	<jsp:include page="includes/navbarcomlogin.jsp" />

	<!-- Begin page content -->
	<div class="container">
		<div class="page-header">

			<h1>
				Crie sua Conta <small>Rápido e Grátis</small>
			</h1>
		</div>

		<c:if
			test="${requestScope.vsPlayers != null && requestScope.facebook == null}">

			<sf:form modelAttribute="cadastrarForm" action="cadastrar"
				class="form-horizontal">

				<spring:hasBindErrors htmlEscape="true" name="cadastrarForm">
					<c:if test="${errors.errorCount gt 0}">
						<div class="alert alert-info">
							<sf:errors path="*" />
						</div>
					</c:if>
				</spring:hasBindErrors>

				<div class="form-group">
					<label for="nomeCompleto" class="col-sm-2 control-label">Nome
						Completo</label>
					<div class="col-sm-10">
						<sf:input path="nomeUser" class="form-control" id="nomeCompleto"
							placeholder="Informe seu nome" style="width: 300px;" tabindex="1"
							htmlEscape="true" maxlength="255" title="Nome Completo" />
					</div>
				</div>

				<div class="form-group">
					<label for="email" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-10">
						<sf:input path="emailUser" class="form-control" id="email"
							placeholder="Informe seu e-mail" style="width: 300px;"
							htmlEscape="true" />
					</div>
				</div>
				<div class="form-group">
					<label for="senha" class="col-sm-2 control-label">Senha</label>
					<div class="col-sm-10">

						<sf:password path="senha" class="form-control" id="senha"
							placeholder="Sua senha" style="width: 220px;" htmlEscape="true" />
					</div>
				</div>

				<div class="form-group">
					<label for="cpf" class="col-sm-2 control-label">CPF</label>
					<div class="col-sm-10">
						<sf:input path="cpf" class="form-control" id="cpf"
							placeholder="000.000.000-00" style="width: 220px;" />
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-default">Cadastrar</button>
					</div>
				</div>

				<%-- 			<c:if test="${requestScope.vsPlayers == null}"> --%>
				<%-- 	<p>
					<label for="email">E-mail: </label> <input type="text" id="email"
						name="email" />
				</p>
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				<input type="hidden" name="nome"
					value="<c:out value="${requestScope.nomeUsuarioFacebook}"/>" />
				<button type="submit" class="btn">Cadastrar com Facebook</button> --%>
				<%-- </c:if> --%>


				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-9">
						<a href="<c:url value="/auth/facebook"/>"><img
							src="<c:url value="/resources/images/social/conectar_com_facebook.png"/>"></a>
					</div>
				</div>


			</sf:form>

		</c:if>

		<c:if
			test="${requestScope.facebook != null && requestScope.vsPlayers == null}">

			<sf:form modelAttribute="cadastrarFacebookForm" action="signup"
				class="form-horizontal">

				<spring:hasBindErrors htmlEscape="true" name="cadastrarFacebookForm">
					<c:if test="${errors.errorCount gt 0}">
						<div class="alert alert-info">
							<sf:errors path="*" />
						</div>
					</c:if>
				</spring:hasBindErrors>

				<div class="form-group">
					<label for="nomeCompleto" class="col-sm-2 control-label">Nome
						Completo</label>
					<div class="col-sm-10">
						<sf:input path="nomeUser" class="form-control" id="nomeCompleto"
							placeholder="Informe seu nome" style="width: 300px;" tabindex="1"
							htmlEscape="true" maxlength="255" title="Nome Completo" />
					</div>
				</div>

				<div class="form-group">
					<label for="email" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-10">
						<sf:input path="emailUser" class="form-control" id="email"
							placeholder="Informe seu e-mail" style="width: 300px;"
							htmlEscape="true" />
					</div>
				</div>

				<div class="form-group">
					<label for="cpf" class="col-sm-2 control-label">CPF</label>
					<div class="col-sm-10">
						<sf:input path="cpf" class="form-control" id="cpf"
							placeholder="000.000.000-00" style="width: 220px;"
							data-mask="000.000.000-00" data-mask-reverse="true" />
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-default">Cadastrar</button>
					</div>
				</div>

			</sf:form>
		</c:if>

	</div>

	<jsp:include page="includes/footer.jsp" />

</body>
</html>
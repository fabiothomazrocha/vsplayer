<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<a href="<c:url value="/" />"><img
				src="<c:url value="/resources/images/logo.png"/>"
				style="margin-top: 6px;" /></a>
		</div>
		<c:if test="${pageContext.request.userPrincipal.name == null}">

			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="<c:url value="/cadastrar"/>"
						title="Efetue seu cadastro, é grátis!">Cadastre-se!</a></li>
					<li><a href="<c:url value="/login"/>"
						title="Entrar no sistema">Login</a></li>

					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown">Ajuda<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="<c:url value="/faqPublico"/>">FAQ -
									Perguntas Frequentes</a></li>
							<li class="divider"></li>
							<li><a href="#">Sobre o VsPlayers</a></li>
							<li><a href="#">Política de Privacidade</a></li>
						</ul></li>
				</ul>
			</div>
		</c:if>
		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#" title="Minha Conta">${pageContext.request.userPrincipal.name}</a></li>
					<li><a href="<c:url value="/logout" />"
						title="Sair do Sistema"> Logout</a></li>
				</ul>
			</div>
		</c:if>
	</div>
</div>
<div class="container" style="height: 30px;">
	<c:if test="${pageContext.request.userPrincipal.name != null}">
		<ul class="nav nav-pills">
			<li><a href="<c:url value="/restricted/welcome"/>">Página
					Inicial</a></li>
			<li><a href="<c:url value="/restricted/minhaConta/viewConta"/>">Minha
					Conta</a></li>
			<li><a href="<c:url value="/restricted/perfil/listPerfil"/>">Meus
					Perfis</a></li>


			<li><a
				href="<c:url value="/restricted/acessoRedeJogo/listAcessoRedeJogo"/>">Acesso
					Rede</a></li>

			<li class="dropdown"><a class="dropdown-toggle"
				data-toggle="dropdown" href="#"> Financeiro <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a
						href="<c:url value="/restricted/financeiro/listFinanceiro"/>">Histórico
							de Transações</a></li>

					<li><a
						href="<c:url value="/restricted/financeiro/solicitarSaque"/>">Retirada
							/ Solicitar Saque</a></li>

					<li><a
						href="<c:url value="/restricted/financeiro/novoDeposito"/>">Efetuar
							Depósito</a></li>
					<li><a
						href="<c:url value="/restricted/financeiro/relatFinanceiro"/>">Relatório
							Financeiro</a></li>

				</ul></li>

			<li class="dropdown"><a class="dropdown-toggle"
				data-toggle="dropdown" href="#"> Encontrar Adversários <span
					class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a
						href="<c:url value="/restricted/desafio/desafioSalaJogos"/>">Desafio
							por Salas de Jogos</a></li>
				</ul></li>

			<li><a href="<c:url value="/admin/salaJogos/listSalaJogos"/>">Sala
					de Jogos</a></li>
			<li><a href="<c:url value="/admin/regras/listRegra"/>">Regras</a></li>
			<li><a href="<c:url value="/admin/faq/listFaq"/>">FAQ</a></li>
			<li><a href="<c:url value="/admin/itemFaq/listItemFaq"/>">ItemFAQ</a></li>
		</ul>

		<sec:authorize access="hasRole('ADMINISTRADOR')">
			<ul class="nav nav-pills">
				<li><a href="<c:url value="/admin/pais/listPais"/>">País</a></li>
				<li><a href="<c:url value="/admin/console/listConsole"/>">Consoles</a></li>
				<li><a href="<c:url value="/admin/jogos/listJogos"/>">Jogos</a></li>


				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#"> Financeiro <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a
							href="<c:url value="/admin/financeiro/listarTransacoesAprovar"/>">Solicitações
								de Saque</a></li>
						<li><a
							href="<c:url value="/admin/financeiro/listarTransacoesAprovadas"/>">Transações
								Aprovadas</a></li>

					</ul></li>

				<li><a
					href="<c:url value="/admin/financeiro/listarTransacoesAprovar"/>">Solicitações
						de Saque</a></li>

			</ul>
		</sec:authorize>
	</c:if>
</div>
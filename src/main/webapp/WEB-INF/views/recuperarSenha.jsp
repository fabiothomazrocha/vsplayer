<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="pt-br">

<jsp:include page="includes/header.jsp" />
</head>

<body>

	<!-- Fixed navbar -->
	<jsp:include page="includes/navbarcomlogin.jsp" />

	<!-- Begin page content -->
	<div class="container">

		<div class="page-header">
			<h1>Recuperar Senha</h1>
		</div>

		<c:if test="${requestScope.mensagem != null}">
			<div class="alert alert-info">
				<c:out value="${requestScope.mensagem}"></c:out>
			</div>
		</c:if>

		<p class="lead">Sua senha pode ser recuperada a qualquer momento,
			apenas informe seu e-mail e você receberá um e-mail para redefinir
			sua senha.</p>


		<form action="<c:url value="/getPassword"/>" method="post"
			class="form-horizontal" role="form">

			<div class="form-group">
				<label for="email" class="col-sm-2 control-label">Informe
					seu E-mail</label>
				<div class="col-sm-10">
					<input type="email" id="email" name="email"
						value="<c:out
						value="${requestScope.emailResend}"/>"
						placeholder="vsplayers@vsplayers.com.br" class="form-control"
						style="width: 350px;" />
				</div>
			</div>


			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">Recuperar
						Senha</button>
				</div>
			</div>

			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>

	</div>
	<jsp:include page="includes/footer.jsp" />

</body>
</html>
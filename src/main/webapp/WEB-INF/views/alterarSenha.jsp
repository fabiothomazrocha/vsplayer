<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">

<jsp:include page="includes/header.jsp" />
</head>

<body>

	<!-- Fixed navbar -->
	<jsp:include page="includes/navbarcomlogin.jsp" />

	<!-- Begin page content -->
	<div class="container">
		<div class="page-header">

			<h1>Alterar Senha</h1>
		</div>

		<sf:form modelAttribute="alterarSenhaForm"
			action="${pageContext.request.contextPath}/alteraSenha" method="POST"
			class="form-horizontal">

			<spring:hasBindErrors htmlEscape="true" name="alterarSenhaForm">
				<c:if test="${errors.errorCount gt 0}">
					<div class="alert alert-info">
						<sf:errors path="*" />
					</div>
				</c:if>
			</spring:hasBindErrors>

			<div class="form-group">
				<label for="novaSenha" class="col-sm-2 control-label">Nova
					senha</label>
				<div class="col-sm-10">
					<sf:password path="novaSenha" class="form-control" id="novaSenha"
						placeholder="Ex: !#@Joao123" style="width: 200px;" tabindex="1"
						htmlEscape="true" maxlength="255" title="Informe sua nova senha" />
				</div>
			</div>

			<div class="form-group">
				<label for="repitaNovaSenha" class="col-sm-2 control-label">Repita
					sua senha</label>
				<div class="col-sm-10">
					<sf:password path="repitaNovaSenha" class="form-control"
						id="repitaNovaSenha" placeholder="Ex: !#@Joao123"
						style="width: 200px;" tabindex="2" htmlEscape="true"
						maxlength="255" title="Repita sua nova senha" />
				</div>
			</div>

			<sf:hidden path="codigoAlterarSenha" id="codigoAlterarSenha" />

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">Alterar
						Senha</button>
				</div>
			</div>

		</sf:form>

	</div>

	<jsp:include page="includes/footer.jsp" />

</body>
</html>
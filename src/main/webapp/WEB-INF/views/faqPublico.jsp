<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<jsp:include page="includes/header.jsp" />
<script src="//code.jquery.com/jquery-1.11.0.js"></script>
<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<style>
#feedback {
	font-size: 1.4em;
}

#selectable .ui-selecting {
	background: #DDD;
}

#selectable .ui-selected {
	background: #333;
	color: white;
}

#selectable {
	list-style-type: none;
	margin: 0;
	padding: 0;
	width: 100%;
}

#selectable li {
	margin: 3px;
	padding: 0.4em;
	font-size: 1.4em;
	height: 50px;
}
</style>
<script>
	$(function() {

		var $a = jQuery.noConflict()
		var id;
		$a("#selectable").selectable({
			selected : function(event, ui) {
				console.info(ui.selected);
				id = $("li.ui-selected").text();
				verFaq(id);
			}
		});

		$("#accordion").accordion({
			collapsible : true,
			heightStyle : "auto"

		});

	});
	function verFaq(nome) {
		formAcao.action = '<c:url value="/faqPublico"/>' + '/' + nome;
		formAcao.submit();
	}
</script>
</head>
<body>

	<jsp:include page="includes/navbarcomlogin.jsp" />
	<div class="container">
		<form name="formAcao" action="" method="get">
			<h2>FAQs</h2>
			<div class="panel-heading">Perguntas Frequêntes</div>
			<div class="form-group col-sm-4">
				<ol id="selectable">
					<c:forEach var="faq" items="${requestScope.listaFaq}"
						varStatus="statsVar">
						<li class="ui-widget-content selectable li">${faq.nmFaq}</li>
					</c:forEach>
				</ol>
			</div>

			<div class="form-group col-sm-6">
				<div id="accordion" class="">
					<c:forEach var="itemFaq" items="${requestScope.faq.listaItemFAQ}"
						varStatus="statsVar">
						<h3>${itemFaq.dsTituloItem}</h3>
						<div>
							<p>${itemFaq.txItem}</p>
						</div>
					</c:forEach>
				</div>
			</div>

		</form>
	</div>


	<jsp:include page="includes/footer.jsp" />
</body>
</html>
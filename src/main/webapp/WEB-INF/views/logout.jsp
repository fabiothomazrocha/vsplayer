<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="pt-br">
</head>
<body>

	<jsp:include page="includes/header.jsp" />

	<h1>Logout page</h1>

	<div id="login-box"></div>

	<jsp:include page="includes/footer.jsp" />
</body>
</html>
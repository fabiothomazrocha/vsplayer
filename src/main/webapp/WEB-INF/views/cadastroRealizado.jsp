<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="includes/header.jsp" />
</head>
<body>

	<jsp:include page="includes/navbarcomlogin.jsp" />

	<div class="container">


		<c:if test="${param.resent != null && param.resent == 'true'}">
			<div class="alert alert-info">Seu código de ativação foi
				re-enviado.</div>
		</c:if>

		<c:if test="${param.resent != null && param.resent == 'false'}">
			<div class="alert alert-warning">Ocorreu um erro e seu código
				de ativação não pode ser enviado.</div>
		</c:if>

		<div class="page-header">
			<h1>
				Quase lá
				<c:out value="${requestScope.nomeUsuario}" />
				!&nbsp;<small>Verifique seu e-mail e realize sua ativação</small>
			</h1>
		</div>

		<br />

		<form action="<c:url value="/gerarNovoCodigo"/>" method="post"
			class="form-horizontal" role="form">
			<input type="hidden" id="email" name="email"
				value="<c:out
						value="${requestScope.emailResend}"/>" />
			<button type="submit" class="btn btn-default">Não recebi o
				e-mail, enviar novamente!</button>

			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>

	</div>

	<jsp:include page="includes/footer.jsp" />

</body>
</html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="includes/header.jsp" />
</head>
<body>

	<jsp:include page="includes/navbarcomlogin.jsp" />

	<div class="container">


		<c:if test="${param.resent != null && param.resent == 'true'}">
			<div class="alert alert-info">Seu código de ativação foi
				re-enviado.</div>
		</c:if>

		<c:if test="${param.resent != null && param.resent == 'false'}">
			<div class="alert alert-warning">Ocorreu um erro e seu código
				de ativação não pode ser enviado.</div>
		</c:if>

		<div class="page-header">
			<p class="lead">Para receber seu código de ativação novamente,
				informe seu e-mail</p>
		</div>

		<br />

		<form action="<c:url value="/getCodigoAtivacao"/>" method="post"
			class="form-horizontal" role="form">
			<div class="form-group">
				<label for="email" class="col-sm-2 control-label">E-mail de
					cadastro</label>
				<div class="col-sm-10">
					<input type="email" id="email" name="email" class="form-control"
						style="width: 300px;" tabindex="1"
						placeholder="Ex: joao@yahoo.com.br" />
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">Enviar
						código</button>
				</div>
			</div>

			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />

		</form>

	</div>

	<jsp:include page="includes/footer.jsp" />

</body>
</html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
</head>
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />

	<script>
		function cadastrar() {
			formAcao.action = '<c:url value="/restricted/perfil/createPerfil"/>';
			formAcao.submit();
		}

		function editar() {
			formAcao.action = '<c:url value="/restricted/perfil/editPerfil"/>';
			formAcao.submit();
		}

		function excluir() {
			formAcao.action = '<c:url value="/restricted/perfil/deletePerfil"/>';
			formAcao.submit();
		}

		function verPerfil(idPerfil) {
			formAcao.action = '<c:url value="/restricted/perfil/viewPerfil"/>'
					+ '/' + idPerfil;
			formAcao.submit();
		}
	</script>

	<div class="container">
		<form name="formAcao" action="" method="get">
			<h2>Meus Perfis</h2>

			<div class="panel panel-default">

				<div class="panel-heading">Perfis Ativos</div>

				<c:if test="${empty requestScope.listaPerfis}">

					<div class="alert alert-warning">Você ainda não possui nenhum
						perfil cadastrado.</div>
				</c:if>

				<c:if test="${not empty requestScope.listaPerfis}">

					<table class="table">
						<thead>
							<tr style="background-color: #f0f0f0">
								<td width="5%"><span class="glyphicon glyphicon-ok-circle"></span></td>
								<td width="50%"><strong>Apelido</strong></td>
								<td width="15%"><strong>Data Criação</strong></td>
								<td width="15%"><strong>Data Atualização</strong></td>
								<td width="10%"><strong>&nbsp;</strong></td>
							</tr>
						</thead>

						<tbody>
							<c:forEach var="perfil" items="${requestScope.listaPerfis}"
								varStatus="statsVar">
								<tr>
									<td><input id="idPerfilSelecionado"
										name="idPerfilSelecionado" type="radio"
										value="${perfil.idPerfilUsuario}"
										<c:if test="${statsVar.index == 0}"> checked="checked" </c:if> /></td>
									<td>${perfil.apelido}</td>
									<td>${perfil.dtCriacaoFormatada}</td>
									<td>${perfil.dtAtualizacaoFormatada}</td>
									<td><button type="button" class="btn btn-primary btn-xs"
											onclick="verPerfil('${perfil.idPerfilUsuario}');">Ver
											Perfil</button></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>

				</c:if>
			</div>


			<div class="form-group">
				<div align="center">
					<button type="button" class="btn btn-primary"
						onclick="cadastrar();">Cadastrar Novo Perfil</button>
					<button <c:if test="${empty requestScope.listaPerfis}"> disabled="disabled" </c:if> type="button" class="btn btn-info" onclick="editar();">Editar</button>
					<button <c:if test="${empty requestScope.listaPerfis}"> disabled="disabled" </c:if> class="btn btn-danger" type="button" data-toggle="modal"
						data-target="#confirmDesativar" data-title="Desativar Perfil"
						data-message="Tem certeza que deseja desativar este perfil? ?">
						Desativar</button>
				</div>
			</div>
		</form>
	</div>

	<!-- Modal Dialog -->

	<div class="modal fade" id="confirmDesativar" role="dialog"
		aria-labelledby="confirmDesativarLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Desativar Perfil</h4>
				</div>
				<div class="modal-body">
					<p>Deseja realmente desativar este perfil?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="button" class="btn btn-danger" id="confirm"
						onclick="excluir();">Sim, Desativar</button>
				</div>
			</div>
		</div>
	</div>


	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
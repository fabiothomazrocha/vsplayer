<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />
	<div class="container">

		<h3>Editar Perfil</h3>
		<br />
		<div>

			<sf:form action="editarPerfil" commandName="cadastrarPerfilForm"
				method="post" class="form-horizontal" enctype="multipart/form-data">

				<spring:hasBindErrors htmlEscape="true" name="cadastrarPerfilForm">
					<c:if test="${errors.errorCount gt 0}">
						<div class="alert alert-info">
							<sf:errors path="*" />
						</div>
					</c:if>
				</spring:hasBindErrors>

				<div class="form-group" style="padding-left: 100px; width: 500px;">
					<div class="media">
						<a class="pull-left" href="#"><img
							src="<c:url value="/restricted/perfil/getSessionImage"/>"
							width="100" height="100" class="media-object" /> </a>
						<div class="media-body">
							<sf:input path="fotoPerfil" style="padding-left: 15px;"
								id="fotoUpload" tabindex="1" title="Sua foto ou uma clipart"
								type="file" />
							<p style="padding-left: 20px;" align="justify">
								<small>Selecione uma imagem para seu perfil, esta imagem
									deve estar no formato PNG ou JPG, ser preferencialmente do
									tamanho 100x100 pixels e ter no máximo 200kb</small></span><br/>
									<sf:checkbox id="removerImg" path="removerImagem"/>
									<label for="removerImg">Remover Imagem</label>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="apelido" class="col-sm-2 control-label">Apelido</label>
					<div class="col-sm-10">
						<sf:input path="apelido" class="form-control" id="apelido"
							placeholder="Informe seu Apelido" style="width: 300px;"
							tabindex="2" htmlEscape="true" maxlength="255"
							title="Seu apelido, Ex: Junior1983" />
					</div>
				</div>

				<div class="form-group">
					<label for="sobre" class="col-sm-2 control-label">Sobre
						Você</label>
					<div class="col-sm-10">
						<sf:textarea class="form-control" path="sobre" id="sobre" rows="3"
							placeholder="Fale um pouco sobre você" style="width: 300px;"
							tabindex="3" htmlEscape="true" maxlength="400" />
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-default">Editar</button>
												<button type="button" class="btn btn-default" onclick="javascript:window.location.href='<c:url value="/restricted/perfil/listPerfil"/>'">Cancelar</button>
					</div>
				</div>
				
				<sf:hidden path="idPerfilEdicao"/>

			</sf:form>

		</div>
	</div>
	<jsp:include page="../../includes/footer.jsp" />

</body>
</html>
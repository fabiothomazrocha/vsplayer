<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<jsp:include page="../../includes/header.jsp" />
<script src="//code.jquery.com/jquery-1.11.0.js"></script>
<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<style>
#feedback {
	font-size: 1.4em;
}

#selectable .ui-selecting {
	background: #DDD;
}

#selectable .ui-selected {
	background: #333;
	color: white;
}

#selectable {
	list-style-type: none;
	margin: 0;
	padding: 0;
	width: 100%;
}

#selectable li {
	margin: 3px;
	padding: 0.4em;
	font-size: 1.4em;
	height: auto;
	width: 700px;
}
</style>
<script>
	$(function() {
		var $a = jQuery.noConflict()
		$a("#selectable").selectable({
			selected : function(event, ui) {
				console.info(ui.selected);
			}
		});
	});
</script>
</head>
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />
	<div class="container">
		<form name="formAcao" action="" method="get">
			<h2>Relatorio Finaceiro</h2>
			<div class="panel-heading">
				<div class="form-group">
					<div class="col-sm-10">
						<h3>${requestScope.nomeUsuario}</h3>
					</div>
					<div class="col-sm-10">
						<h1>Saldo Atual: ${requestScope.saldoAtual}</h1>
					</div>
				</div>


				<div class="form-group col-sm-4">
					<ol id="selectable">
						<c:forEach var="trx" items="${requestScope.listTrx}"
							varStatus="statsVar">
							<li class=" list-group list-group-item">
								<p class="list-group-item active">${trx.nmTransacao}</p> <a
								href="#" class="list-group-item">
									<p class="list-group-item-text">
										Data da Transação: <span class="badge">${trx.dataTransacao}</span>
									</p>
									<p class="list-group-item-text">
										Aprovado: <span class="badge">${trx.aprovada.descricao}</span>
									</p>
									<p class="list-group-item-text">
										Data de Aprovação: <span class="badge">${trx.dataAprovacao}</span>
									</p>
									<p class="list-group-item-text">
										Valor: <span class="badge">${trx.valorFormatado}</span>
									</p>
							</a>
							</li>
						</c:forEach>
					</ol>
				</div>
			</div>

		</form>

	</div>


	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
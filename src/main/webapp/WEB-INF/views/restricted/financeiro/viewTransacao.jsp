<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
</head>
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />

	<div class="container">
		<div class="form-group" style="padding-left: 100px; width: 500px;">
			<div class="media">
				<div class="media-body"></div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10">
				<h3>${requestScope.transacaoFinanceira.nmTransacao}</h3>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-10">
				<label for="txItem" class="col-sm-2 control-label">Data
					Transação: </label> <small><fmt:formatDate
						pattern="dd/MM/yyyy HH:mm"
						value="${requestScope.transacaoFinanceira.dtTransacao}" /> </small>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10">
				<label for="txItem" class="col-sm-2 control-label">Data
					Aprovação:</label> <small><fmt:formatDate
						pattern="dd/MM/yyyy HH:mm"
						value="${requestScope.transacaoFinanceira.dtAprovacao}" /></small>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10">
				<label for="txItem" class="col-sm-2 control-label">Valor
					(R$):</label> <small>${requestScope.transacaoFinanceira.valorTransacao}</small>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-10">
				<label for="txItem" class="col-sm-2 control-label">Aprovada:</label>
				<small>${requestScope.transacaoFinanceira.aprovada.descricao}</small>
			</div>
		</div>
		<div class="form-group"></div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="button" class="btn btn-default"
					onclick=" window.history.back()">Voltar</button>
			</div>
		</div>
	</div>


	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
<script>
	jQuery(document).ready(function() {
		$("#valor").maskMoney({
			showSymbol : true,
			symbol : "R$",
			decimal : ",",
			thousands : ".",
			allowZero : true
		});
	});

	jQuery(document).ready(function() {
		$("#cpf").mask("999.999.999-99");
	});

	function verificarSaldo() {
		jQuery.ajax({
			type : "GET",
			dataType : "json",
			url : "<c:url value="/restricted/financeiro/rest/getSaldo"/>",
			success : function(data) {
				$("#dialogSaldo").modal('show');
				$("#saldospan").text('Seu saldo é: ' + data.saldo);
			}
		});
	}

	function fecharModalSaldo() {
		$("#dialogSaldo").modal('hide');
	}
</script>
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />
	<div class="container">

		<sf:form name="formAcao" action="efetuarSolicitacao"
			commandName="solicitarSaqueForm" method="post"
			class="form-horizontal">
			<br/><br/>

			<p class="lead">Nesta página você pode solicitar o saque de seu
				saldo e o valor será efetivado em 24 horas em sua conta</p>

			<spring:hasBindErrors htmlEscape="true" name="solicitarSaqueForm">
				<c:if test="${errors.errorCount gt 0}">
					<div class="alert alert-info">
						<sf:errors path="*" />
					</div>
				</c:if>
			</spring:hasBindErrors>


			<div class="form-group">
				<label for="nomeBanco" class="col-sm-2 control-label">Seu
					Banco: </label>
				<div class="col-sm-10">
					<sf:select path="nomeBanco" class="selectpicker" id="banco"
						style="width: 300px;" tabindex="1" htmlEscape="true"
						maxlength="255">
						<option value="">Selecione o Banco</option>
						<option value="Banco do Brasil">Banco do Brasil</option>
						<option value="Itau">Itaú Unibanco</option>
						<option value="Santander">Santander</option>
						<option value="HSBC">HSBC</option>
						<option value="Bradesco">Bradesco</option>
						<option value="Caixa">Caixa</option>
					</sf:select>
				</div>
			</div>

			<div class="form-group">
				<label for="nomeTitular" class="col-sm-2 control-label">Nome
					Titular</label>
				<div class="col-sm-10">
					<sf:input path="nomeTitular" class="form-control" id="nomeTitular"
						placeholder="Informe o nome do Titular da Conta"
						style="width: 300px;" tabindex="1" htmlEscape="true"
						maxlength="255" title="Informe o nome do Titular da Conta" />
				</div>
			</div>


			<div class="form-group">
				<label for="agencia" class="col-sm-2 control-label">Agência</label>
				<div class="col-sm-10">
					<sf:input path="agencia" class="form-control" id="agencia"
						placeholder="Informe o número de sua Agência"
						style="width: 300px;" tabindex="1" htmlEscape="true"
						maxlength="255" title="Informe o número de sua agência" />
				</div>
			</div>

			<div class="form-group">
				<label for="contaCorrente" class="col-sm-2 control-label">Conta
					Corrente</label>
				<div class="col-sm-10">
					<sf:input path="numeroConta" class="form-control"
						id="contaCorrente"
						placeholder="Informe o número de sua Conta Corrente"
						style="width: 300px;" tabindex="1" htmlEscape="true"
						maxlength="255" title="Informe o número de sua Conta Corrente" />
				</div>
			</div>


			<div class="form-group">
				<label for="cpf" class="col-sm-2 control-label">CPF</label>
				<div class="col-sm-10">
					<sf:input path="cpf" class="form-control" id="cpf"
						placeholder="Informe o número de seu CPF"
						data-mask="000.000.000-00" data-mask-reverse="true"
						style="width: 300px;" tabindex="1" htmlEscape="true"
						maxlength="255" title="Informe o número de seu CPF" />
				</div>
			</div>

			<div class="form-group">
				<label for="cpf" class="col-sm-2 control-label">Valor</label>
				<table>
					<tr>
						<td><div class="col-sm-10">
								<sf:input path="valor" class="form-control" id="valor"
									placeholder="Informe o valor" style="width: 150px;"
									tabindex="1" htmlEscape="true" maxlength="255"
									title="Informe o valor para retirada" />
							</div></td>
						<td><button type="button" id="verificaSaldo"
								class="btn btn-primary btn-xs" onclick="verificarSaldo()">Verificar
								Saldo</button></td>
					</tr>
				</table>


			</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-primary">Solicitar Saque</button>
			<button type="button" class="btn btn-default"
				onclick="javascript:window.location.href='<c:url value="/restricted/financeiro/listFinanceiro"/>'">Cancelar</button>
		</div>
	</div>

	</sf:form>

	</div>
	</div>

	<div class="modal fade" id="dialogSaldo" role="dialog"
		aria-labelledby="dialogSaldo" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Saldo de sua Conta</h4>
				</div>
				<div class="modal-body">
					<p>
						<span id="saldospan"></span>
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" id="confirm"
						onclick="fecharModalSaldo();">Ok</button>
				</div>
			</div>
		</div>
	</div>

	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
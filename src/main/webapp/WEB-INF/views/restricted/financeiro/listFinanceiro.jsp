<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
</head>
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />

	<script>
		function novoDeposito() {
			window.location.href = '<c:url value="/restricted/financeiro/novoDeposito"/>';
		}
		function verTransacao(idTransacao) {
			formAcao.action = '<c:url value="/restricted/financeiro/viewTransacao"/>'
					+ '/' + idTransacao;
			formAcao.submit();
		}
	</script>

	<div class="container">
		<form name="formAcao" action="" method="get">
			<br /> <br />
			<div class="panel panel-default">

				<div class="panel-heading">Transações Financeiras</div>

				<c:if test="${empty requestScope.listaTrx}">
					<div class="alert alert-warning">Você ainda não possui
						nenhuma transação.</div>
				</c:if>

				<c:if test="${not empty requestScope.listaTrx}">

					<table class="table">
						<thead>
							<tr style="background-color: #f0f0f0">
								<td width="5%"><span class="glyphicon glyphicon-ok-circle"></span></td>
								<td width="35%"><strong>Nome Transação</strong></td>
								<td width="15%"><strong>Data Transação</strong></td>
								<td width="15%"><strong>Data Aprovação</strong></td>
								<td width="15%"><strong>Valor (R$)</strong></td>
								<td width="15%"><strong>Aprovada</strong></td>
								<td width="10%"><strong>&nbsp;</strong></td>
							</tr>
						</thead>

						<tbody>
							<c:forEach var="trx" items="${requestScope.listaTrx}"
								varStatus="statsVar">
								<tr>
									<td><input id="idTrxSelecionado" name="idTrxSelecionado"
										type="radio" value="${trx.idTransacao}"
										<c:if test="${statsVar.index == 0}"> checked="checked" </c:if> /></td>
									<td>${trx.nmTransacao}</td>
									<td>${trx.dataTransacao}</td>
									<td>${trx.dataAprovacao}</td>
									<td>${trx.valorFormatado}</td>
									<td>${trx.aprovada.descricao}</td>
									<td><button type="button" class="btn btn-primary btn-xs"
											onclick="verTransacao('${trx.idTransacao}');">Ver
											Transação</button></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>

				</c:if>
			</div>

			<h4>Saldo Atual: ${requestScope.saldoAtual}</h4>

			<!-- 
			<div class="form-group">
				<div align="center">
					<button type="button" class="btn btn-primary"
						onclick="novoDeposito();">Efetuar Novo Depósito</button>
				</div>
			</div>
 -->
		</form>
	</div>

	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
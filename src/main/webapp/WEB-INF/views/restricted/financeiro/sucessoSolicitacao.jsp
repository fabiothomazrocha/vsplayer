<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />
	<div class="container">

		<h4 style="color: #b0b0b0">Solicitacção Efetuada com Sucesso!</h4>

		<p class="lead">Sua solicitação foi realizada com sucesso. Você
			receberá um e-mail assim que atendida em um prazo máximo de 48 horas.</p>


	</div>
	</div>
	<jsp:include page="../../includes/footer.jsp" />

</body>
</html>
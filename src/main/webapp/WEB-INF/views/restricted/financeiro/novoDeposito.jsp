<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
<script>

   function efetuarSaque(valor) {
	   document.formAcao.valorSaque.value = valor;
	   document.formAcao.submit();
   }

</script>
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />
	<div class="container">

		<form action="<c:url value="/restricted/financeiro/makePayment"/>"
			method="post" name="formAcao" class="form-horizontal" role="form">
			<br/><br/>
			<p class="lead">Para comprar créditos, você será redirecionado ao
				PayPal através de uma conexão segura, responsável pela segurança de
				seus dados.</p>

			<spring:hasBindErrors htmlEscape="true" name="depositoForm">
				<c:if test="${errors.errorCount gt 0}">
					<div class="alert alert-info">
						<sf:errors path="*" />
					</div>
				</c:if>
			</spring:hasBindErrors>

			<div class="form-group">
				<div align="left" width="100%" style="padding-left: 170px;">
					<img
						src="<c:url value="/resources/images/financeiro/botao50.png"/>"
						style="align: left; cursor: pointer;"
						onclick="efetuarSaque('50.00');" />&nbsp; <img
						src="<c:url value="/resources/images/financeiro/botao100.png"/>"
						onclick="efetuarSaque('100.00');"
						style="align: left; cursor: pointer;" /> &nbsp; <img
						src="<c:url value="/resources/images/financeiro/botao150.png"/>"
						onclick="efetuarSaque('150.00');"
						style="align: left; cursor: pointer;" />
				</div>
				
				<input type="hidden" name="valorSaque" value="">
			</div>
			
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
				
		</form>
	</div>
	</div>
	<jsp:include page="../../includes/footer.jsp" />

</body>
</html>
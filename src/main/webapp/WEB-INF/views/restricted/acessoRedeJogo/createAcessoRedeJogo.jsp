<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />
	<div class="container">

		<h3>Criar Aceso Rede 1/2</h3>
		<br />
		<div>

			<sf:form name="formAcao" action="cadastrarAcessoRede"
				commandName="cadastrarAcessoRedeJogoForm" method="post"
				class="form-horizontal">

				<spring:hasBindErrors htmlEscape="true"
					name="cadastrarAcessoRedeJogoForm">
					<c:if test="${errors.errorCount gt 0}">
						<div class="alert alert-info">
							<sf:errors path="*" />
						</div>
					</c:if>
				</spring:hasBindErrors>

				<div class="form-group">
					<label for="usuarioRede" class="col-sm-2 control-label">Seu
						UserId</label>
					<div class="col-sm-10">
						<sf:input path="usuarioRede" class="form-control" id="usuarioRede"
							placeholder="Informe o nome de seu usuário na Rede"
							style="width: 300px;" tabindex="1" htmlEscape="true"
							maxlength="255" title="Seu usuário, Ex: Boby2" />
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<table>
							<tr>
								<td width="20%"><sf:radiobutton path="tipoAcessoRedeJogo"
										value="PSN" /></td>
								<td width="80%"><img
									src="<c:url value="/resources/images/networks/psnlogo.png"/>"
									class="media-object" style="align: left; cursor: pointer;" onclick="document.formAcao.tipoAcessoRedeJogo1.checked = true" /></td>
							</tr>
						</table>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<table>
							<tr>

								<td width="20%"><sf:radiobutton path="tipoAcessoRedeJogo"
										value="LIVE" /></td>
								<td width="80%"><img
									src="<c:url value="/resources/images/networks/xboxlogo.png"/>"
									class="media-object" onclick="document.formAcao.tipoAcessoRedeJogo2.checked = true" style="align: left; cursor: pointer;"/></td>
							</tr>
						</table>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-default">Cadastrar</button>
						<button type="button" class="btn btn-default"
							onclick="javascript:window.location.href='<c:url value="/restricted/acessoRedeJogo/listAcessoRedeJogo"/>'">Cancelar</button>
					</div>
				</div>

			</sf:form>

		</div>
	</div>
	<jsp:include page="../../includes/footer.jsp" />

</body>
</html>
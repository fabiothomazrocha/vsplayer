<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
</head>
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />
	
		<script>
		function cadastrar() {
			formAcao.action = '<c:url value="/restricted/acessoRedeJogo/createAcessoRedeJogo"/>';
			formAcao.submit();
		}

		function excluir() {
			formAcao.action = '<c:url value="/restricted/acessoRedeJogo/deleteAcessoRedeJogo"/>';
			formAcao.submit();
		}
		
	</script>

	<div class="container">
		<form name="formAcao" action="" method="get">
			<h2>Meus Acessos</h2>

			<div class="panel panel-default">

				<div class="panel-heading">Acessos Ativos</div>

				<c:if test="${empty requestScope.listaAcesso}">

					<div class="alert alert-warning">Você ainda não possui nenhum acesso rede.</div>
				</c:if>

				<c:if test="${not empty requestScope.listaAcesso}">

					<table class="table">
						<thead>
							<tr style="background-color: #f0f0f0">
								<td width="5%"><span class="glyphicon glyphicon-ok-circle"></span></td>
								<td width="50%"><strong>Usuário</strong></td>
								<td width="15%"><strong>Data Criação</strong></td>
								<td width="35%"><strong>Rede</strong></td>
							</tr>
						</thead>

						<tbody>
							<c:forEach var="acsRede" items="${requestScope.listaAcesso}"
								varStatus="statsVar">
								<tr>
									<td><input id="idAcessoRedeJogo"
										name="idAcessoRedeJogo" type="radio"
										value="${acsRede.idAcessoRedeJogo}"
										<c:if test="${statsVar.index == 0}"> checked="checked" </c:if> /></td>
									<td>${acsRede.userId}</td>
									<td>${acsRede.dataCriacao}</td>
									<td>${acsRede.redeAcesso}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>

				</c:if>
			</div>

			<div class="form-group">
				<div align="center">
					<button type="button" class="btn btn-primary"
						onclick="cadastrar();">Cadastrar Novo Acesso</button>
					<button class="btn btn-danger" type="button" data-toggle="modal"
						<c:if test="${empty requestScope.listaAcesso}"> disabled="disabled" </c:if> data-target="#confirmDesativar">
						Desativar</button>
				</div>
			</div>
		</form>
	</div>

	<!-- Modal Dialog -->
	<div class="modal fade" id="confirmDesativar" role="dialog"
		aria-labelledby="confirmDesativarLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Desativar Acesso Rede</h4>
				</div>
				<div class="modal-body">
					<p>Deseja realmente desativar este Acesso Rede?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="button" class="btn btn-danger" id="confirm"
					 onclick="excluir();">Sim, Desativar</button>
				</div>
			</div>
		</div>
	</div>

	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
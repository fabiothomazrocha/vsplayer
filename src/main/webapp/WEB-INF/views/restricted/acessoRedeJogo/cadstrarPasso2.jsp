<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />
	<div class="container">

		<h3>Criar Aceso Rede 2/2</h3>
		<br />
		<div>
			<sf:form name="formAcao" action="finalizarCadastro"
				commandName="cadastrarAcessoRedeJogoForm" method="post"
				class="form-horizontal">

				<spring:hasBindErrors htmlEscape="true"
					name="cadastrarAcessoRedeJogoForm">
					<c:if test="${errors.errorCount gt 0}">
						<div class="alert alert-info">
							<sf:errors path="*" />
						</div>
					</c:if>
				</spring:hasBindErrors>

				<p class="lead">Selecione os Consoles que você utiliza com a
					conta: ${requestScope.cadastrarAcessoRedeJogoForm.usuarioRede}</p>


				<c:if test="${not empty requestScope.listaConsole}">
					<c:forEach var="console" items="${requestScope.listaConsole}"
						varStatus="statsVar">
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<sf:checkbox id="checkThis${statsVar.index}"
									path="consolesSelecionados" value="${console.tipoConsole}" />
								&nbsp;<label for="checkThis${statsVar.index}"><strong>${console.nomeConsole}</strong></label>
							</div>
						</div>
					</c:forEach>
				</c:if>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-default">Cadastrar</button>
						<button type="button" class="btn btn-default"
							onclick="javascript:window.location.href='<c:url value="/restricted/acessoRedeJogo/listAcessoRedeJogo"/>'">Cancelar</button>
					</div>
				</div>
				<sf:hidden path="tipoAcessoRedeJogo"/>
				<sf:hidden path="usuarioRede"/>
			</sf:form>

		</div>
	</div>
	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
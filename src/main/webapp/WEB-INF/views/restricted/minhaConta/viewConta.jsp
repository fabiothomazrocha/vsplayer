<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
</head>
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />
	<div class="container">
		<br/><br/>
		<table width="100%">
			<tr>
				<td width="20%" style="padding-right: 10px;" valign="top"><div
						class="list-group" width="200px;" align="left">
						<a href="<c:url value="/restricted/minhaConta/viewConta"/>"
							class="list-group-item active">Meus Dados</a> <a
							href="<c:url value="/restricted/minhaConta/alterarDados"/>"
							class="list-group-item">Alterar Dados</a><a
							href="<c:url value="/restricted/minhaConta/alterarSenha"/>"
							class="list-group-item">Alterar Senha</a>
					</div></td>
				<td width="80%" style="padding-left: 10px;" valign="top"><div
						class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">
								<strong>Minha Conta</strong>
							</h3>
						</div>
						<div class="panel-body">
							<table>
								<tr>
									<td width="20%"><strong>Usuário:</strong></td>
									<td width="80%" style="padding-left: 5px;">${requestScope.usuario.nmNome}</td>
								</tr>
								<tr>
									<td width="20%"><strong>CPF:</strong></td>
									<td width="80%" style="padding-left: 5px;">${requestScope.usuario.dsCpf}</td>
								</tr>
								<tr>
									<td width="20%"><strong>E-mail:</strong></td>
									<td width="80%" style="padding-left: 5px;">${requestScope.usuario.dsEmail}</td>
								</tr>
								<tr>
									<td width="20%"><strong>Ativo:</strong></td>
									<td width="80%" style="padding-left: 5px;">${requestScope.usuario.ativado.descricao}</td>
								</tr>
								<tr>
									<td width="20%"><strong>Data Cadastro:</strong></td>
									<td width="80%" style="padding-left: 5px;"><fmt:formatDate
											pattern="dd/MM/yyyy HH:mm"
											value="${requestScope.usuario.dataCadastro}" /></td>
								</tr>
								<tr>
									<td width="20%"><strong>Data Ativação:</strong></td>
									<td width="80%" style="padding-left: 5px;"><fmt:formatDate
											pattern="dd/MM/yyyy HH:mm"
											value="${requestScope.usuario.dataAtivacao}" /></td>
								</tr>
								<tr>
									<td width="20%"><strong>Data Atualização:</strong></td>
									<td width="80%" style="padding-left: 5px;"><fmt:formatDate
											pattern="dd/MM/yyyy HH:mm"
											value="${requestScope.usuario.dataUltimaAtualizacao}" /></td>
								</tr>
								<tr>
									<td width="20%"><strong>Perfis Ativos:</strong></td>
									<td width="80%" style="padding-left: 5px;">${requestScope.perfilCount}
										&nbsp; <a
										href="<c:url value="/restricted/perfil/listPerfil"/>"><span>Visualizar</span></a>
									</td>
								</tr>
								<tr>
									<td width="20%"><strong>Acessos Rede Ativo:</strong></td>
									<td width="80%" style="padding-left: 5px;">${requestScope.acessoRedeCount}
										&nbsp; <a
										href="<c:url value="/restricted/acessoRedeJogo/listAcessoRedeJogo"/>"><span>Visualizar</span></a>
									</td>
								</tr>
							</table>
						</div>
					</div></td>
			</tr>
		</table>
	</div>


	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
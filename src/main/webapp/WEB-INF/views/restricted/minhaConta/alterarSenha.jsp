<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
</head>
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />


	<div class="container">
		<br/><br/>
		<table width="100%">
			<tr>
				<td width="20%" style="padding-right: 10px;" valign="top"><div
						class="list-group" width="200px;" align="left">
						<a href="<c:url value="/restricted/minhaConta/viewConta"/>"
							class="list-group-item">Meus Dados</a> <a
							href="<c:url value="/restricted/minhaConta/alterarDados"/>"
							class="list-group-item">Alterar Dados</a><a
							href="<c:url value="/restricted/minhaConta/alterarSenha"/>"
							class="list-group-item active">Alterar Senha</a>
					</div></td>
				<td width="80%" style="padding-left: 10px;" valign="top"><div
						class="panel panel-default">

						<div class="panel-heading">
							<h3 class="panel-title">
								<strong>Alterar Senha</strong>
							</h3>
						</div>

						<br />

						<sf:form modelAttribute="alterarSenhaForm" action="${pageContext.request.contextPath}/restricted/minhaConta/alterarSenha"
							method="POST" class="form-horizontal">

							<c:if test="${requestScope.mensagemSucesso != null}">
								<div class="alert alert-info">${requestScope.mensagemSucesso}</div>
							</c:if>
							
							<spring:hasBindErrors htmlEscape="true" name="alterarSenhaForm">
								<c:if test="${errors.errorCount gt 0}">
									<div class="alert alert-info">
										<sf:errors path="*" />
									</div>
								</c:if>
							</spring:hasBindErrors>

							<div class="form-group">
								<label for="senhaAntiga" class="col-sm-2 control-label">Senha
									atual</label>
								<div class="col-sm-10">
									<sf:password path="senhaAntiga" class="form-control"
										id="senhaAntiga" placeholder="Ex: !#@Joao123"
										style="width: 200px;" tabindex="1" htmlEscape="true"
										maxlength="255" title="Informe sua senha atual" />
								</div>
							</div>

							<div class="form-group">
								<label for="novaSenha" class="col-sm-2 control-label">Nova
									senha</label>
								<div class="col-sm-10">
									<sf:password path="novaSenha" class="form-control"
										id="novaSenha" placeholder="Ex: !#@Joao123"
										style="width: 200px;" tabindex="2" htmlEscape="true"
										maxlength="255" title="Informe sua nova senha" />
								</div>
							</div>

							<div class="form-group">
								<label for="repitaNovaSenha" class="col-sm-2 control-label">Repita
									sua senha</label>
								<div class="col-sm-10">
									<sf:password path="repitaNovaSenha" class="form-control"
										id="repitaNovaSenha" placeholder="Ex: !#@Joao123"
										style="width: 200px;" tabindex="3" htmlEscape="true"
										maxlength="255" title="Repita sua nova senha" />
								</div>
							</div>

							<sf:hidden path="codigoAlterarSenha" id="codigoAlterarSenha" />

							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-default">Alterar
										Senha</button>
								</div>
							</div>

						</sf:form></td>
			</tr>
		</table>
	</div>


	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
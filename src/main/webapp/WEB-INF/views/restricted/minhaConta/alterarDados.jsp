<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:include page="../../includes/header.jsp" />
</head>
<body>
	<jsp:include page="../../includes/navbarcomlogin.jsp" />

	<div class="container">
		<br /> <br />
		<table width="100%">
			<tr>
				<td width="20%" style="padding-right: 10px;" valign="top"><div
						class="list-group" width="200px;" align="left">
						<a href="<c:url value="/restricted/minhaConta/viewConta"/>"
							class="list-group-item">Meus Dados</a> <a
							href="<c:url value="/restricted/minhaConta/alterarDados"/>"
							class="list-group-item active">Alterar Dados</a><a
							href="<c:url value="/restricted/minhaConta/alterarSenha"/>"
							class="list-group-item">Alterar Senha</a>
					</div></td>
				<td width="80%" style="padding-left: 10px;" valign="top"><div
						class="panel panel-default">

						<div class="panel-heading">
							<h3 class="panel-title">
								<strong>Alterar Dados</strong>
							</h3>
						</div>
						<div class="panel-body">

							<p class="lead">Atenção! O seu e-mail pode ser alterado,
								porém seu acesso será bloqueado até que você confirme seu e-mail
								através do link para ativação.</p>

							<sf:form modelAttribute="alterarDadosForm"
								action="${pageContext.request.contextPath}/restricted/minhaConta/alterarDados"
								method="POST" name="alterarDadosForm" class="form-inline">

								<spring:hasBindErrors htmlEscape="true" name="alterarDadosForm">
									<c:if test="${errors.errorCount gt 0}">
										<div class="alert alert-info">
											<sf:errors path="*" />
										</div>
									</c:if>
								</spring:hasBindErrors>

								<div class="form-group">
									<div class="form-group">
										<sf:input path="novoEmail" class="form-control" id="novoEmail"
											placeholder="Ex: jamesbogus@yahoo.com" style="width: 400px;"
											tabindex="1" htmlEscape="true" maxlength="255"
											title="Informe seu novo e-mail" />
									</div>

									<button type="button" data-toggle="modal"
										data-target="#confirmAlterar" class="btn btn-danger">Alterar
										meu e-mail</button>
								</div>

							</sf:form>
						</div>
						<br />
					</div></td>
			</tr>
		</table>
	</div>

	<!-- Modal Dialog -->
	<div class="modal fade" id="confirmAlterar" role="dialog"
		aria-labelledby="confirmAlterarLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Alterar e-mail</h4>
				</div>
				<div class="modal-body">
					<p>Deseja realmente alterar seu e-mail?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="button" class="btn btn-danger" id="confirm"
						onclick="document.alterarDadosForm.submit();">Sim, Alterar</button>
				</div>
			</div>
		</div>
	</div>

	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<jsp:include page="../../includes/header.jsp" />
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.11.0.js"></script>
<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<style>
#sortable {
	list-style-type: none;
	margin: 1;
	padding: 0;
	width: 450px;
}

#sortable li {
	margin: 3px 50px 3px 0;
	padding: 1px;
	float: left;
	width: 100px;
	height: 120px;
	font-size: 1em;
	text-align: center;
}
</style>
<script>
	$(function() {
		$("#sortable").sortable();
		$("#sortable").disableSelection();
	});
</script>
</head>
<body>

	<jsp:include page="../../includes/navbarcomlogin.jsp" />
	<div class="container">
		<form name="formAcao" action="" method="get">
			<h2>Desafio por Sala de Jogos</h2>
			<div class="panel-heading">
				<ul id="sortable">
					<c:forEach var="sala" items="${requestScope.listSalaJogos}"
						varStatus="statsVar">
						<li>
							<div class="row">

								<p>${sala.nmSalaJogos}</p>
								<p>${sala.nomeJogo}</p>
								<p>Online: ${sala.qtdUsuario}</p>

								<%-- 	<p> ${requestScope.usuariosOnline}</p> --%>
								<div class="thumbnail">
									<c:if test="${sala.tipoConsole.nmConsole eq'Playstation 3' }">
										<img
											src="<c:url value="/resources/images/networks/PS3-Pink-128.png"/>"
											alt="#">
									</c:if>
									<c:if test="${sala.tipoConsole.nmConsole eq'Playstation 4' }">
										<img
											src="<c:url value="/resources/images/networks/playstation3.png"/>"
											alt="#">
									</c:if>
									<c:if test="${sala.tipoConsole.nmConsole eq'XBOX 360' }">
										<img
											src="<c:url value="/resources/images/networks/xbox_360.png"/>"
											alt="#">
									</c:if>
									<c:if test="${sala.tipoConsole.nmConsole eq'XBOX ONE' }">
										<img
											src="<c:url value="/resources/images/networks/xboxlogo.png"/>"
											alt="#">
									</c:if>
								</div>
							</div> <span class="label label-danger"> R$:${sala.valorSala}</span>
							<p>
								<a href="#" class="btn btn-primary" type="button">Desafiar</a>
							</p>

						</li>
					</c:forEach>
				</ul>
			</div>


		</form>

	</div>


	<jsp:include page="../../includes/footer.jsp" />
</body>
</html>